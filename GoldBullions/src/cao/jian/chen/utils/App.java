package cao.jian.chen.utils;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.text.TextUtils;

import com.baidu.frontia.FrontiaApplication;
import com.easemob.chat.EMChat;
import com.easemob.chat.EMChatManager;
import com.easemob.chat.EMChatOptions;
import com.easemob.chat.EMMessage;
import com.easemob.chat.EMMessage.ChatType;
import com.easemob.chat.OnMessageNotifyListener;
import com.easemob.chat.OnNotificationClickListener;


public class App extends FrontiaApplication {

	private static Context _context;

	@Override
	public void onCreate() {
		super.onCreate();
		_context = getApplicationContext();
		initEMChat();
		EMChat.getInstance().init(_context);
		EMChat.getInstance().setDebugMode(true);
		EMChat.getInstance().setAutoLogin(true);
		EMChatManager.getInstance().getChatOptions().setUseRoster(true);
		FrontiaApplication.initFrontiaApplication(this);
		// CrashHandler crashHandler = CrashHandler.getInstance();// 鍏ㄥ眬寮傚父鎹曟崏
		// crashHandler.init(_context);
	}

	private void initEMChat() {
		int pid = android.os.Process.myPid();
		String processAppName = getAppName(pid);
		if (processAppName == null
				|| !processAppName.equalsIgnoreCase("hk.com.cybersys.basic")) {
			return;
		}
		EMChatOptions options = EMChatManager.getInstance().getChatOptions();
		// 鑾峰彇鍒癊MChatOptions瀵硅薄
		// 璁剧疆鑷畾涔夌殑鏂囧瓧鎻愮ず
		options.setNotifyText(new OnMessageNotifyListener() {

			@Override
			public String onNewMessageNotify(EMMessage message) {
				return "浣犵殑濂藉弸鍙戞潵浜嗕竴鏉℃秷鎭摝";
			}

			@Override
			public String onLatestMessageNotify(EMMessage message,
					int fromUsersNum, int messageNum) {
				return fromUsersNum + "涓ソ鍙嬶紝鍙戞潵浜�" + messageNum + "鏉℃秷鎭�";
			}

			@Override
			public String onSetNotificationTitle(EMMessage arg0) {
				return null;
			}

			@Override
			public int onSetSmallIcon(EMMessage arg0) {
				return 0;
			}
		});
		options.setOnNotificationClickListener(new OnNotificationClickListener() {

			@Override
			public Intent onNotificationClick(EMMessage message) {
				//Intent intent = new Intent(_context, MainActivity.class);
				ChatType chatType = message.getChatType();
		/*		if (chatType == ChatType.Chat) { // 鍗曡亰淇℃伅
					intent.putExtra("userId", message.getFrom());
					intent.putExtra("chatType", ChatActivity.CHATTYPE_SINGLE);
				} else { // 缇よ亰淇℃伅
					// message.getTo()涓虹兢鑱奿d
					intent.putExtra("groupId", message.getTo());
					intent.putExtra("chatType", ChatActivity.CHATTYPE_GROUP);
				}*/
				return null;
			}
		});
		// IntentFilter callFilter = new
		// IntentFilter(EMChatManager.getInstance()
		// .getIncomingCallBroadcastAction());
		// registerReceiver(new CallReceiver(), callFilter);
	}

	/*private class CallReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			// 鎷ㄦ墦鏂箄sername
			String from = intent.getStringExtra("from");
			// call type
			String type = intent.getStringExtra("type");
			startActivity(new Intent(_context, VoiceCallActivity.class)
					.putExtra("username", from).putExtra("isComingCall", true));
		}
	}
*/
	private String getAppName(int pID) {
		String processName = null;
		ActivityManager am = (ActivityManager) this
				.getSystemService(ACTIVITY_SERVICE);
		List l = am.getRunningAppProcesses();
		Iterator i = l.iterator();
		PackageManager pm = this.getPackageManager();
		while (i.hasNext()) {
			ActivityManager.RunningAppProcessInfo info = (ActivityManager.RunningAppProcessInfo) (i
					.next());
			try {
				if (info.pid == pID) {
					CharSequence c = pm.getApplicationLabel(pm
							.getApplicationInfo(info.processName,
									PackageManager.GET_META_DATA));
					processName = info.processName;
					return processName;
				}
			} catch (Exception e) {
			}
		}
		return processName;
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		try {
			deleteCacheDirFile(getHJYCacheDir(), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.gc();
	}

	public static Context getInstance() {
		return _context;
	}

	// 杩愮敤list鏉ヤ繚瀛樹滑姣忎竴涓猘ctivity鏄叧閿�
	private List<Activity> mList = new LinkedList<Activity>();
	private static App instance;

	// 鏋勯�犳柟娉�
	// 瀹炰緥鍖栦竴娆�
	public synchronized static App getInstance2() {
		if (null == instance) {
			instance = new App();
		}
		return instance;
	}

	// add Activity
	public void addActivity(Activity activity) {
		mList.add(activity);
	}

	// 鍏抽棴姣忎竴涓猯ist鍐呯殑activity
	public void exit() {
		try {
			for (Activity activity : mList) {
				if (activity != null)
					activity.finish();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.exit(0);
		}
	}

	public static String getHJYCacheDir() {
		if (Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED))
			return Environment.getExternalStorageDirectory().toString()
					+ "/Health/Cache";
		else
			return "/System/com.juns.Walk/Walk/Cache";
	}

	public static String getHJYDownLoadDir() {
		if (Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED))
			return Environment.getExternalStorageDirectory().toString()
					+ "/Walk/Download";
		else {
			return "/System/com.Juns.Walk/Walk/Download";
		}
	}

	public static void deleteCacheDirFile(String filePath,
			boolean deleteThisPath) throws IOException {
		if (!TextUtils.isEmpty(filePath)) {
			File file = new File(filePath);
			if (file.isDirectory()) {// 澶勭悊鐩綍
				File files[] = file.listFiles();
				for (int i = 0; i < files.length; i++) {
					deleteCacheDirFile(files[i].getAbsolutePath(), true);
				}
			}
			if (deleteThisPath) {
				if (!file.isDirectory()) {// 濡傛灉鏄枃浠讹紝鍒犻櫎
					file.delete();
				} else {// 鐩綍
					if (file.listFiles().length == 0) {// 鐩綍涓嬫病鏈夋枃浠舵垨鑰呯洰褰曪紝鍒犻櫎
						file.delete();
					}
				}
			}
		}
	}
}
