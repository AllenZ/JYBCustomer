package hk.com.cybersys.activity;

import hk.com.cybersys.basic.R;
import hk.com.cybersys.http.HCPostRequest;
import hk.com.cybersys.myLibrary.CommonWidget;
import hk.com.cybersys.myLibrary.Constant;
import hk.com.cybersys.otheractivity.SelectBrokerActivity;
import hk.com.cybersys.service.PushService;
import hk.com.cybersys.widget.CalendarView.OnItemClickListener;
import hk.com.cybersys.widget.MyCalendarView;



import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.AnticipateOvershootInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import cao.jian.chen.utils.Constants;
import cao.jian.chen.utils.Utils;

import com.wheel.StrericWheelAdapter;
import com.wheel.WheelView;



public class Appointment extends Activity {
	
	private SimpleDateFormat format;
	private TextView txt_title;
	private ImageView backImg;

	//预约日期
	private LinearLayout myAppointmentDateLayout;
	private TextView myAppointmentDate;
	//预约时间
	private LinearLayout myAppointmentTimeLayout,select_broker;
	private TextView myAppointmentTime;
	//理财经理
	private TextView appointmnet_brokerName;
	//被选中的理财经理ID
	private String isSelectedBrokerID;
	//客户的ID
	private String customerID;
	//选择产品的视图
	private TextView selected_private_equity;

	public static String[] hourContent = null;
	public static String[] minuteContent = null;
	private WheelView hourWheel, minuteWheel;
	
	private Handler appointmentHandler;
	
	private String prName[] = null;
	private String appointmentDate;
	private String appoingmentTime;
	private String appointmentWhile;
	
	//日历控件
	private MyCalendarView myCalendar;
	private ImageButton calendarLeft;
	private TextView calendarCenter;
	private ImageButton calendarRight;
	private SimpleDateFormat calendarFormat;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_c);	
		//初始化Title
		initTitle();
		select_broker=(LinearLayout) findViewById(R.id.select_broker);
		//给理财经理添加点击事件
		select_broker.setOnClickListener(new View.OnClickListener() {		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(Appointment.this,SelectBrokerActivity.class));
				
			}
		});
		//SharedPreferences settings = getSharedPreferences("LoginStatus", 0);
		isSelectedBrokerID = this.getIntent().getExtras().getString("isSelectedBrokerID");
		//customerID =  settings.getString("customerID", "");;
		customerID=Utils.getValue(this, Constants.CustomerID);
		initContent();
		LayoutInflater inflater = LayoutInflater.from(this);
		// 引入窗口配置文件
		View view = inflater.inflate(R.layout.popup_calendar_view, null);
		// 创建PopupWindow对象
		final PopupWindow pop = new PopupWindow(view,
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, false);

		// 需要设置一下此参数，点击外边可消失
		pop.setBackgroundDrawable(new BitmapDrawable());
		// 设置点击窗口外边窗口消失
		pop.setOutsideTouchable(true);
		// 设置此参数获得焦点，否则无法点击
		pop.setFocusable(true);
		myAppointmentDate = (TextView)findViewById(R.id.appointment_date);
		myAppointmentTime = (TextView)findViewById(R.id.appointment_time);
		calendarCenter = (TextView)view.findViewById(R.id.calendarCenter);
        calendarLeft = (ImageButton)view.findViewById(R.id.calendarLeft);
		calendarRight = (ImageButton)view.findViewById(R.id.calendarRight);
		myAppointmentDateLayout = (LinearLayout) findViewById(R.id.appointment_date_layout);
		
		myAppointmentDateLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (pop.isShowing()) {
					// 隐藏窗口，如果设置了点击窗口外小时即不需要此方式隐藏
					pop.dismiss();
				} else {
					// 显示窗口
					pop.showAsDropDown(v);
					//showMyCalendarView();
				}			
			}
		});
		appointmnet_brokerName = (TextView)findViewById(R.id.appointment_brokerName);
		appointmnet_brokerName.setText(this.getIntent().getExtras().getString("brokerName"));
		
		LayoutInflater inflater2 = LayoutInflater.from(this);
		// 引入窗口配置文件
		View view2 = inflater2.inflate(R.layout.popup_time_view, null);
		// 创建PopupWindow对象
		final PopupWindow pop2 = new PopupWindow(view2,
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, false);

		// 需要设置一下此参数，点击外边可消失
		pop2.setBackgroundDrawable(new BitmapDrawable());
		// 设置点击窗口外边窗口消失
		pop2.setOutsideTouchable(true);
		// 设置此参数获得焦点，否则无法点击
		pop2.setFocusable(true);

		Calendar calendar22 = Calendar.getInstance();
		int curYear = calendar22.get(Calendar.YEAR);
		int curMonth = calendar22.get(Calendar.MONTH) + 1;
		int curDay = calendar22.get(Calendar.DAY_OF_MONTH);
		int curHour = calendar22.get(Calendar.HOUR_OF_DAY);
		int curMinute = calendar22.get(Calendar.MINUTE);
		int curSecond = calendar22.get(Calendar.SECOND);
		hourWheel = (WheelView) view2.findViewById(R.id.hourwheel);
		minuteWheel = (WheelView) view2.findViewById(R.id.minutewheel);
		List<String> data = new ArrayList<String>();  
        List<String> seconds = new ArrayList<String>();
		hourWheel.setAdapter(new StrericWheelAdapter(hourContent));
		hourWheel.setCurrentItem(curHour);
		hourWheel.setCyclic(true);
		hourWheel.setInterpolator(new AnticipateOvershootInterpolator());

		minuteWheel.setAdapter(new StrericWheelAdapter(minuteContent));
		minuteWheel.setCurrentItem(curMinute);
		minuteWheel.setCyclic(true);
		minuteWheel.setInterpolator(new AnticipateOvershootInterpolator());

		this.hourWheel.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub
				myAppointmentTime.setText(hourWheel.getCurrentItemValue() + ":"
						+ minuteWheel.getCurrentItemValue());
				appoingmentTime=myAppointmentTime.getText().toString().trim();
				System.out.println("appoingmentTime"+appoingmentTime);
				return false;
			}
		});
		this.minuteWheel.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub
				myAppointmentTime.setText(hourWheel.getCurrentItemValue() + ":"
						+ minuteWheel.getCurrentItemValue());
				return false;
			}
		});
      
        
        
		this.myAppointmentTimeLayout = (LinearLayout) findViewById(R.id.appointment_time_layout);
		this.myAppointmentTimeLayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (pop2.isShowing()) {
					// 隐藏窗口，如果设置了点击窗口外小时即不需要此方式隐藏
					pop2.dismiss();
				} else {
					// 显示窗口
					pop2.showAsDropDown(v);
				}
			}
		});

		//showMyCalendarView();
		// 获取日历控件对象
	    myCalendar = (MyCalendarView) view.findViewById(R.id.calendar);
		//设置控件监听，可以监听到点击的每一天（大家也可以在控件中根据需求设定）
	    //myCalendar.showMyCalendarView();
	    showMyCalendarView();
	    myCalendar.setOnItemClickListener(new hk.com.cybersys.widget.MyCalendarView.OnItemClickListener() {			
					@Override
					public void OnItemClick(Date selectedStartDate,
							Date selectedEndDate, Date downDate) {
						if(myCalendar.isSelectMore()){
							Toast.makeText(getApplicationContext(), "kkkkkkkkk", Toast.LENGTH_SHORT).show();
						}else{
							//Toast.makeText(getApplicationContext(), "HHHHHHHHH", Toast.LENGTH_SHORT).show();
							myAppointmentDate.setText(new SimpleDateFormat("yyyy-MM-dd")
						     .format(downDate));
						}
					}
				});
	    

		// // 获取日历中年月 ya[0]为年，ya[1]为月（格式大家可以自行在日历控件中改）
		// String[] ya = calendar.getYearAndmonth().split("-");
		// // 点击上一月 同样返回年月
		// String leftYearAndmonth = calendar.clickLeftMonth();
		// String[] lya = leftYearAndmonth.split("-");
		// // 点击下一月
		// String rightYearAndmonth = calendar.clickRightMonth();
		// String[] rya = rightYearAndmonth.split("-");
		// format = new SimpleDateFormat("yyyy-MM-dd");
		// 设置控件监听，可以监听到点击的每一天（大家也可以在控件中根据需求设定）
//		calendar.setOnItemClickListener(new OnItemClickListener() {
//
//			@Override
//			public void OnItemClick(Date selectedStartDate,
//					Date selectedEndDate, Date downDate) {
//				//appointmentDate=new SimpleDateFormat("yyyy-MM-dd").format(downDate);
//				
//				myAppointmentDate.setText(new SimpleDateFormat("yyyy-MM-dd")
//						.format(downDate));
//				System.out.println("appointmentDate:"+myAppointmentDate.getText().toString().trim());
//				Toast.makeText(getApplicationContext(),
//						new SimpleDateFormat("yyyy-MM-dd").format(downDate),
//						Toast.LENGTH_SHORT).show();
//			}
//		});
		//calendar.
				
		appointmentHandler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				CommonWidget.progressDialog.dismiss();
				Intent intent=new Intent();
				intent.setClass(Appointment.this, MainActivity.class);
				startActivity(intent);
				Utils.finish(Appointment.this);
				super.handleMessage(msg);
			}		
		};
	}

	public void initContent() {
		hourContent = new String[24];
		for (int i = 0; i < 24; i++) {
			hourContent[i] = String.valueOf(i);
			if (hourContent[i].length() < 2) {
				hourContent[i] = "0" + hourContent[i];
			}
		}

		minuteContent = new String[60];
		for (int i = 0; i < 60; i++) {
			minuteContent[i] = String.valueOf(i);
			if (minuteContent[i].length() < 2) {
				minuteContent[i] = "0" + minuteContent[i];
			}
		}
	}

	public void onViewClick(View v) {
		switch (v.getId()) {
		case R.id.private_equity:
			showDialog("私募基金","field=name&category=私募基金",v);
			break;
		case R.id.sumbit_appointment:
			String param = "brokerid=" + isSelectedBrokerID + "&" + "customerid=" + customerID + "&" + "date=" + myAppointmentDate.getText() + "&" + "time=" + myAppointmentTime.getText(); 
			appointmentWhile=myAppointmentDate.getText().toString().trim()+" "+myAppointmentTime.getText().toString().trim()+":00";
			System.out.println("appointmentWhile:"+appointmentWhile);
			Runnable r = new AppointmentSubmitThread(param);
			new Thread(r).start();
			CommonWidget.myProgressDialogShow(Appointment.this, "正在预约，请稍后...", "", false);
			break;
		case R.id.trust_products:
			showDialog("信托产品","field=name&category=信托产品",v);
			break;
		case R.id.asst_management:
			showDialog("信托产品","field=name&category=资产管理",v);
			break;
		}
	}
	@SuppressLint("NewApi") 
	private void showDialog(String title,String parameter,View v){
		StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		String productList = HCPostRequest.sendPost(Constant.URL_PRODUCT_FULL_LIST, parameter);
		
		
		
		try {
			JSONObject jsonOb = new JSONObject(productList);
			JSONArray jsArray = jsonOb.getJSONArray("list");
			prName = new String[jsArray.length()];
			for (int i = 0; i < jsArray.length(); i++) {
				prName[i] = jsArray.getJSONObject(i).getString("name");
			}
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		AlertDialog.Builder listAlerDialog = new AlertDialog.Builder(Appointment.this);
		listAlerDialog.setTitle(title);
		listAlerDialog.setSingleChoiceItems(prName, 0,new sureOnClick());
		listAlerDialog.setPositiveButton("确定",new sureOnClick());
		listAlerDialog.setNegativeButton("取消", null).show();
	}
	
	class sureOnClick implements DialogInterface.OnClickListener {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			
			if(which != -1){
				selected_private_equity = (TextView)findViewById(R.id.selected_private_equity);
				selected_private_equity.setVisibility(View.VISIBLE);
				selected_private_equity.setText(prName[which]);
			}
		}

		
	}
	
	/*
	 * 提交预约确认
	 */
	class AppointmentSubmitThread implements Runnable{
		private String param;
		private String jsonStr;
		public AppointmentSubmitThread(String param){
			this.param = param;
		}
		
		@Override
		public void run() {
			Looper.prepare();
			jsonStr = HCPostRequest.sendPost(Constant.URL_APPOINTMENT_ADD, param);
			try {
				JSONObject jsonOb = new JSONObject(jsonStr);
				System.out.println("预约返回的数据："+jsonOb);
				if(jsonOb.getString("success").equals("1")){
					Toast.makeText(Appointment.this, "预约成功，感谢您的支持！", Toast.LENGTH_LONG).show();
					//NotificationManager mn= (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
				    //啓動服務，在後臺执行
					startMyService();
				}else{
					Toast.makeText(Appointment.this, "预约失败，请重新预约！", Toast.LENGTH_LONG).show();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Appointment.this.appointmentHandler.sendEmptyMessage(0);
			Looper.loop();
		}
		
	}
	private void startMyService(){
		System.out.println("启动后台服务");
		Intent myIntent=new Intent();
		myIntent.putExtra("appointmentTime", appointmentWhile);
		myIntent.putExtra("tickerText", "哈哈哈哈");
		myIntent.putExtra("contentText", "你与理财师"+appointmnet_brokerName.getText().toString()+"的见面将在一小时后进行");
		myIntent.putExtra("contentTitle", "财富365的预约见面通知");
		
		myIntent.setClass(Appointment.this,PushService.class);
		startService(myIntent);
	}
	//初始化Title
	private void initTitle() {
		backImg=(ImageView) findViewById(R.id.img_back);
		backImg.setVisibility(View.VISIBLE);
		backImg.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//System.out.println("哈哈哈哈！");
				Utils.finish(Appointment.this);
			}
		});
		findViewById(R.id.txt_right).setVisibility(View.GONE);
		txt_title = (TextView) findViewById(R.id.txt_title);
		txt_title.setText("预约服务");
	}

	
	public void showMyCalendarView(){
		calendarFormat = new SimpleDateFormat("yyyy-MM-dd");
		//获取日历控件对象	
		
		try {
			//设置日历日期
			Date date = calendarFormat.parse("2015-01-01");
			myCalendar.setCalendarData(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		//获取日历中年月 ya[0]为年，ya[1]为月（格式大家可以自行在日历控件中改）
		String[] ya = myCalendar.getYearAndmonth().split("-"); 
		calendarCenter.setText(ya[0]+"年"+ya[1]+"月");
		calendarLeft.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//点击上一月 同样返回年月 
				String leftYearAndmonth = myCalendar.clickLeftMonth(); 
				String[] ya = leftYearAndmonth.split("-"); 
				calendarCenter.setText(ya[0]+"年"+ya[1]+"月");
			}
		});
		
		calendarRight.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				//点击下一月
				String rightYearAndmonth =myCalendar.clickRightMonth();
				String[] ya = rightYearAndmonth.split("-"); 
				calendarCenter.setText(ya[0]+"年"+ya[1]+"月");
			}
		});
	}
	
	
}