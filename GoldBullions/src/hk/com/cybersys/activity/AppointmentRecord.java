package hk.com.cybersys.activity;


import hk.com.cybersys.activity.MyDialog.MyDialogOption;
import hk.com.cybersys.basic.R;
import hk.com.cybersys.http.HCPostRequest;
import hk.com.cybersys.myLibrary.CommonWidget;
import hk.com.cybersys.myLibrary.Constant;
import hk.com.cybersys.view.HistoryFragment;
import hk.com.cybersys.view.TobePresentFragment;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cao.jian.chen.utils.App;
import cao.jian.chen.utils.Constants;
import cao.jian.chen.utils.Utils;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

//查看预约记录，两个Fragment，待出席，历史记录
public class AppointmentRecord extends FragmentActivity {
	private TextView txt_title;
	private ImageView backImg;
	private Fragment[] appointfragments;
	private TobePresentFragment tobePFragment;
	private HistoryFragment     appointmentHisFragment;
	private Button[] btn_appointment;
	
	private int index;
	private int currentTabIndex=0;// 当前fragment的index
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.activity_appointment_record);
		super.onCreate(savedInstanceState);
		App.getInstance2().addActivity(this);
		initTitle();
		//先判断是否以及登录
		if(Utils.getBooleanValue(this,Constants.LoginState)){
			//登录
			initAppointmentTab();
		}else{
			//未登录
			Toast.makeText(this, "请登陆后，在查看预约记录！",Toast.LENGTH_SHORT).show();
			Intent intent = new Intent();
			intent.setClass(this, Login.class);
			startActivity(intent);
			Utils.finish(this);	
		}
		
			
	}
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		
	}

	
	//初始化Title
	private void initTitle() {
			backImg=(ImageView) findViewById(R.id.img_back);
			backImg.setVisibility(View.VISIBLE);
			backImg.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//System.out.println("哈哈哈哈！");
					Utils.finish(AppointmentRecord.this);
				}
			});
			findViewById(R.id.txt_right).setVisibility(View.GONE);
			txt_title = (TextView) findViewById(R.id.txt_title);
			txt_title.setText("我的预约");
		}
		
	//初始化两个fragment
	public void initAppointmentTab(){
			tobePFragment=new TobePresentFragment();
			appointmentHisFragment=new HistoryFragment();			
			appointfragments=new Fragment[]{tobePFragment,appointmentHisFragment};
			btn_appointment=new Button[2];
			btn_appointment[0]=(Button) findViewById(R.id.btn_tobePresent);
			btn_appointment[1]=(Button) findViewById(R.id.btn_history);
			getSupportFragmentManager().beginTransaction()
	    	   .add(R.id.appointment_container, tobePFragment)
			   .add(R.id.appointment_container, appointmentHisFragment) 
			   .hide(appointmentHisFragment)
			   .show(tobePFragment)
		       .commit();
		}
	public void onAppointmentClick(View view){
		switch (view.getId()) {
		case R.id.btn_tobePresent:
			System.out.println("这是onAppointment上的待出席点击事件");
			index=0;
			if(tobePFragment==null){
				tobePFragment=new TobePresentFragment();
			}
			break;
		case R.id.btn_history:
			System.out.println("这是onAppointment上的历史记录点击事件");
			index=1;
			break;
		default:
			break;
		}
		if (currentTabIndex != index) {
			FragmentTransaction trx = getSupportFragmentManager()
					.beginTransaction();
			trx.hide(appointfragments[currentTabIndex]);
			if (!appointfragments[index].isAdded()) {
				trx.add(R.id.appointment_container, appointfragments[index]);
			}
			trx.show(appointfragments[index]).commit();
		}
		//transaction.commit();
		btn_appointment[currentTabIndex].setSelected(false);
		// 把当前tab设为选中状态
		btn_appointment[index].setSelected(true);
		btn_appointment[currentTabIndex].setBackgroundColor(Color.rgb(245, 245, 245));
		btn_appointment[index].setBackgroundColor(Color.rgb(65, 105, 225));		
		//textviews[currentTabIndex].setTextColor(0xFF999999);
		//textviews[index].setTextColor(0xFF45C01A);
		currentTabIndex = index;
	}
	
	
}
