package hk.com.cybersys.activity;

import hk.com.cybersys.basic.R;

import java.util.ArrayList;
import java.util.List;

import com.tencent.android.tpush.XGPushManager;
import com.tencent.android.tpush.service.XGPushService;

import android.app.Activity;
import android.app.LocalActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;

public class BaseActivity extends Activity {
	private ViewPager viewPager;
	private LayoutInflater mInflater;
	private List<View> mListViews = new ArrayList<View>();
	private MainActivityPagerAdapter mainActivityPagerAdapter;
	LocalActivityManager manager = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
//		TabHost tabHost = this.geth  
//
//		tabHost.addTab(tabHost.newTabSpec("tab1")
//		.setIndicator("list")//设置显示的tab名
//		.setContent(new Intent(this, List1.class)));//设置跳转的activity
		
		setContentView(R.layout.activity_base);
		initPagerViewer();
//		this.mInflater = getLayoutInflater();
//		View layout1 = this.mInflater.inflate(R.layout.bottom_toolbar, null);
//		View layout3 = this.mInflater.inflate(R.layout.bottom_toolbar, null);
//		View layout2 = this.mInflater.inflate(R.layout.popup_calendar_view, null);
//		View layout2 = this.mInflater.inflate(R.layout.activity_collection,
//				null);
//		View layout3 = this.mInflater.inflate(R.layout.activity_products, null);
//		View layout4 = this.mInflater.inflate(
//				R.layout.activity_individual_center, null);

//		this.mListViews.add(layout1);
//		this.mListViews.add(layout2);
//		this.mListViews.add(layout3);
//		this.mListViews.add(layout2);
//		this.mListViews.add(layout3);
//		this.mListViews.add(layout4);

//		this.mainActivityPagerAdapter = new MainActivityPagerAdapter(
//				this.mListViews);
//		 this.viewPager = (ViewPager) findViewById(R.id.viewpager1); 
//		this.viewPager.setAdapter(this.mainActivityPagerAdapter);
//		this.viewPager.setOnPageChangeListener(new OnPageChangeListener() {
//			
//			@Override
//			public void onPageSelected(int arg0) {
//				// TODO Auto-generated method stub
//				
//			}
//			
//			@Override
//			public void onPageScrolled(int arg0, float arg1, int arg2) {
//				// TODO Auto-generated method stub
//				
//			}
//			
//			@Override
//			public void onPageScrollStateChanged(int arg0) {
//				// TODO Auto-generated method stub
//				
//			}
//		});
		
		

	}
	
	 /**
     * 初始化PageViewer
     */
    private void initPagerViewer() {
    	ViewPager pager = (ViewPager) findViewById(R.id.viewpager1);
        final ArrayList<View> list = new ArrayList<View>();
        Intent intent = new Intent(BaseActivity.this,MapReservation.class);
        list.add(getView("A", intent));
      //  Intent intent2 = new Intent(BaseActivity.this, Collection.class);
       // list.add(getView("B", intent2));
        pager.setAdapter(new MyPagerAdapter(list));
        pager.setCurrentItem(0);
        pager.setOnPageChangeListener(new MyOnPageChangeListener());
    }
    /**
     * 通过activity获取视图
     * @param id
     * @param intent
     * @return
     */
    @SuppressWarnings("deprecation")
	private View getView(String id, Intent intent) {
        return manager.startActivity(id, intent).getDecorView();
    }
    /**
     * Pager适配器
     */
    public class MyPagerAdapter extends PagerAdapter{
        List<View> list =  new ArrayList<View>();
        public MyPagerAdapter(ArrayList<View> list) {
            this.list = list;
        }


        @Override
        public void destroyItem(ViewGroup container, int position,
                Object object) {
            ViewPager pViewPager = ((ViewPager) container);
            pViewPager.removeView(list.get(position));
        }


        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }


        @Override
        public int getCount() {
            return list.size();
        }
        @Override
        public Object instantiateItem(View arg0, int arg1) {
            ViewPager pViewPager = ((ViewPager) arg0);
            pViewPager.addView(list.get(arg1));
            return list.get(arg1);
        }


        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {


        }


        @Override
        public Parcelable saveState() {
            return null;
        }


        @Override
        public void startUpdate(View arg0) {
        }
    }
    /**
     * 页卡切换监听
     */
    public class MyOnPageChangeListener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int arg0) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void onPageSelected(int arg0) {
			// TODO Auto-generated method stub
			
		}   	
    }
}
