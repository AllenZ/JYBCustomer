package hk.com.cybersys.activity;

import hk.com.cybersys.basic.R;
import cao.jian.chen.utils.NetUtils;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

public class Display extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);//无标题
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_display);
		
		if(NetUtils.isConnected(Display.this)){
			Toast.makeText(Display.this, "联网成功", Toast.LENGTH_LONG).show();
		}else{
			Toast.makeText(Display.this, "手机无网络", Toast.LENGTH_LONG).show();
		}
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				Display.this.finish();
				Intent intent = new Intent(Display.this,MainActivity.class);
				Display.this.startActivity(intent);
			}
		}).start();
	}

}
