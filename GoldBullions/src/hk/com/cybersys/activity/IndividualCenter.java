package hk.com.cybersys.activity;


import hk.com.cybersys.basic.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class IndividualCenter extends Activity {
	
	private LinearLayout myCenterBar = null;
	private ImageButton myHomePage = null;
	private ImageButton myCollection = null;
	private ImageButton myProducts = null;
	private ImageButton myCenter = null;
	
	private TextView my_account = null;
	private TextView my_loginNmae = null;
	private Button my_login_but = null;
	
	private SharedPreferences settings;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_individual_center);
		
		settings = getSharedPreferences("LoginStatus", 0);
		String isLogin = settings.getString("isLogin", "");
		
		
//		this.myHomePage = (ImageButton)this.myCenterBar.findViewById(R.id.myHomePage);
//		this.myCollection = (ImageButton)this.myCenterBar.findViewById(R.id.myCollection);
//		this.myProducts = (ImageButton)this.myCenterBar.findViewById(R.id.myProducts);
//		this.myCenter = (ImageButton)this.myCenterBar.findViewById(R.id.myCenter);
//		this.myCenter.setBackgroundResource(R.drawable.accouted);
//		this.myHomePage.setOnClickListener(new MyButtonListener());
//		this.myCollection.setOnClickListener(new MyButtonListener());
//		this.myProducts.setOnClickListener(new MyButtonListener());
//		this.myCenter.setOnClickListener(new MyButtonListener());
		
		this.my_loginNmae = (TextView)findViewById(R.id.myName);
		this.my_account = (TextView)findViewById(R.id.my_account);
		this.my_loginNmae.setText("你好，欢迎使用金元宝！");
		this.my_account.setText("账号：" + "1234567890");
		
//		this.my_login_but = (Button)findViewById(R.id.my_login_but);
//		
//		this.my_login_but.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				Button b = (Button)v;
//				if(b.getText().equals("退出登陆")){
//					my_loginNmae.setText("你好，欢迎使用金元宝！");
//					my_account.setText("账号：" + "1234567890");
//					my_login_but.setText("登陆");
//					settings.edit().clear().commit();
//				}else{
//					startActivity(new Intent(IndividualCenter.this,Login.class));
//					IndividualCenter.this.finish();
//				}
//			}
//		});
//		//新页面接收数据
//        Bundle bundle = this.getIntent().getExtras();
//        if(bundle != null){
//        	//接收name值
//			String name = bundle.getString("name");
//			this.my_account.setText("账号：" + settings.getString("account", ""));
//			this.my_loginNmae.setText(settings.getString("name", ""));
//			this.my_login_but.setText("退出登陆");
//        }else{
//        	this.my_account.setText("账号：123456789");
//        }
       
		if(isLogin.equals("yes")){
			this.my_account.setText("账号：" + settings.getString("account", ""));
			this.my_loginNmae.setText(settings.getString("name", ""));
			this.my_login_but.setText("退出登陆");
		}else{
			
		}
		
	}
	public class MyButtonListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent intent = new Intent();
			switch(v.getId()){
			case R.id.myHomePage:
				intent.setClass(IndividualCenter.this,MainActivity.class);
				startActivity(intent);
				break;
			case R.id.myCollection:
				//intent.setClass(IndividualCenter.this,Collection.class);
				//startActivity(intent);
				break;
			case R.id.myProducts:
				intent.setClass(IndividualCenter.this,MapReservation.class);
				startActivity(intent);
				break;
			case R.id.myCenter:
				intent.setClass(IndividualCenter.this,IndividualCenter.class);
				startActivity(intent);
				break;
			}
			overridePendingTransition(R.anim.fade, R.anim.hold);
			IndividualCenter.this.finish();
		}
		
	}
	public void onClick(View v){
		switch(v.getId()){
		case R.id.individual_center_account_information:
			startActivity(new Intent(IndividualCenter.this,IndividualCenterInformation.class));
			break;
		case R.id.qr_code_iv:
			ImageView img =  new ImageView(IndividualCenter.this);  
			img.setImageResource(R.drawable.qr_show);  
			new  AlertDialog.Builder(IndividualCenter.this)  
			.setView(img)  
			.show();  
			break;
		case R.id.setting:
			startActivity(new Intent(IndividualCenter.this,Setting.class));
			break;
		case R.id.myTradingBut:
			startActivity(new Intent(IndividualCenter.this,MyTrading.class));
			break;
		case R.id.newMsg:
			startActivity(new Intent(IndividualCenter.this,NewMsg.class));
			break;
		case R.id.myAppointmentsBut:
			startActivity(new Intent(IndividualCenter.this,AppointmentRecord.class));
			break;
			default:
				Toast.makeText(this, "此功能待待开发。。。。。", Toast.LENGTH_LONG).show();
				break;
		}
		
	}

}
