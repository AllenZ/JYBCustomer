package hk.com.cybersys.activity;


import hk.com.cybersys.basic.R;
import hk.com.cybersys.http.AsyncHttpClient;
import hk.com.cybersys.http.AsyncHttpResponseHandler;
import hk.com.cybersys.http.HCPostRequest;
import hk.com.cybersys.http.JsonHttpResponseHandler;
import hk.com.cybersys.http.RequestParams;
import hk.com.cybersys.myLibrary.CommonWidget;
import hk.com.cybersys.myLibrary.Constant;
import internal.org.apache.http.entity.mime.MultipartEntity;
import internal.org.apache.http.entity.mime.content.FileBody;
import internal.org.apache.http.entity.mime.content.StringBody;
import org.apache.http.Header;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cao.jian.chen.utils.Constants;
import cao.jian.chen.utils.Utils;

import com.tencent.android.tpush.horse.Tools;

public class IndividualCenterInformation extends Activity {

	private Handler informationHandler;
	private TextView txt_title;
	private ImageView backImg;

	private TextView information_customer_name, information_account,
			information_phone, information_gender, information_occupation;
	private TextView information_income, information_address,
			information_address_arrow, information_emaill, information_wechat;
	private JSONArray jsonArrayData;
	private ImageView backToindividual,Img_UserIcon;

	// 客户的ID
	private String customerID;
	// 是否已经登录
	private String isLogin;

	private Builder alertDialog;

	private EditText inputET;

	private String field;

	private LayoutInflater inflater;
	private View input_dialog_v;
	private TextView inputDialogTitle;
	private Button inputDialogQD, inputDialogQX, sexChangerButQD,
			sexChangerButQX;

	private RelativeLayout upload_userIcon;
	private RadioGroup genderRadioGroup;
	private RadioButton male, female;
	private int sexCode;
	private String sexStr;
	private String[] iconItems=new String[]{"本地图片","拍照"};
	//头像名称
	private static final String IMAGE_FILE_NAME="UserIcon.jpg";
	//请求码
	private static final int IMAGE_REQUEST_CODE = 0;
	private static final int CAMERA_REQUEST_CODE = 1;
	private static final int RESULT_REQUEST_CODE = 2;
	private Bitmap bitmap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.activity_individual_center_information);

		initTitle();
		this.information_customer_name = (TextView) findViewById(R.id.information_customer_name);
		//this.information_account = (TextView) findViewById(R.id.information_account);
		this.information_phone = (TextView) findViewById(R.id.information_phone);
		this.information_gender = (TextView) findViewById(R.id.information_gender);
		this.information_occupation = (TextView) findViewById(R.id.information_occupation);
		this.information_income = (TextView) findViewById(R.id.information_income);
		this.information_address = (TextView) findViewById(R.id.information_address);
		this.information_emaill = (TextView) findViewById(R.id.information_emaill);
		this.information_wechat = (TextView) findViewById(R.id.information_wechat);
		upload_userIcon=(RelativeLayout) findViewById(R.id.upload_userIcon);
		Img_UserIcon=(ImageView) findViewById(R.id.Img_UserIcon);
		//this.backToindividual = (ImageView) findViewById(R.id.backToIndividual);
	
		customerID = Utils.getValue(this, Constants.CustomerID);

		if (!(Utils.getBooleanValue(this,Constants.LoginState ))) {
			Toast.makeText(getApplicationContext(), "请登陆后，再查看个人信息！",
					0).show();
			Intent intent = new Intent();
			intent.setClass(IndividualCenterInformation.this, Login.class);
			startActivity(intent);
			MainActivity.mActivity.finish();
			this.finish();
		} else {
			Runnable r = new GetInformationThread(customerID);
			new Thread(r).start();

			CommonWidget.myProgressDialogShow(IndividualCenterInformation.this,
					"正在加载...", "", false);
		}
		informationHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				CommonWidget.progressDialog.dismiss();
				switch (msg.what) {
				case 1:
					try {
						if(jsonArrayData.getJSONObject(0).getString("gender")
								.equals("0")) {
							sexStr = "女";
						} else {
							sexStr = "男";
						}
						information_customer_name.setText(jsonArrayData
								.getJSONObject(0).getString("name"));
						Utils.RemoveValue(getApplicationContext(), Constants.UserName);
						Utils.putValue(getApplicationContext(), Constants.UserName, jsonArrayData
								                                  .getJSONObject(0).getString("name"));
						/*information_account.setText(jsonArrayData
								.getJSONObject(0).getString("customerid"));*/
						information_phone.setText(jsonArrayData
								.getJSONObject(0).getString("mobile"));
						information_gender.setText(sexStr);
						System.out.println(jsonArrayData.getJSONObject(0)
								.getString("career") + "--------");
						if (jsonArrayData.getJSONObject(0).getString("career")
								.equals(null)) {
							information_occupation.setText(jsonArrayData
									.getJSONObject(0).getString("career"));
						}else {
							information_occupation.setText("未填写");
						}
						if (jsonArrayData.getJSONObject(0)
								.getString("salaryincome").equals(null)) {
							information_income
									.setText(jsonArrayData.getJSONObject(0)
											.getString("salaryincome"));

						} else {
							information_income.setText("未填写");

						}

						if (jsonArrayData.getJSONObject(0)
								.getString("addressid").equals(null)) {
							information_address.setText(jsonArrayData
									.getJSONObject(0).getString("addressid"));

						} else {
							information_address.setText("未填写");

						}

						if (jsonArrayData.getJSONObject(0).getString("email")
								.equals(null)) {
							information_emaill.setText(jsonArrayData
									.getJSONObject(0).getString("email"));

						} else {
							information_emaill.setText("未填写");

						}

						if (jsonArrayData.getJSONObject(0).getString("wechat")
								.equals(null)) {
							information_wechat.setText(jsonArrayData
									.getJSONObject(0).getString("wechat"));
						} else {
							information_wechat.setText("未填写");

						}
						// information_occupation.setText(jsonArrayData.getJSONObject(0).getString("career"));
						// information_income.setText(jsonArrayData.getJSONObject(0).getString("salaryincome"));
						// information_address.setText(jsonArrayData.getJSONObject(0).getString("addressid"));
						// information_emaill.setText(jsonArrayData.getJSONObject(0).getString("email"));
						// information_wechat.setText(jsonArrayData.getJSONObject(0).getString("wechat"));

					} catch (JSONException e) {
						e.printStackTrace();
					}
					break;
				case 2:
					break;
				default:
					break;
				}
				super.handleMessage(msg);
			}

		};

		super.onCreate(savedInstanceState);
	}

	public void onClick(View v) {
		String title = null;
		String viewSwitch = "";
		switch (v.getId()) {
		case R.id.information_customer_name_layout:
			title = "请输入名字";
			field = "name";
			break;
		/*case R.id.information_account_layout:
			// title = "请输入金子号";
			Toast.makeText(IndividualCenterInformation.this, "金子号不可更改",
					Toast.LENGTH_LONG).show();
			return;*/
		case R.id.information_phone_layout:
			// title = "请输入手机号";
			// field = "mobile";
			Toast.makeText(IndividualCenterInformation.this, "手机号码不可更改",
					Toast.LENGTH_LONG).show();
			//修改手机号，跳转到ModifyPhoneActivity
//			Intent myIntent=new Intent();
//			myIntent.setClass(IndividualCenterInformation.this, ModifyPhoneActivity.class);
//			startActivity(myIntent);
			return;
		case R.id.information_gender_layout:
			title = "请选择性别";
			viewSwitch = "sex";
			field = "gender";
			break;
		case R.id.information_occupation_layout:
			title = "请输入职业";
			field = "career";
			break;
		case R.id.information_income_layout:
			title = "请输入收入";
			field = "salaryincome";
			break;
		case R.id.information_address_layout:
			title = "请输入地址";
			break;
		case R.id.information_emaill_layout:
			title = "请输入Email";
			field = "email";
			break;
		case R.id.information_wechat_layout:
			title = "请输入微信号";
			field = "wechat";
			break;
		}
		inflater = LayoutInflater.from(IndividualCenterInformation.this);
		if (viewSwitch.equals("sex")) {
			input_dialog_v = inflater.inflate(R.layout.sex_selection, null);// 得到加载view
			genderRadioGroup = (RadioGroup) input_dialog_v
					.findViewById(R.id.changerSexradioGroup);
			male = (RadioButton) genderRadioGroup.findViewById(R.id.sexRadio0);
			female = (RadioButton) genderRadioGroup
					.findViewById(R.id.sexRadio1);
			sexChangerButQD = (Button) input_dialog_v
					.findViewById(R.id.sexChangerButQD);
			sexChangerButQX = (Button) input_dialog_v
					.findViewById(R.id.sexChangerButQX);
			sexChangerButQD
					.setOnClickListener(new alertDialogLiOnClickListener());
			sexChangerButQX
					.setOnClickListener(new alertDialogLiOnClickListener());
			CommonWidget.myCustomDialog(IndividualCenterInformation.this,
					input_dialog_v, false);
		} else {
			input_dialog_v = inflater.inflate(R.layout.input_dialog, null);// 得到加载view
			inputET = (EditText) input_dialog_v
					.findViewById(R.id.inputDialogEditText);
			inputDialogTitle = (TextView) input_dialog_v
					.findViewById(R.id.inputDialogTitle);
			inputDialogQD = (Button) input_dialog_v
					.findViewById(R.id.inputDialogQD);
			inputDialogQX = (Button) input_dialog_v
					.findViewById(R.id.inputDialogQX);
			inputDialogQD
					.setOnClickListener(new alertDialogLiOnClickListener());
			inputDialogQX
					.setOnClickListener(new alertDialogLiOnClickListener());
			inputDialogTitle.setText(title);
			CommonWidget.myCustomDialog(IndividualCenterInformation.this,
					input_dialog_v, false);
		}
	}

	class alertDialogLiOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			CommonWidget.loadingDialog.dismiss();
			switch (v.getId()) {
			case R.id.inputDialogQD:
				Runnable r = new CommitChangesThread("customerid=" + customerID
						+ "&" + field + "=" + inputET.getText());
				new Thread(r).start();
				// CommonWidget.myProgressDialogShow(IndividualCenterInformation.this,
				// "正在提交数据...", "", false);
				break;
			case R.id.sexChangerButQD:
				if (male.isChecked()) {
					sexCode = 1;
				} else if (female.isChecked()) {
					sexCode = 0;
				}
				Runnable sex = new CommitChangesThread("customerid="
						+ customerID + "&" + field + "=" + sexCode);
				new Thread(sex).start();
				// CommonWidget.myProgressDialogShow(IndividualCenterInformation.this,
				// "正在提交数据...", "", false);
				break;
			case R.id.inputDialogQX:
				break;
			}
		}
	}

	/*
	 * 获取个人信息
	 */
	class GetInformationThread implements Runnable {
		private String param;

		public GetInformationThread(String customerID) {
			this.param = customerID;
		}

		@Override
		public void run() {
			String returnData = HCPostRequest
					.sendPost(Constant.URL_CUSTOMER_GET_BY_ID, "customerid="
							+ this.param);
			System.out.println("獲取個人信息:" + returnData);
			JSONObject jsonOb = null;
			try {
				jsonOb = new JSONObject(returnData);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if (jsonOb.getString("success").equals("1")) {
					// 通过customer返回Json数据
					jsonArrayData = jsonOb.getJSONArray("customer");
					// System.out.println(jsonArrayData+"---------------");
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Message msg = new Message();
			msg.what = 1;
			IndividualCenterInformation.this.informationHandler
					.sendMessage(msg);
		}
	}

	/*
	 * 修改个人信息
	 */
	class CommitChangesThread implements Runnable {

		private String parameter;

		public CommitChangesThread(String param) {
			this.parameter = param;
		}

		@Override
		public void run() {
			Looper.prepare();
			String returnData = HCPostRequest.sendPost(
					Constant.URL_CUSTOMER_EDIT, this.parameter);
			JSONObject jsonOb = null;
			try {
				jsonOb = new JSONObject(returnData);
				if (jsonOb.getString("success").equals("1")) {
					Runnable r = new GetInformationThread(customerID);
					new Thread(r).start();
					Message msg = new Message();
					msg.what = 1;
					IndividualCenterInformation.this.informationHandler
							.sendMessage(msg);
					CommonWidget.myProgressDialogShow(
							IndividualCenterInformation.this, "正在加载...", "",
							false);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Looper.loop();

		}

	}

	/**
	 * @author Allen 左上角的图片返回
	 */
	@Override
	protected void onStart(){
		
		super.onStart();
		//点击上传头像
		upload_userIcon.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showMyDialog();
			}
		});
	}
	private void showMyDialog(){
		new AlertDialog.Builder(this)
		    .setTitle("设置头像")
		    .setItems(iconItems, new DialogInterface.OnClickListener() {			
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					switch (which) {
					case 0:
						Intent intentFromGallery=new Intent();
						intentFromGallery.setType("image/*"); // 设置文件类型
						intentFromGallery
								.setAction(Intent.ACTION_GET_CONTENT);
						startActivityForResult(intentFromGallery,
								IMAGE_REQUEST_CODE);
						break;
					case 1:
						Intent intentFromCapture = new Intent(
								MediaStore.ACTION_IMAGE_CAPTURE);
						// 判断存储卡是否可以用，可用进行存储
						if(Utils.hasSdcard()){
							intentFromCapture.putExtra(
									MediaStore.EXTRA_OUTPUT,
									Uri.fromFile(new File(Environment
											.getExternalStorageDirectory(),
											IMAGE_FILE_NAME)));
						}
						startActivityForResult(intentFromCapture,
								CAMERA_REQUEST_CODE);
						break;
					default:
						break;
					}
				}
			})
			.setNegativeButton("取消", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			}).show();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		//结果码不等于取消时候
		if (resultCode != RESULT_CANCELED) {
			switch (requestCode) {
			case IMAGE_REQUEST_CODE://从图库中截取
				startPhotoZoom(data.getData());
				break;
			case CAMERA_REQUEST_CODE://从相机中截取
				if (Utils.hasSdcard()) {
					File tempFile = new File(
							Environment.getExternalStorageDirectory()
									+ IMAGE_FILE_NAME);
					startPhotoZoom(Uri.fromFile(tempFile));
				} else {
					Toast.makeText(IndividualCenterInformation.this, "未找到存储卡，无法存储照片！",
							Toast.LENGTH_LONG).show();
				}

				break;
			case RESULT_REQUEST_CODE:
				if (data != null) {
					getImageToView(data);
				}
				break;
			} 
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * 裁剪图片方法实现
	 * 
	 * @param uri
	 */
	public void startPhotoZoom(Uri uri) {

		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, "image/*");
		// 设置裁剪
		intent.putExtra("crop", "true");
		// aspectX aspectY 是宽高的比例
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		// outputX outputY 是裁剪图片宽高
		intent.putExtra("outputX", 320);
		intent.putExtra("outputY", 320);
		intent.putExtra("return-data", true);
		startActivityForResult(intent, 2);
	}

	/**
	 * 保存裁剪之后的图片数据
	 * 
	 * @param picdata
	 */
	private void getImageToView(Intent data) {
		Bundle extras = data.getExtras();
		if (extras != null) {
			Bitmap photo = extras.getParcelable("data");
			//将图片上传到服务器,先压缩
			double maxSize =400.00; 
			 //将bitmap放至数组中，意在bitmap的大小（与实际读取的原文件要大）   
            ByteArrayOutputStream baos = new ByteArrayOutputStream(); 
            photo.compress(Bitmap.CompressFormat.JPEG, 70, baos); 
            byte[] b = baos.toByteArray();
            System.out.println("压缩前图片的大小："+b.length);
            //将字节换成KB 
            double mid = b.length/1024;
            if (mid > maxSize) { 
                //获取bitmap大小 是允许最大大小的多少倍 
               double i = mid / maxSize; 
                //开始压缩  此处用到平方根 将宽带和高度压缩掉对应的平方根倍 （1.保持刻度和高度和原bitmap比率一致，压缩后也达到了最大大小占用空间的大小） 
               photo=zoomImage(photo,photo.getWidth()/Math.sqrt(i),photo.getHeight()/Math.sqrt(i));      
            } 	
            uploadUserIcon(photo);			
			Img_UserIcon.setImageBitmap(photo);
		}	
	}
	//图片压缩
	
	public Bitmap zoomImage(Bitmap bgimage, double newWidth, 
                double newHeight) { 
        // 获取这个图片的宽和高 
        float width = bgimage.getWidth(); 
        float height = bgimage.getHeight(); 
        // 创建操作图片用的matrix对象 
        Matrix matrix = new Matrix(); 
        // 计算宽高缩放率 
        float scaleWidth = ((float) newWidth) / width; 
        float scaleHeight = ((float) newHeight) / height; 
        // 缩放图片动作 
        matrix.postScale(scaleWidth, scaleHeight); 
        Bitmap bitmap = Bitmap.createBitmap(bgimage, 0, 0, (int) width, 
                        (int) height, matrix, true); 
        return bitmap; 
	}
	//上传头像到服务器
	private void uploadUserIcon( Bitmap bitmap){
    	
    	 ByteArrayOutputStream stream = new ByteArrayOutputStream();
    	 bitmap.compress(Bitmap.CompressFormat.JPEG, 10, stream);
    	 try {
			stream.close();
		 } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
         byte[] bytes = stream.toByteArray();
         System.out.println("压缩后图片大小："+bytes.length);
         //将图片的字节流数据加密成base64字符输出
         String img = new String(Base64.encodeToString(bytes, Base64.DEFAULT));

         AsyncHttpClient client = new AsyncHttpClient();
         RequestParams params = new RequestParams();
         params.add("usertype", new String("customer"));
         params.add("userid", customerID);
         params.add("photo", img);
         client.post(Constant.URL_UPLOAD_CUSTOMER_ICON,
   		      params, new AsyncHttpResponseHandler() {  	 
             @Override
             public void onSuccess(int i, Header[] headers, byte[] bytes) {
                Toast.makeText(IndividualCenterInformation.this, "Upload Success!", Toast.LENGTH_LONG).show();
             }
              @Override
             public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                Toast.makeText(IndividualCenterInformation.this, "Upload Fail!", Toast.LENGTH_LONG).show();
             }    
       });       
    }
		
	//初始化Title
	private void initTitle() {
		backImg=(ImageView) findViewById(R.id.img_back);
		backImg.setVisibility(View.VISIBLE);
		backImg.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			//System.out.println("哈哈哈哈！");
			//LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(IndividualCenterInformation.this);
			
			sendBrodcast("modify");
			Utils.finish(IndividualCenterInformation.this);
	      }
		});
		findViewById(R.id.txt_right).setVisibility(View.GONE);
		txt_title = (TextView) findViewById(R.id.txt_title);
	    txt_title.setText("个人信息");
	}
	
	//发送广播,更改信息后
    private void sendBrodcast(String Action){
			Intent intent = new Intent();
			intent.setAction("hk.com.cybersys.basic.IndividualCenterInformation");
			intent.putExtra("Action",Action);
			sendBroadcast(intent);
		}
}
