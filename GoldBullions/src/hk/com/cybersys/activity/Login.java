package hk.com.cybersys.activity;


import hk.com.cybersys.basic.R;
import hk.com.cybersys.http.HCPostRequest;
import hk.com.cybersys.myLibrary.CommonWidget;
import hk.com.cybersys.myLibrary.Constant;
import hk.com.cybersys.myinterface.FragementCallBack;
import hk.com.cybersys.otheractivity.PhoneCheck;
import hk.com.cybersys.view.PersonalCenterFragment;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import cao.jian.chen.utils.Constants;
import cao.jian.chen.utils.Utils;

import com.tencent.android.tpush.XGPushConfig;

public class Login extends FragmentActivity {

	private Button loginBut,registerBut;

	private String params;
	private String param;

	private JSONArray jsonArrayData;
	private Handler handler = null;
	private boolean isRegisterSuccess;
	private String account;
	private String password;
	private String token;
	private String returnData;
	private Intent intent;
	private int activityNum;
	private PersonalCenterFragment personFragment;
	
	private LoginActivityListener myLogin;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		this.loginBut = (Button) findViewById(R.id.login_but);
		this.loginBut.setOnClickListener(new MyClickListener());
		this.registerBut = (Button)findViewById(R.id.to_register_but);
		this.registerBut.setOnClickListener(new MyClickListener());
		intent=getIntent();
		handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				CommonWidget.progressDialog.dismiss();
				super.handleMessage(msg);
			}
		};
		if(intent.getBooleanExtra("isRegisterSuccess", false)){
			autoLogin();
		}
		
	}
	
	public class MyClickListener implements OnClickListener {
		private EditText etName;
		private EditText etPassword;
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.login_but:
				this.etName = (EditText) findViewById(R.id.my_login_name);
				this.etPassword = (EditText) findViewById(R.id.password);
				
				String regEx = "(\\(\\d{3,4}\\)|\\d{3,4}-|\\s)?\\d{7,14}";
				Pattern pattern = Pattern.compile(regEx);
				Matcher matcher = pattern.matcher(etName.getText());
				boolean b = matcher.matches();
				if(!b){
					Toast.makeText(Login.this, "手机号码有误，请重新输入！", Toast.LENGTH_SHORT).show();
					return;
				}
				if(etPassword.getText().toString().trim().length() == 0){
					Toast.makeText(Login.this, "密码不能为空，请重新输入！", Toast.LENGTH_SHORT).show();
					return;
				}
				CommonWidget.myProgressDialogShow(Login.this, "正在登陆，请稍后！", "", false);
				account = this.etName.getText().toString().trim();
				password = this.etPassword.getText().toString().trim();
				token=XGPushConfig.getToken(getApplicationContext());
				params = "mobile=" + account + "&" + "password=" + password;
			   
				
			    Runnable r = new LoginThread(handler);
				new Thread(r).start();
				break;
			case R.id.to_register_but:
				Intent intent = new Intent();
				intent.setClass(Login.this, PhoneCheck.class);
				startActivity(intent);
				break;
			}
			
		}		
	}
	
	private void autoLogin(){
	      System.out.println(intent.getStringExtra("autoParams")+"2345678");
		  params=intent.getStringExtra("autoParams");
		  account=intent.getStringExtra("account");
		  if(params!=null){
			Runnable r = new LoginThread(handler);
			new Thread(r).start();
		  }
	}
	
	class LoginThread implements Runnable {
		private Handler handler;
		private SharedPreferences loginStatus;
		private SharedPreferences.Editor editor;

		public LoginThread(Handler handler) {
			this.handler = handler;
		}

		@Override
		public void run() {
			Looper.prepare();
			
			returnData = HCPostRequest.sendPost(Constant.URL_CUSTOMER_LOGIN,params);
						
			try {
				JSONObject jsonOb = new JSONObject(returnData);
				if (jsonOb.getString("success").equals("1")) {					
					param="customerid="+jsonOb.getString("customerid")+"&"+"token="+XGPushConfig.getToken(getApplicationContext());		
					String returnToken=HCPostRequest.sendPost(Constant.URL_CUSTOMER_ADD_TOKEN,param);
					JSONObject jsonObject = new JSONObject(returnToken);
					if(jsonObject.getString("success").equals("1")){
						System.out.println("註冊TOKEN成功！");
					}else{
						return;
					}
					Toast.makeText(getApplicationContext(), "登陆成功",Toast.LENGTH_SHORT).show();
					Intent myIntent = new Intent();
					//Bundle bundle = new Bundle();
					//bundle.putString("account", account);
					//myIntent.putExtras(bundle);
					//2代表从收藏界面到登录！
					/*if(activityNum==2){
						//myIntent.setClass(Login.this,Collection.class);
						//startActivity(myIntent);
					}else if(activityNum==3){
						//3代表从产品列表的收藏显示到登录，登录后在跳转！
						myIntent.setClass(Login.this,ProductsShow.class);
						startActivity(myIntent);
					}
					else{
						Utils.finish(Login.this);
					   // myIntent.setClass(Login.this,ProductsClass.class);
					   //startActivity(myIntent);
					}*/
					//保存登陆状态和用户ID
					personFragment=new PersonalCenterFragment();
					Bundle bundle = new Bundle();
					bundle.putString("account", account);
					bundle.putString("userName", jsonOb.getString("name"));
					bundle.putString("custometID",  jsonOb.getString("customerid"));
					bundle.putString("isLogin", "yes");					
					Utils.putBooleanValue(Login.this, Constants.LoginState,true);
					Utils.putValue(Login.this, Constants.CustomerID, jsonOb.getString("customerid"));
					Utils.putValue(Login.this,Constants.Account, account);
					Utils.putValue(Login.this, Constants.UserName,jsonOb.getString("name") );
					Utils.putValue(Login.this, Constants.PWD, password);
								
					myIntent.setClass(Login.this,MainActivity.class);
					
					
					//Login.this.setResult(1, myIntent);
					MainActivity.mActivity.finish();
					//startActivityForResult(myIntent, 1);
					startActivity(myIntent);
					//退出Activity
					Login.this.finish();
										
				} else {
					Toast.makeText(getApplicationContext(), "登陆失败,请检查账号和密码是否正常",Toast.LENGTH_SHORT).show();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.handler.sendEmptyMessage(0);
			Looper.loop();
		}

	}
	
	public interface LoginActivityListener{
		void loginCallBack();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, intent);
	}
	
}
