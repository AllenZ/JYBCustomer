package hk.com.cybersys.activity;

import hk.com.cybersys.basic.R;
import hk.com.cybersys.view.AppointmentFragment;
import hk.com.cybersys.view.CollectionFragment;
import hk.com.cybersys.view.PersonalCenterFragment;
import hk.com.cybersys.view.ProductFragment;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import cao.jian.chen.utils.App;
import cn.smssdk.SMSSDK;

import com.tencent.android.tpush.XGPushManager;
import com.tencent.android.tpush.service.XGPushService;

public class MainActivity extends FragmentActivity implements OnClickListener,PersonalCenterFragment.onLogoutListener{
	
	private TextView txt_title;
	private ImageView img_right;
	private Fragment[] fragments;
	private ProductFragment productFragment;
	private CollectionFragment collectionFragment;
	private PersonalCenterFragment personFragment;
	private AppointmentFragment appointmentFragment;
	private ImageView[] imagebuttons;
	private TextView[] textviews;
	private int index;
	private int currentTabIndex;// 当前fragment的index
	public static Activity mActivity;
	MyBroadcastReceiver receiver;
	
	// private Button
	// myPrivateEquity,myTrustProducts,myAssetManagement,myRecommendations;
//--------------ProductFrame后用到的的逻辑
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_product_first);
		App.getInstance2().addActivity(this);
		mActivity=this;	
		findViewById();
		initTabView();
		
	    
		//信鸽代码
		Context context = getApplicationContext();// 獲得上下文
		XGPushManager.registerPush(context); // 註冊上下文
	   
		// 2.36（不包括）之前的版本需要调用以下2行代码
		Intent service = new Intent(context, XGPushService.class);
		context.startService(service);		
	}
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		// 注册广播,接受收藏列表改变后的值
		receiver=new MyBroadcastReceiver();
		registerReceiver(receiver, new IntentFilter("hk.com.cybersys.basic.collection"));
		registerReceiver(receiver, new IntentFilter("hk.com.cybersys.basic.IndividualCenterInformation"));		
		super.onStart();				
	}
	//初始化一级导航
	private void initTabView(){
		productFragment=new ProductFragment();
		collectionFragment=new CollectionFragment();
		appointmentFragment=new AppointmentFragment();
		personFragment=new PersonalCenterFragment();
		//fragments=new Fragment[4];
		//fragments[0]=productFragment;
		fragments=new Fragment[]{productFragment,collectionFragment,
			                     appointmentFragment,personFragment
	    };
		imagebuttons = new ImageView[4];
		imagebuttons[0] = (ImageView) findViewById(R.id.ib_weixin);
		imagebuttons[1] = (ImageView) findViewById(R.id.ib_contact_list);
		imagebuttons[2] = (ImageView) findViewById(R.id.ib_find);
		imagebuttons[3] = (ImageView) findViewById(R.id.ib_profile);
		imagebuttons[0].setSelected(true);
		textviews = new TextView[4];
		textviews[0] = (TextView) findViewById(R.id.tv_weixin);
		textviews[1] = (TextView) findViewById(R.id.tv_contact_list);
		textviews[2] = (TextView) findViewById(R.id.tv_find);
		textviews[3] = (TextView) findViewById(R.id.tv_profile);
		textviews[index].setTextColor(Color.rgb(0,122,255));
		//textviews[0].setTextColor(0xFF45C01A);
		//设置默认的 Fragment()
		//setDefaultFragment();
		// 添加显示第一个fragment
		getSupportFragmentManager().beginTransaction()
		    	.add(R.id.fragment_container, productFragment)
				.add(R.id.fragment_container, collectionFragment)
				.add(R.id.fragment_container, appointmentFragment)
				.add(R.id.fragment_container, personFragment)
				.hide(collectionFragment).hide(appointmentFragment)
				.hide(personFragment).show(productFragment)
			    .commit();
	}
	//设置默认的Fragment
	public void setDefaultFragment(){
		 /*FragmentManager fm =getSupportFragmentManager();  
	     FragmentTransaction transaction = fm.beginTransaction();
	     transaction.replace(R.id.fragment_container, productFragment);
	     imagebuttons[0].setSelected(true);
	     transaction.commit();*/
	   	 imagebuttons[currentTabIndex].setSelected(true);
	     imagebuttons[0].setSelected(true);
		 textviews[0].setTextColor(0xFF999999);
		 textviews[0].setTextColor(0xFF45C01A);	     
	}
	//给一级导航设置监听事件
	public void onTabClicked(View view) {	
		switch (view.getId()) {
		case R.id.re_weixin:
			index = 0;
			if (productFragment == null) {
				productFragment=new ProductFragment();
			} 
			txt_title.setText("财富365");
			break;
		case R.id.re_contact_list:
			index = 1;
			txt_title.setText("收藏");	
			break;
		case R.id.re_find:
			index = 2;
			txt_title.setText("预约");
			break;
		case R.id.re_profile:
			index = 3;
			txt_title.setText("我");
			break;
		}
		if (currentTabIndex != index) {
			FragmentTransaction trx = getSupportFragmentManager()
					.beginTransaction();
			trx.hide(fragments[currentTabIndex]);
			if (!fragments[index].isAdded()) {
				trx.add(R.id.fragment_container, fragments[index]);
			}
			trx.show(fragments[index]).commit();
		}
		//transaction.commit();
		imagebuttons[currentTabIndex].setSelected(false);
		// 把当前tab设为选中状态
		imagebuttons[index].setSelected(true);
		textviews[currentTabIndex].setTextColor(0xFF999999);
		textviews[index].setTextColor(Color.rgb(0,122,255));
		//textviews[index].setTextColor(0xFF45C01A);
		currentTabIndex = index;
	}
		
	//初始化上方的控件
	private void findViewById() {
		txt_title =  (TextView) findViewById(R.id.txt_title);
		//img_right = (ImageView) findViewById(R.id.img_right);
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
	
	//Logout的回调刷新
	@Override
	public void refCollectFragment() {
		// TODO Auto-generated method stub	
		refCollection();
	}
    
	//刷新收藏的Fragment,用于Logout和增加收藏
	public void refCollection(){
		if(collectionFragment!=null){
			System.out.println("这是refCollectFragment上的回调函数1");
			collectionFragment.refresh();
		}else{
			System.out.println("这是refCollectFragment上的回调函数2");
			collectionFragment=new CollectionFragment();
			FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			transaction.replace(R.id.fragment_container, collectionFragment);
			transaction.commit();
		}
	}   
	/*
	 * 收藏数据的返回监听，广播
	 * @Author by AllenZ
	 * @Time 2015-8-6
	 */
    class MyBroadcastReceiver extends BroadcastReceiver{
	   @Override
	    public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		   System.out.println("这是MainActivity的MyBroadcast上的输出");
		   String action=intent.getAction();
		   System.out.println("action:"+action);
		   if(action.equals("hk.com.cybersys.basic.collection")){
			   System.out.println("收藏刷新了");
			   collectionFragment.refresh();
		   }else if(action.equals("hk.com.cybersys.basic.IndividualCenterInformation")){
			   System.out.println("個人中心刷新了");
			   personFragment.refresh();
		   }  
	   }		
	}  
    @Override
    protected void onDestroy() {
    	// TODO Auto-generated method stub
    	unregisterReceiver(receiver);
    	super.onDestroy();
    }
    @Override
    protected void onStop() {
    	// TODO Auto-generated method stub
    	super.onStop();
    	
    }
}
