package hk.com.cybersys.activity;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import hk.com.cybersys.basic.R;
import hk.com.cybersys.http.HCPostRequest;
import cao.jian.chen.utils.Constants;
import cao.jian.chen.utils.Utils;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BaiduMap.OnMapClickListener;
import com.baidu.mapapi.map.BaiduMap.OnMapTouchListener;
import com.baidu.mapapi.map.BaiduMap.OnMarkerClickListener;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapPoi;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.model.LatLng;
import com.baidu.navisdk.util.SysOSAPI;
import com.baidu.nplatform.comapi.map.MapController;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MapReservation extends Activity {

	private WebView wv;
	MapView mMapView = null;
	BaiduMap bMap = null;
	// 定位相关
	LocationClient mLocClient;
	public MyLocationListener myListener = new MyLocationListener();
	boolean isFirstLoc = true;// 是否首次定位
	
	private LinearLayout myHomePageBar = null;
	private ImageButton myHomePage = null;
	private ImageButton myCollection = null;
	private ImageButton myProducts = null;
	private ImageButton myCenter = null;
	private Button mBtnReservation;
	
	private long exitTime = 0;
	private String brokerStr;
	public TextView mapBrokerName;
	public TextView mapBrokerAddress;
	public RelativeLayout brokerRelativeLayout;
	
	private String brokerName;
	private String isSelectedBrokerID;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// 在使用SDK各组件之前初始化context信息，传入ApplicationContext
		// 注意该方法要再setContentView方法之前实现
		SDKInitializer.initialize(getApplicationContext());
		setContentView(R.layout.activity_map_appointment);
		// 获取地图控件引用
		mMapView = (MapView) findViewById(R.id.bmapView);
		bMap = mMapView.getMap();
		// 开启定位图层
		bMap.setMyLocationEnabled(true);
		this.mLocClient = new LocationClient(this);
		this.mLocClient.registerLocationListener(this.myListener);
		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(true);// 打开gps
		option.setCoorType("bd09ll"); // 设置坐标类型
		option.setScanSpan(1000);
		mLocClient.setLocOption(option);
		mLocClient.start();
		
		brokerRelativeLayout = (RelativeLayout)findViewById(R.id.showBroker);
		//mBtnReservation=(Button) findViewById(R.id.btn_reservation);
		this.mapBrokerName = (TextView)this.brokerRelativeLayout.findViewById(R.id.mapBrokerName);
		this.mapBrokerAddress = (TextView)this.brokerRelativeLayout.findViewById(R.id.mapBrokerAddress);
		brokerRelativeLayout.setVisibility(View.GONE);
		Button but = (Button)findViewById(R.id.appointment_button);
		but.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				SharedPreferences settings = getSharedPreferences("LoginStatus", 0);
				String isLogin = settings.getString("isLogin", "");
				if(Utils.getBooleanValue(MapReservation.this,Constants.LoginState )){
					Bundle bundle=new Bundle();
				    bundle.putString("brokerName", brokerName);
				    bundle.putString("isSelectedBrokerID", isSelectedBrokerID);
					Intent intent = new Intent();
					intent.putExtras(bundle);
					intent.setClass(MapReservation.this, Appointment.class);
					startActivity(intent);
				}else{
					Toast.makeText(getApplicationContext(), "请登陆后，再预约！",Toast.LENGTH_SHORT).show();
					Intent intent = new Intent();
					intent.setClass(MapReservation.this, Login.class);
					startActivity(intent);
				}
			}
		});
		mBtnReservation.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.out.println("预约成功！");
				
			}
		});
		
		StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		brokerStr = HCPostRequest.sendPost("http://128.199.148.4/index.php/Home/Broker/getFullList", "");
		
		
		try {
			JSONObject jsonObject = new JSONObject(brokerStr);
			JSONArray jsonArray = jsonObject.getJSONArray("list");
			for(int i=0;i < jsonArray.length();i++){
				LatLng point = new LatLng(Double.parseDouble(jsonArray.getJSONObject(i).getString("latitude")),Double.parseDouble(jsonArray.getJSONObject(i).getString("longtitude")));
				BitmapDescriptor bitmap = new BitmapDescriptorFactory().fromResource(R.drawable.broker_position);
				OverlayOptions option22 = new MarkerOptions().position(point).icon(bitmap);
				bMap.addOverlay(option22);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		LatLng point = new LatLng(22.54,113.98);
		
		
		bMap.setOnMarkerClickListener(new OnMarkerClickListener() {
			
			@Override
			public boolean onMarkerClick(Marker marker) {
				// TODO Auto-generated method stub
				if(brokerRelativeLayout.getVisibility() != View.GONE){
					mBtnReservation.setVisibility(View.VISIBLE);
				}else{
					brokerRelativeLayout.setVisibility(View.VISIBLE);
					mBtnReservation.setVisibility(View.GONE);
					JSONObject jsonObject;
					JSONArray jsonArray;
					try {
						jsonObject = new JSONObject(brokerStr);
						jsonArray = jsonObject.getJSONArray("list");
						for(int i=0;i < jsonArray.length();i++){
							if(marker.getPosition().latitude == Double.parseDouble(jsonArray.getJSONObject(i).getString("latitude"))){
								mapBrokerName.setText("姓名：" + jsonArray.getJSONObject(i).getString("name"));
								mapBrokerAddress.setText("地址：" + jsonArray.getJSONObject(i).getString("province") + jsonArray.getJSONObject(i).getString("city") + jsonArray.getJSONObject(i).getString("district"));
								//以下要传给预约界面的数据
								brokerName = jsonArray.getJSONObject(i).getString("name");
								isSelectedBrokerID = jsonArray.getJSONObject(i).getString("brokerid");
							}			
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
				return false;
			}
		});
		bMap.setOnMapTouchListener(new OnMapTouchListener() {
			
			@Override
			public void onTouch(MotionEvent arg0) {
				// TODO Auto-generated method stub
				brokerRelativeLayout.setVisibility(View.GONE);
			}
		});
		
		
		
		myProducts.setBackgroundResource(R.drawable.yuyued);
		
		this.myHomePage.setOnClickListener(new MyButtonListener());
		this.myCollection.setOnClickListener(new MyButtonListener());
		this.myProducts.setOnClickListener(new MyButtonListener());
		this.myCenter.setOnClickListener(new MyButtonListener());
		
		
		

	}

	public class MyLocationListener implements BDLocationListener {

		@Override
		public void onReceiveLocation(BDLocation location) {
			// map view 销毁后不在处理新接收的位置
			if (location == null || mMapView == null){
				return;
			}
//			System.out.println("维度"+location.getLatitude());
//			System.out.println("经度"+location.getLongitude());
			MyLocationData locData = new MyLocationData.Builder()
					.accuracy(location.getRadius())
					// 此处设置开发者获取到的方向信息，顺时针0-360
					.direction(100).latitude(location.getLatitude())
					.longitude(location.getLongitude()).build();
			bMap.setMyLocationData(locData);
			if (isFirstLoc) {
				isFirstLoc = false;
				LatLng ll = new LatLng(location.getLatitude(),
						location.getLongitude());
				MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(ll);
				bMap.animateMapStatus(u);
			}
		}

	}
	
/*
	@SuppressLint("NewApi")
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn1:
			PopupMenu pop = new PopupMenu(this, v);
			pop.getMenuInflater().inflate(R.menu.main, pop.getMenu());
			pop.show();
			break;
		case R.id.btn2:
			Intent intent = new Intent(this, Products.class);
			startActivity(intent);
			break;
		default:
			break;
		}
	}
*/
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN){
			if((System.currentTimeMillis() - this.exitTime) > 2000){
				Toast.makeText(this, R.string.exitApp, Toast.LENGTH_SHORT).show();
				this.exitTime = System.currentTimeMillis();
			}else{
				this.finish();
			}
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	@Override
	protected void onDestroy() {
		// 退出时销毁定位
		mLocClient.stop();
		// 关闭定位图层
		bMap.setMyLocationEnabled(false);
		mMapView.onDestroy();
		mMapView = null;
		
		super.onDestroy();
//		// 在activity执行onDestroy时执行mMapView.onDestroy()，实现地图生命周期管理
//		mMapView.onDestroy();
	}

	@Override
	protected void onResume() {
		super.onResume();
		// 在activity执行onResume时执行mMapView. onResume ()，实现地图生命周期管理
		mMapView.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		// 在activity执行onPause时执行mMapView. onPause ()，实现地图生命周期管理
		mMapView.onPause();
	}
	public class MyButtonListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent intent = new Intent();
			switch(v.getId()){
			case R.id.myHomePage:
				intent.setClass(MapReservation.this,MainActivity.class);
				startActivity(intent);
				break;
			case R.id.myCollection:
				//intent.setClass(MapReservation.this,Collection.class);
				//startActivity(intent);
				break;
			case R.id.myProducts:
				myProducts.setBackgroundResource(R.drawable.yuyued);
				intent.setClass(MapReservation.this,MapReservation.class);
				startActivity(intent);
				break;
			case R.id.myCenter:
				intent.setClass(MapReservation.this,IndividualCenter.class);
				startActivity(intent);
				break;
			}
			overridePendingTransition(R.anim.fade, R.anim.hold);
			MapReservation.this.finish();
		}
		
	}

}
