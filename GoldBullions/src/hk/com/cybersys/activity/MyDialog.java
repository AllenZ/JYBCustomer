package hk.com.cybersys.activity;



import hk.com.cybersys.basic.R;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MyDialog extends Dialog implements android.view.View.OnClickListener{

	private Context mContext;
	private Button mBtn_delappointment,mBtn_cancelappointment,mBtn_cancel;
	private int mappointmentId;
	
	private MyDialogOption myDialogOption;
	
	
	public MyDialog(Context context, boolean cancelable,
			OnCancelListener cancelListener) {
		super(context, cancelable, cancelListener);
		// TODO Auto-generated constructor stub
	}


	public MyDialog(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}


	public MyDialog(){
		super(null);
	}
	@SuppressLint("Instantiatable")
	public MyDialog(Context context,int appointmentId) {
		super(context);
		this.mContext=(Context) context;
		this.mappointmentId=appointmentId;
		// TODO Auto-generated constructor stub
	}
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//设置无标题
		requestWindowFeature(Window.FEATURE_NO_TITLE);    
        //全屏    
       getWindow().setFlags(WindowManager.LayoutParams. FLAG_FULLSCREEN ,      
                      WindowManager.LayoutParams. FLAG_FULLSCREEN);  
		setContentView(R.layout.my_dialog_at_appointment);
		initDialog();
	}
	//初始化
	public void initDialog(){
		LayoutInflater inflater = LayoutInflater.from(mContext);
		setCancelable(true);
		mBtn_cancelappointment=(Button) findViewById(R.id.tv_cancelappointment);
		mBtn_delappointment=(Button) findViewById(R.id.tv_delappointment);
		mBtn_cancel=(Button) findViewById(R.id.tv_cancel);
		
		getWindow().setLayout(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		getWindow().setGravity(Gravity.BOTTOM);
		//getWindow().setWindowAnimations(R.style.dialog_bottom_enterandout);
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.tv_delappointment:
			//删除预约
			myDialogOption.del();
			break;
		case R.id.tv_cancelappointment:
			//取消预约
			myDialogOption.cancleAppointment();
			break;
		case R.id.tv_cancel:
			//取消操作
			myDialogOption.cancle();
			break;

		default:
			break;
		}
	}

	public interface MyDialogOption{
		public abstract void cancle();
		public abstract void del();
		public abstract void cancleAppointment();
	}
	public void setMyDialogOption(MyDialogOption myDialogOption){
		this.myDialogOption=myDialogOption;
	}
}
