package hk.com.cybersys.activity;

import cao.jian.chen.utils.Utils;
import hk.com.cybersys.basic.R;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MyNews extends Activity {

	private TextView txt_title;
	private ImageView backImg;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.activity_mynews);
		super.onCreate(savedInstanceState);
		initTitle();
	}
	//初始化Title
  	private void initTitle() {
  		backImg=(ImageView) findViewById(R.id.img_back);
  		backImg.setVisibility(View.VISIBLE);
  		backImg.setOnClickListener(new View.OnClickListener() {
  			@Override
  			public void onClick(View v) {
  				// TODO Auto-generated method stub
  				//System.out.println("哈哈哈哈！");
  				Utils.finish(MyNews.this);
  			}
  		});
  		findViewById(R.id.txt_right).setVisibility(View.GONE);
  		txt_title = (TextView) findViewById(R.id.txt_title);
  		txt_title.setText("我的新闻");
  	}
}
