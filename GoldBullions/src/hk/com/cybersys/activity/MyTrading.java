package hk.com.cybersys.activity;

import hk.com.cybersys.adapter.TradeRecordAdapter;
import hk.com.cybersys.adapter.TradeRecordBean;
import hk.com.cybersys.basic.R;

import hk.com.cybersys.http.HCPostRequest;
import hk.com.cybersys.myLibrary.Constant;
import hk.com.cybersys.otheractivity.TransationDetailsiActivity;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import cao.jian.chen.utils.Constants;
import cao.jian.chen.utils.Utils;

public class MyTrading extends Activity {

	private ListView mlistView;
	private TextView txt_title;
	private ImageView backImg;
	private TextView tv_myTrading_norecord;
	private SharedPreferences perference;
	private Handler mTradeHandler;
	private TradeRecordBean mTradeRecored;
	private List<TradeRecordBean> TradeRecordBeanList=new ArrayList<TradeRecordBean>();;
	private String customerID;
	private JSONArray tradeDataArray;
	private TradeRecordAdapter mTradeRecAdapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		setContentView(R.layout.activity_trading);
		super.onCreate(savedInstanceState);
		tv_myTrading_norecord=(TextView) findViewById(R.id.tv_myTrading_norecord);
		mlistView=(ListView) findViewById(R.id.tradeRecord_list_view);
		//先判断用户是否登录
		//perference=getSharedPreferences("LoginStatus", 0);
		//String isLogin = perference.getString("isLogin", "");
		initTitle();
		if(Utils.getBooleanValue(this,Constants.LoginState )){
			//用户已经登录,获得CustomerID			
			customerID=Utils.getValue(this, Constants.CustomerID);
			//加载从数据库中取得的数据
			Runnable r=new GetTransactionRecored(customerID);
			new Thread(r).start();
		}else{
			//用户未登录
			Toast.makeText(this, "请先登录哦!亲~", 1).show();
		}
		mTradeHandler=new Handler(){
			
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				super.handleMessage(msg);
				//开始更新UI
				switch (msg.what) {
				case 1:
					//更新UI					
					System.out.println("进入handler");
					System.out.println("tradeDataArray长度："+tradeDataArray.length());
					for(int i=0;i<tradeDataArray.length();i++){
						    TradeRecordBean tradeRecord;					
							try {
								tradeRecord=new TradeRecordBean(
										tradeDataArray.getJSONObject(i).getString("transactionid"),
										tradeDataArray.getJSONObject(i).getString("productname"),
										tradeDataArray.getJSONObject(i).getInt("productid"),
										tradeDataArray.getJSONObject(i).getString("brokername"),										
										tradeDataArray.getJSONObject(i).getInt("totalamount"),
										tradeDataArray.getJSONObject(i).getInt("valid"),
										tradeDataArray.getJSONObject(i).getString("transactiontime"),
										tradeDataArray.getJSONObject(i).getString("iconname"),
										tradeDataArray.getJSONObject(i).getString("category")
										);
								
								TradeRecordBeanList.add(tradeRecord);	
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}									                         				                             
					}
					System.out.println("TradeRecordBeanList的SIZE:"+TradeRecordBeanList.size());
					mTradeRecAdapter=new TradeRecordAdapter(MyTrading.this, TradeRecordBeanList);		
					mlistView.setAdapter(mTradeRecAdapter);
					mlistView.setDivider(new ColorDrawable(Color.WHITE));  
					mlistView.setDividerHeight(5);
					mlistView.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub
							Bundle myBundle=new Bundle();
							myBundle.putSerializable("TradeRecordBean",TradeRecordBeanList.get(position));						
							//Toast.makeText(getActivity(), "点击的历史记录listview的position："+position, 1).show();
							Intent myIntent=new Intent();
							myIntent.putExtras(myBundle);
							myIntent.setClass(MyTrading.this,TransationDetailsiActivity.class);
							startActivity(myIntent);
						}					
					});
					break;
				case 2: 
					mlistView.setVisibility(View.GONE);
					tv_myTrading_norecord.setVisibility(View.VISIBLE);
					System.out.println("没有交易记录！");
					break;
				default:
					break;
				}
			}
		};		
	}
	
	//开启线程从数据库获取数据,
    class GetTransactionRecored implements Runnable{

    	private JSONObject jsonOb;
    	private String customerId;
    	public GetTransactionRecored(String param){
    		this.customerId=param;
    	}
		@Override
		public void run() {
			// TODO Auto-generated method stub
			System.out.println("开始查询语交易记录");
			System.out.println("交易记录上的customerID"+customerId);
			Message msg=new Message();
			try {
				//System.out.println(HCPostRequest.sendPost(Constant.URL_GET_Transaction_BY_CUSTOMERID,
						                                  //"customerid="+customerId));
				jsonOb=new JSONObject(HCPostRequest.sendPost(Constant.URL_GET_Transaction_BY_CUSTOMERID, 
						"customerid="+this.customerId));
				System.out.println(jsonOb+"++++++++++++++++");
				//System.out.println("customerid"+this.customerId);
				if(jsonOb.getString("success").equals("1")){
					System.out.println("查询成功！");
					
					tradeDataArray=jsonOb.getJSONArray("list");
					System.out.println("交易记录值："+tradeDataArray);
					msg.what=1;
					
				}else {
					//没有交易记录
					msg.what=2;
					
				}
				mTradeHandler.sendMessage(msg);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//查询数据库
			
		}
    	
    }
  //初始化Title
  	private void initTitle() {
  		backImg=(ImageView) findViewById(R.id.img_back);
  		backImg.setVisibility(View.VISIBLE);
  		backImg.setOnClickListener(new View.OnClickListener() {
  			@Override
  			public void onClick(View v) {
  				// TODO Auto-generated method stub
  				//System.out.println("哈哈哈哈！");
  				Utils.finish(MyTrading.this);
  			}
  		});
  		findViewById(R.id.txt_right).setVisibility(View.GONE);
  		txt_title = (TextView) findViewById(R.id.txt_title);
  		txt_title.setText("我的交易");
  	}

}
