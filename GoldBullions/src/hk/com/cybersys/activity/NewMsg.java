package hk.com.cybersys.activity;

import hk.com.cybersys.adapter.MessageAdapter;
import hk.com.cybersys.adapter.MessageBean;
import hk.com.cybersys.basic.R;
import hk.com.cybersys.http.HCPostRequest;
import hk.com.cybersys.myLibrary.Constant;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import cao.jian.chen.utils.Constants;
import cao.jian.chen.utils.Utils;

public class NewMsg extends Activity {

	private TextView txt_title;
	private ImageView backImg;
	private String customerID;
	private ListView lv_myMSG;
	private JSONArray jsonArrayData;
	private Handler mHandler;
	private List<MessageBean> msgBean=new ArrayList<MessageBean>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.activity_new);
		super.onCreate(savedInstanceState);
		initTitle();
		if(Utils.getBooleanValue(this,Constants.LoginState )){		
			customerID=Utils.getValue(this, Constants.CustomerID);
			lv_myMSG=(ListView) findViewById(R.id.lv_myMSG);
			String param="userid="+customerID+"&usertype=customer";
			Runnable myMsg=new GetMsg(param);
			new Thread(myMsg).start();
			mHandler=new Handler(){
				@Override
				public void handleMessage(Message msg) {
					// TODO Auto-generated method stub
					super.handleMessage(msg);
					switch (msg.what) {
					case 1:
						MessageBean myMsg;
						for(int i=0;i<jsonArrayData.length();i++){
							try {
								myMsg=new MessageBean(jsonArrayData.getJSONObject(i).getString("content"),
										              jsonArrayData.getJSONObject(i).getString("updatetime"));
								msgBean.add(myMsg);
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						MessageAdapter msgAdapter=new MessageAdapter(getApplicationContext(),msgBean);
						lv_myMSG.setAdapter(msgAdapter);
						lv_myMSG.setDivider(new ColorDrawable(Color.WHITE));  
						lv_myMSG.setDividerHeight(10);
						break;

					default:
						break;
					}
				}
			};
		}else{
			Toast.makeText(this, "请先登录哦!亲~", 1).show();
		}
		
	}
	private void initView(){
		
	}
	class GetMsg implements Runnable{
		String params;
		Message msg;
        public GetMsg(String params){
        	this.params=params;
        	System.out.println("NewMsg上的params："+params);
        	msg=new Message();
        }
		@Override
		public void run() {
			// TODO Auto-generated method stub
			try {
				JSONObject jsonOb=new JSONObject(HCPostRequest.sendPost(Constant.URL_GET_MSG, params));
			    System.out.println("jsonOb:"+jsonOb);
			    if(jsonOb.getString("success").equals("1")){
			    	jsonArrayData=jsonOb.getJSONArray("news");
			    	System.out.println("jsonArrayData:"+jsonArrayData);
			    	msg.what=1;
			    	mHandler.sendMessage(msg);
			    }
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	   
		}
	}
	//初始化Title
  	private void initTitle() {
  		backImg=(ImageView) findViewById(R.id.img_back);
  		backImg.setVisibility(View.VISIBLE);
  		backImg.setOnClickListener(new View.OnClickListener() {
  			@Override
  			public void onClick(View v) {
  				// TODO Auto-generated method stub
  				//System.out.println("哈哈哈哈！");
  				Utils.finish(NewMsg.this);
  			}
  		});
  		findViewById(R.id.txt_right).setVisibility(View.GONE);
  		txt_title = (TextView) findViewById(R.id.txt_title);
  		txt_title.setText("我的消息");
  	}

}
