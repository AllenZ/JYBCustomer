package hk.com.cybersys.activity;

import hk.com.cybersys.adapter.MyAdapter;
import hk.com.cybersys.adapter.ProductBean;
import hk.com.cybersys.basic.R;
import hk.com.cybersys.http.HCPostRequest;
import hk.com.cybersys.myLibrary.CommonWidget;
import hk.com.cybersys.myLibrary.Constant;
import hk.com.cybersys.widget.RefreshableView;
import hk.com.cybersys.widget.RefreshableView.PullToRefreshListener;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import cao.jian.chen.JsonArraySort;
import cao.jian.chen.utils.Utils;

//TabHost
public class Products extends Activity {
	private GestureDetector mGestureDetector;
	RefreshableView refreshableView;//下拉刷新控件
	ListView listView;
	
	public Handler product_handler;
	private TextView txt_title;
	private ImageView backImg;	
	private TextView productsClassTitle = null;	
	private JSONArray jsArray = null;
	private MyAdapter myAdapter;
	private List<ProductBean> list=new ArrayList<ProductBean>();//装数据的	
	private String [] productsNameList;	
	private String [] productName;	
	private String saleTime;//销售时间
	private JSONArray productArray;	
	private String category;//分类
	
	//排行方式1、门槛资金2、年收益率3、产品期限
	private int rankingMethod = 1;
	
	private String sortWay = "DES";
	
	private Button but1,but2,but3;
	
	
	@SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_products);
		//productsClassTitle = (TextView)findViewById(R.id.products_list_titile);
		findViewById();
		Bundle bundle = this.getIntent().getExtras();//获得传递进来的参数
		String name = bundle.getString("productType");
		if(name.equals("001")){
			txt_title.setText("私募基金");
			category = "私募基金";
		}else if(name.equals("002")){
			txt_title.setText("信托产品");
			category = "信托产品";
		}else if(name.equals("003")){
			txt_title.setText("资产管理");
			category = "资产管理";
		}else if(name.equals("004")){
			txt_title.setText("热门推荐");
			category = "热门推荐";
		}
		but1 = (Button)findViewById(R.id.product_fund_ranking);
		//刚开始进入时默认是按门槛资金排序，设置其颜色为Glay
		but1.setBackgroundColor(Color.rgb(0,122,255));
	    but2 = (Button)findViewById(R.id.product_rate_of_return);		
	    but3 = (Button)findViewById(R.id.product_term);		
		
		this.refreshableView = (RefreshableView)findViewById(R.id.products_refreshable_view);
		this.listView = (ListView)findViewById(R.id.products_list_view);
			
		CommonWidget.myProgressDialogShow(this, "正在获取数据，请稍后。。。", "你好",false);
		
		product_handler = new Handler(){
			@Override
			public void handleMessage(Message msg) {			
				switch (msg.what) {
				//根据门槛资金，年收益率，产品期限更改排序方式
				case 1:
					productArray = JsonArraySort.sortJsonArrayByDate(productArray,"threshold",sortWay);
					for (int i = 0; i < productArray.length(); i++) {
						ProductBean product;
						try {
							product = new ProductBean(productArray.getJSONObject(i).getString("threshold") ,
									            productArray.getJSONObject(i).getString("name"),
									            productArray.getJSONObject(i).getString("starttime"),
									            productArray.getJSONObject(i).getString("productid"),
									            productArray.getJSONObject(i).getInt("promotion"),
									            productArray.getJSONObject(i).getInt("maxprofit"),
									            productArray.getJSONObject(i).getInt("minperiod"),
									            productArray.getJSONObject(i).getString("period"),
									            productArray.getJSONObject(i).getInt("progress"));
							list.add(product);
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
					break;
				case 2:
					productArray = JsonArraySort.sortJsonArrayByDate(productArray,"maxprofit",sortWay);
					for (int i = 0; i < productArray.length(); i++) {
						ProductBean product;
						try {
							product = new ProductBean(productArray.getJSONObject(i).getString("threshold"), 
									                  productArray.getJSONObject(i).getString("name"),
									                  productArray.getJSONObject(i).getString("starttime"),
									                  productArray.getJSONObject(i).getString("productid"),
									                  productArray.getJSONObject(i).getInt("promotion"),
									                  productArray.getJSONObject(i).getInt("maxprofit"),
											          productArray.getJSONObject(i).getInt("minperiod"),
											          productArray.getJSONObject(i).getString("period"),
											          productArray.getJSONObject(i).getInt("progress"));
							list.add(product);
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
					break;
				case 3:
					productArray = JsonArraySort.sortJsonArrayByDate(productArray,"minperiod",sortWay);
					for (int i = 0; i < productArray.length(); i++) {
						ProductBean product;
						try {
							product = new ProductBean(productArray.getJSONObject(i).getString("threshold") ,
									                  productArray.getJSONObject(i).getString("name"),
									                  productArray.getJSONObject(i).getString("starttime"),
									                  productArray.getJSONObject(i).getString("productid"),
									                  productArray.getJSONObject(i).getInt("promotion"),
									                  productArray.getJSONObject(i).getInt("maxprofit"),
											          productArray.getJSONObject(i).getInt("minperiod"),
											          productArray.getJSONObject(i).getString("period"),
											          productArray.getJSONObject(i).getInt("progress"));
							list.add(product);
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
					break;		
				}
				myAdapter=new MyAdapter(Products.this,Products.this ,list,rankingMethod);
				
				listView.setAdapter(myAdapter);
				listView.setDivider(new ColorDrawable(Color.WHITE));  
				listView.setDividerHeight(2);
				//每个item添加监听事件
				listView.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
						Intent intent = new Intent();
						Bundle bundle = new Bundle();
						//将ProductID做为参数传递过去
						bundle.putString("ProductID", list.get(position).getProductID());
						intent.putExtras(bundle);
						intent.setClass(Products.this,ProductsShow.class);
						startActivity(intent);
						Toast.makeText(Products.this, list.get(position).getProductID(), Toast.LENGTH_SHORT).show();						
					}
				});
				
				//给每个Item添加滑动事件
				listView.setOnTouchListener(new View.OnTouchListener() {				
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						
						return mGestureDetector.onTouchEvent(event);
					}
				});
				CommonWidget.progressDialog.dismiss();
				super.handleMessage(msg);
			}
			
			
		};
		//滑动效果
		mGestureDetector=new GestureDetector(this,new OnGestureListener() {
			@Override
			public boolean onSingleTapUp(MotionEvent e) {
				// TODO Auto-generated method stub
				return false;
			}			
			@Override
			public void onShowPress(MotionEvent e) {
				// TODO Auto-generated method stub		
			}
			@Override
			public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
					float distanceY) {
				// TODO Auto-generated method stub
				return false;
			}
			@Override
			public void onLongPress(MotionEvent e) {
				// TODO Auto-generated method stub	
			}
			@Override
			public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
					float velocityY) {
				// TODO Auto-generated method stub
				int x = (int) (e2.getX() - e1.getX());
				int y=(int) (e2.getY()-e1.getY());							
				boolean dir=x>0;
				if(dir&&y<100){
					//dir为true 向右
					if(rankingMethod==3){	
						setButtonColor();
						but1.setBackgroundColor(Color.rgb(0,122,255));
						list.clear();
						Runnable run = new GetProducts(product_handler);
						new Thread(run).start();
						rankingMethod=1;
						myAdapter.notifyDataSetChanged();						
					}else if(rankingMethod==1){
						//System.out.println("从门槛资金到年收益率！");
						setButtonColor();
						but2.setBackgroundColor(Color.rgb(0,122,255));
						list.clear();
						//CommonWidget.myProgressDialogShow(this, "正在获取数据，请稍后。。。", "你好",false);
						Runnable run = new GetProducts(product_handler);
						new Thread(run).start();
						rankingMethod=2;
						myAdapter.notifyDataSetChanged();
					}else if(rankingMethod==2){
						//System.out.println("从年收益率到产品期限！");
						setButtonColor();
						but3.setBackgroundColor(Color.rgb(0,122,255));
						list.clear();
						//CommonWidget.myProgressDialogShow(this, "正在获取数据，请稍后。。。", "你好",false);
						Runnable run = new GetProducts(product_handler);
						new Thread(run).start();
						myAdapter.notifyDataSetChanged();
						rankingMethod=3;
					}			
				}else{
					//向左滑动
					//如果当前处于门槛资金则到产品期限
					if(rankingMethod==1){
						//System.out.println("从门槛资金到产品期限！");
						setButtonColor();
						but3.setBackgroundColor(Color.rgb(0,122,255));
						list.clear();
						//CommonWidget.myProgressDialogShow(this, "正在获取数据，请稍后。。。", "你好",false);
						Runnable run = new GetProducts(product_handler);
						new Thread(run).start();
						rankingMethod=3;
						myAdapter.notifyDataSetChanged();
					}else if(rankingMethod==3){
						//System.out.println("从产品期限到年收益率！");
						setButtonColor();
						but2.setBackgroundColor(Color.rgb(0,122,255));
						list.clear();
						//CommonWidget.myProgressDialogShow(this, "正在获取数据，请稍后。。。", "你好",false);
						Runnable run = new GetProducts(product_handler);
						new Thread(run).start();
						rankingMethod=2;
						myAdapter.notifyDataSetChanged();
					}else if(rankingMethod==2){
						//System.out.println("从年收益率到门槛资金！");
						setButtonColor();
						but1.setBackgroundColor(Color.rgb(0,122,255));
						list.clear();
						//CommonWidget.myProgressDialogShow(, "正在获取数据，请稍后。。。","hhh" ,false);
						Runnable run = new GetProducts(product_handler);
						new Thread(run).start();
						rankingMethod=1;
						myAdapter.notifyDataSetChanged();
					}		
				}
				return true;
			}
			
			@Override
			public boolean onDown(MotionEvent e) {
				// TODO Auto-generated method stub
				return true;
			}
		});
		
		Runnable run = new GetProducts(product_handler);
		new Thread(run).start();
		refreshableView.setOnRefreshListener(new PullToRefreshListener() {
			@Override
			public void onRefresh() {
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				refreshableView.finishRefreshing();
			}
		}, 1);		
	}
	
	public void onClick(View v) {
		if(sortWay == "DES"){
			sortWay = "ASC";
		}else if(sortWay == "ASC") {
			sortWay = "DES";
		}

		setButtonColor();
		switch (v.getId()) {
		case R.id.product_fund_ranking:
			v.setBackgroundColor(Color.GRAY);
			rankingMethod = 1;
			break;
		case R.id.product_rate_of_return:
			v.setBackgroundColor(Color.GRAY);
			rankingMethod = 2;
			break;
		case R.id.product_term:
			v.setBackgroundColor(Color.GRAY);
			rankingMethod =  3;
			break;
		default:
			break;
		}
		list.clear();
		CommonWidget.myProgressDialogShow(this, "正在获取数据，请稍后。。。", "你好",false);
		Runnable run = new GetProducts(product_handler);
		new Thread(run).start();
	}
	//设置button的颜色和排序显示
	public void setButtonColor(){
		but1.setBackgroundColor(Color.WHITE);
		but2.setBackgroundColor(Color.WHITE);
		but3.setBackgroundColor(Color.WHITE);
		if(sortWay == "DES"){
			but1.setText("门槛基金↓");
			but2.setText("年收益率↓");
			but3.setText("产品期限↓");
		}else if(sortWay == "ASC") {
			but1.setText("门槛基金↑");
			but2.setText("年收益率↑");
			but3.setText("产品期限↑");
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.product_categories_menu, menu);
		
		return super.onCreateOptionsMenu(menu);
		
	}
	
	class GetProducts implements Runnable{	
		private JSONObject productJsonOb;
		private String product_json_str;
		private Handler handler;
		
		public GetProducts(Handler handler){
			this.handler = handler;
		}
		@Override
		public void run() {
			this.product_json_str = HCPostRequest.sendPost(Constant.URL_PRODUCT_FULL_LIST, 
					                  "field=&category="+category);
			try {
				productJsonOb = new JSONObject(product_json_str);
				//获得数据库上的所有产品list
				productArray = productJsonOb.getJSONArray("list");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Message message = new Message();   
            message.what = rankingMethod;   
			this.handler.sendMessage(message);
		}
		
	}	//初始化title的控件
	private void findViewById() {
				txt_title =  (TextView) findViewById(R.id.txt_title);
				
				backImg=(ImageView) findViewById(R.id.img_back);
				backImg.setVisibility(View.VISIBLE);
				backImg.setVisibility(View.VISIBLE);
				backImg.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Utils.finish(Products.this);
					}
				});
			}


	
	
	
}
