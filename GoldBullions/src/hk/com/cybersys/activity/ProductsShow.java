package hk.com.cybersys.activity;


import hk.com.cybersys.basic.R;
import hk.com.cybersys.http.HCPostRequest;
import hk.com.cybersys.myLibrary.CommonWidget;
import hk.com.cybersys.myLibrary.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.widget.SearchViewCompat.OnCloseListenerCompat;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import cao.jian.chen.utils.Constants;
import cao.jian.chen.utils.Utils;

//产品详情Activity
/*
 * @Author By AllenZ
 * @Time 2015-8-7
 * 功能：通过webview来展示用户选择的产品的详细情况，并且可以对该
 * 产品进行收藏，增加了可以直接在该页面进行预约，点击预约按钮，
 * 跳转到预约理财师的界面SelectBrokerActivity.class
 * 
 */
public class ProductsShow extends Activity {
	
	private TextView txt_title;
	private ImageView img_right;
	private ImageView backImg;
	private WebView myWebView;
	private Button btn_product_make_appoint;
	private ImageView collectionProduct;
	
	private Bundle bundle;
	private String productID;
    SharedPreferences perference;
	private int activityNum = 3;
	private String customerId;
	private Handler collectionHandler = null;
	private String params;
	private String returnData;
	private Handler mHandler;
	private Handler cHandler;
	private boolean isUserCollected=false;
	

	private String isSelectedBrokerID;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_products_show);
		//获取屏幕的尺寸
//		DisplayMetrics dm = new DisplayMetrics();
//		getWindowManager().getDefaultDisplay().getMetrics(dm);
//		mHeight=dm.heightPixels;//获取屏幕的高度
		findViewById();
		
		//从上个Activity上获取参数productID;
		bundle = this.getIntent().getExtras();
		productID = bundle.getString("ProductID");
		//初始化控件				
		collectionHandler= new Handler() {
			@Override
			public void handleMessage(Message msg) {
				CommonWidget.progressDialog.dismiss();
				super.handleMessage(msg);
			}
		};	
		mHandler=new Handler(){
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				switch (msg.what) {
				   case 1:
					     isUserCollected=true;
					     initViews(isUserCollected);
					  break;
				   case 0:
					     initViews(isUserCollected);
					  break;
				   default:
					  break;
				}
				super.handleMessage(msg);
			}
		};
	}
		@Override
	protected void onStart() {
			//根据productID加载网页数据
			//myWebView.loadUrl("http://qyu1757770001.my3w.com/index.php/Home/Mobile/product/id/"+productID);
			myWebView.loadUrl(Constant.URL_GET_PRODUCT_BY_ID+productID);
			//1.查看用户是否登录，
			if(Utils.getBooleanValue(getApplicationContext(),Constants.LoginState)){
				//2.已经登录了，根据productID查看产品是否被收藏
				cHandler=new Handler(){
					@Override
					public void handleMessage(Message msg) {
						// TODO Auto-generated method stub
						switch (msg.what) {
						case 1:
							System.out.println("isUserCollected:"+isUserCollected);
							isUserCollected=true;
							initViews(isUserCollected);
							break;
						case 0:
							initViews(isUserCollected);
						default:
							break;
						}						
						super.handleMessage(msg);
					}
				};
				System.out.println("productid="+productID);
				System.out.println("customerid="+Utils.getValue(this, Constants.CustomerID));
				System.out.println("KKKKKKKK: "+"productid="+productID+"&"+"customerid="+Utils.getValue(this, Constants.CustomerID));
				CheckFavorThread checkFavor=new CheckFavorThread("productid="+productID+"&"+"customerid="+Utils.getValue(this, Constants.CustomerID));
				new Thread(checkFavor).start();
			}else{
				//3.没有登录
				initViews(isUserCollected);
			}
			// TODO Auto-generated method stub
			super.onStart();
		}
	
	  		

	
	//检查该产品是否已经被登录的的用户收藏
	class CheckFavorThread implements Runnable{
        
		private String param;
		public CheckFavorThread(String param){
			this.param=param;
		}
		@Override
		public void run() {
			// TODO Auto-generated method stub
			System.out.println("this.param"+this.param);
			returnData =HCPostRequest.sendPost(Constant.URL_CHECK_FAVOR, this.param);
			System.out.println("returnData:"+returnData);
			Message msg=new Message();
			try {
				JSONObject jsonOb = new JSONObject(returnData);
                if(jsonOb.getString("favor").equals("1")){					
					msg.what=1;						
					System.out.println("已经收藏了！=======================");
				}else{
					msg.what=0;
					System.out.println("未收藏！=======================");
				}
                cHandler.sendMessage(msg);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
	}
	

	//收藏产品
	class CollectionThread implements Runnable{
		private Handler handler;
		private String parameter;
		public CollectionThread(String params){
			this.parameter=params;
		}
		@Override
		public void run() {
			Looper.prepare();
			//System.out.println("开始收藏！");
			returnData =HCPostRequest.sendPost(Constant.URL_FAVOR_ADD, this.parameter);
			System.out.println(returnData);
			Message msg=new Message();
			try {
				JSONObject jsonOb = new JSONObject(returnData);
				if(jsonOb.getString("success").equals("1")){				
					System.out.println("收藏成功！=======================");
					//1：表示成功
					msg.what=1;	
					//发送广播，通知收藏已经改变
					sendBrodcast("collection");
				}else{
					//0:表示失败
					msg.what=0;
					Toast.makeText(getApplicationContext(), "收藏失败", 0).show();
					System.out.println("收藏失败！=======================");
				}
				mHandler.sendMessage(msg);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	
	//初始化上方的控件
	private void findViewById() {
			txt_title =  (TextView) findViewById(R.id.txt_title);
			img_right = (ImageView) findViewById(R.id.img_right);
			myWebView = (WebView)findViewById(R.id.webView1);
			btn_product_make_appoint=(Button) findViewById(R.id.btn_product_make_appoint);
			btn_product_make_appoint.setOnClickListener(new ProductAppointment());
//		    final ViewTreeObserver vto = btn_make_appoint.getViewTreeObserver();    
//		    
//		    vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener()
//		        {
//		            public boolean onPreDraw()
//		            {
//		            	    vto.removeOnPreDrawListener(this);
//		            	    btnHeight = btn_make_appoint.getMeasuredHeight();
//		                    btnWidth = btn_make_appoint.getMeasuredWidth();                   	             
//		                return true;
//		            }
//		        });
//		    LayoutParams linearParams =(LinearLayout.LayoutParams) myWebView.getLayoutParams();
//		    linearParams.height=(mHeight-btnHeight);
//		    myWebView.setLayoutParams(linearParams);
		}
    //初始化View
	public void initViews(boolean isUserCollected){
			txt_title.setText("产品详情");
			System.out.println("inintView上的isUserCollected"+isUserCollected);
			backImg=(ImageView) findViewById(R.id.img_back);
			backImg.setVisibility(View.VISIBLE);
			backImg.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Utils.finish(ProductsShow.this);
				}
			});
			if(isUserCollected){
				//表示已经收藏
				System.out.println("collected+++++++++++");
				img_right.setBackgroundResource(R.drawable.collected);
				//给已经收藏的按钮添加点击事件
				/*img_right.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(isUserCollected){
							//删除收藏
						}
					}
				});*/
			}else{
				System.out.println("collected-------------");
				//表示未收藏
				img_right.setImageResource(R.drawable.collect);
				//点击事件
				img_right.setOnClickListener(new OnClickListener() {		
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent myIntent=new Intent();						
						if(!(Utils.getBooleanValue(getApplicationContext(),Constants.LoginState ))){
							//没有登录，先登录
							Toast.makeText(ProductsShow.this, "请先登录！", Toast.LENGTH_SHORT).show();
							myIntent.putExtra("activityNum", activityNum);
							myIntent.setClass(ProductsShow.this, Login.class);
							startActivity(myIntent);
						}else{
							//已经登录，打开数据库
							customerId=Utils.getValue(getApplicationContext(), Constants.CustomerID);
							params = "customerid=" + customerId + "&" + "productid=" + productID;
							System.out.println(params+"++++++++++++++++++++++++++++++++++++++++++++++++=");
						   //异步调用插入数据
							Runnable r = new CollectionThread(params);
							new Thread(r).start();
							
							Toast.makeText(ProductsShow.this, "收藏成功", Toast.LENGTH_SHORT).show();
						}
					}
				});
			}	
			img_right.setVisibility(View.VISIBLE);
		}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		
	}
	//发送广播,收藏成功后，发送广播
	private void sendBrodcast(String Action){
		Intent intent = new Intent();
		intent.setAction("hk.com.cybersys.basic.collection");
		intent.putExtra("Action",Action);
		sendBroadcast(intent);
	}
	
	/*
	 * @Author by AllenZ
	 * @Time 2015-8-10 9:26
	 * 功能：产品详情页面的预约button的点击事件.
	 * 跳转到预约理财师界面
	 */
	class ProductAppointment implements OnClickListener{

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Bundle bundle=new Bundle();
			bundle.putString("isSelectedBrokerID", isSelectedBrokerID);
			
			Intent intent=new Intent();
			intent.putExtras(bundle);
			intent.setClass(ProductsShow.this, Appointment.class);
			startActivity(intent);
		}	
	}
	
}
