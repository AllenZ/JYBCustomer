package hk.com.cybersys.activity;

import hk.com.cybersys.basic.R;
import hk.com.cybersys.http.HCPostRequest;
import hk.com.cybersys.myLibrary.CommonWidget;
import hk.com.cybersys.myLibrary.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import cao.jian.chen.utils.Constants;
import cao.jian.chen.utils.Utils;

public class RegisterCustomer extends Activity {
	private TextView txt_title;
	private Handler registerHandler;
	private Button submit;
	private String loginParams;
	private String account;
	private String name;
	private boolean isRegisterSuccess=false;
	private EditText register_name,register_phone,register_password,register_confirm_password;
	private RadioGroup genderRadioGroup;
	private RadioButton male,female;
	private CheckBox register_accept_user_agreement;
	private String parameter;
	private String gender;

	private String phoneNum;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.activity_register_customer);
		init();
		registerHandler = new Handler(){

			@Override
			public void handleMessage(Message msg) {
				CommonWidget.progressDialog.dismiss();
				super.handleMessage(msg);
			}
		};
		submit = (Button)this.findViewById(R.id.register_submit);
		submit.setOnClickListener(new RegisterOnClickListener());
				
		super.onCreate(savedInstanceState);
		
	}
	class RegisterOnClickListener implements OnClickListener{	
		

		@Override
		public void onClick(View arg0) {
			register_name = (EditText)findViewById(R.id.register_name);
			
			register_password = (EditText)findViewById(R.id.register_password);
			register_confirm_password = (EditText)findViewById(R.id.register_confirm_password);
			
			genderRadioGroup = (RadioGroup)findViewById(R.id.gender);
		    male = (RadioButton)findViewById(R.id.male);
			female = (RadioButton)findViewById(R.id.female);
			
			if(register_name.getText().toString().length() == 0){
				Toast.makeText(RegisterCustomer.this, "账号名称不能为空", Toast.LENGTH_SHORT).show();
				return;
			}
			
			if(register_password.getText().toString().length() == 0){
				Toast.makeText(RegisterCustomer.this, "登录密码不能为空", Toast.LENGTH_SHORT).show();
				return;
			}
			if(register_confirm_password.getText().toString().length() == 0){
				Toast.makeText(RegisterCustomer.this, "确认密码不能为空", Toast.LENGTH_SHORT).show();
				return;
			}
			
			if(genderRadioGroup.getCheckedRadioButtonId() ==male.getId()){
				gender = "男";
			}
			if(genderRadioGroup.getCheckedRadioButtonId() == female.getId()){
				gender = "女";
			}
			
			
			//参数
			name= register_name.getText().toString();
			parameter = "name=" + register_name.getText() + "&" + "gender=" + gender + "&" + "password=" + register_password.getText() + "&" + "repassword=" + register_confirm_password.getText() + "&" + "mobile=" + register_phone.getText();
			loginParams="mobile=" + register_phone.getText() .toString().trim()+ "&" + "password=" + register_password.getText().toString().trim();
			account=register_phone.getText() .toString().trim();
			Runnable r = new RegisterThread(parameter);
			new Thread(r).start();	
			CommonWidget.myProgressDialogShow(RegisterCustomer.this, "正在提交...", "", false);
		}
		
	}
	/*
	 * 注册
	 */
	class RegisterThread implements Runnable{
		private String parameter;
		private String returnData;
		public RegisterThread(String params){
			this.parameter = params;
		}
		@Override
		public void run() {
			Looper.prepare();
			this.returnData = HCPostRequest.sendPost(Constant.URL_CUSTOMER_ADD, this.parameter);
			try {
				JSONObject jsonOb = new JSONObject(this.returnData);
				System.out.println(jsonOb.getString("success")+"++++++");
				if(jsonOb.getString("success").equals("1")){
					//注册成功
					isRegisterSuccess=true;
					Toast.makeText(RegisterCustomer.this, "注册成功", Toast.LENGTH_SHORT).show();
					Intent intent = new Intent();
					intent.putExtra("isRegisterSuccess", isRegisterSuccess);		
					intent.putExtra("autoParams", loginParams);
					intent.putExtra("account", account);
	                //设置登录数据缓存
					Utils.putBooleanValue(RegisterCustomer.this, Constants.LoginState,true);
					Utils.putValue(RegisterCustomer.this, Constants.CustomerID, jsonOb.getString("customerid"));
					Utils.putValue(RegisterCustomer.this,Constants.Account, account);
					Utils.putValue(RegisterCustomer.this, Constants.UserName,name );
					//Bundle bundle=new Bundle();
					//跳转
					intent.setClass(RegisterCustomer.this, MainActivity.class);
					startActivity(intent);
					Utils.finish(RegisterCustomer.this);
				}else{
					//注册失败
					Toast.makeText(RegisterCustomer.this, jsonOb.getString("error"), Toast.LENGTH_SHORT).show();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			RegisterCustomer.this.registerHandler.sendEmptyMessage(0);
			Looper.loop();
		}	
	}
	//初始化
	public void init(){			
		findViewById(R.id.txt_right).setVisibility(View.GONE);	
		txt_title = (TextView) findViewById(R.id.txt_title);
		register_phone = (EditText)findViewById(R.id.register_phone);
		txt_title.setText("注册"); 
		Intent intent=getIntent();
		phoneNum=intent.getStringExtra("phoneNum");
		System.out.println("注册phoneNum:"+phoneNum);
		register_phone.setText(phoneNum+"");
		register_phone.setEnabled(false);
		register_accept_user_agreement=(CheckBox)findViewById(R.id.register_accept_user_agreement);
		register_accept_user_agreement.setOnCheckedChangeListener(new CompoundButton. OnCheckedChangeListener() {			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked){
					submit.setEnabled(true);
				}
			}
		});
	}
}
