package hk.com.cybersys.activity;


import hk.com.cybersys.basic.R;
import hk.com.cybersys.myLibrary.CommonWidget;
import hk.com.cybersys.otheractivity.ModifiPSWActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import cao.jian.chen.utils.Constants;
import cao.jian.chen.utils.Utils;

public class Setting extends Activity implements OnClickListener {
	
	private LayoutInflater inflater;
	private View abouView,changerPsw;
	private TextView txt_title,tv_usersafe,tv_newMsgAler,tv_yinsi,tv_modifiPSW,tv_aboutApp;
	private Button btn_Logout;
	private ImageView backImg;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.activity_setting);
		init();
		inflater = LayoutInflater.from(Setting.this);
		super.onCreate(savedInstanceState);
	}
	
	//初始化
	private void init() {
		backImg=(ImageView) findViewById(R.id.img_back);
		backImg.setVisibility(View.VISIBLE);
		backImg.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//System.out.println("哈哈哈哈！");
				Utils.finish(Setting.this);
			}
		});
		findViewById(R.id.txt_right).setVisibility(View.GONE);
		txt_title = (TextView) findViewById(R.id.txt_title);
		txt_title.setText("设置");
		tv_usersafe=(TextView) findViewById(R.id.tv_usersafe);
		tv_newMsgAler=(TextView) findViewById(R.id.tv_newMsgAler);
		tv_yinsi=(TextView) findViewById(R.id.tv_yinsi);
		tv_modifiPSW=(TextView) findViewById(R.id.tv_modifiPSW);
		tv_aboutApp=(TextView) findViewById(R.id.tv_aboutApp);
		btn_Logout=(Button) findViewById(R.id.btn_Logout);
		if(Utils.getBooleanValue(getApplicationContext(), Constants.LoginState )){
			btn_Logout.setEnabled(true);
			tv_modifiPSW.setEnabled(true);
		}
		//添加点击事件
		tv_usersafe.setOnClickListener(this);
		tv_newMsgAler.setOnClickListener(this);
		tv_yinsi.setOnClickListener(this);
		tv_modifiPSW.setOnClickListener(this);
		tv_aboutApp.setOnClickListener(this);
		btn_Logout.setOnClickListener(this);
		
	}


	/*
	 * @Author By AllenZ
	 * @Time 2015-8-13
	 * @Function 退出登录账户，发送广播，跳转到登录界面
	 */
	private void logout(){
		//清除缓存
		Utils.RemoveValue(Setting.this, Constants.Account);
		Utils.RemoveValue(Setting.this,Constants.LoginState );
		Utils.RemoveValue(Setting.this, Constants.CustomerID);
		Utils.RemoveValue(Setting.this, Constants.UserName);
		//跳转到Login界面
		Intent myIntent=new Intent();
		myIntent.setClass(Setting.this,Login.class);
		startActivity(myIntent);
	}

	
	private void modifiPsw(){
		Intent myIntent=new Intent();
		myIntent.setClass(Setting.this,ModifiPSWActivity.class);
		startActivity(myIntent);
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.tv_usersafe:
			//账户安全
			Toast.makeText(Setting.this, "此功能待开发，敬请期待!", Toast.LENGTH_LONG).show();
			break;
		case R.id.tv_aboutApp:
			//关于金元宝
			abouView = inflater.inflate(R.layout.about, null);
			CommonWidget.myCustomDialog(Setting.this, abouView,true);
			break;
		case R.id.tv_newMsgAler:
			//新消息提醒
			Toast.makeText(Setting.this, "此功能待开发，敬请期待!", Toast.LENGTH_LONG).show();
			//startActivity(new Intent(Setting.this,NewMessageAlerts.class));
			break;
		case R.id.tv_modifiPSW:
			//修改密码
			modifiPsw();
			//changerPsw = inflater.inflate(R.layout.change_password, null);
			//CommonWidget.myCustomDialog(Setting.this, changerPsw,true);
			break;
		case R.id.btn_Logout:			
			//退出登录
			logout();
			break;
		case R.id.tv_yinsi:
			//隐私
			Toast.makeText(Setting.this, "此功能待开发，敬请期待!", Toast.LENGTH_LONG).show();
			break;	
		}
	}
}
