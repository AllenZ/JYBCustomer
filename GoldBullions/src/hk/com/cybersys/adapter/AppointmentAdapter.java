package hk.com.cybersys.adapter;



import hk.com.cybersys.basic.R;
import hk.com.cybersys.myLibrary.Constant;


import java.util.ArrayList;
import java.util.List;
import cao.jian.chen.utils.LoadIcon;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.StrictMode;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
/*
 * 预约记录的adapter
 * @Author By AllenZ
 * 功能：
 * 实现预约记录的适配，以及历史记录的适配
 */

public class AppointmentAdapter extends BaseAdapter {

	private Context context;
    private List<AppointmentBean> appointmentBeanList=new ArrayList<AppointmentBean>();
	private LoadIcon loadIcon;
	Holder holder;
	
	
	    
	public AppointmentAdapter(Context context,
			List<AppointmentBean> appointmentBeanList) {
		super();
		this.context = context;
		this.appointmentBeanList = appointmentBeanList;
		loadIcon=new LoadIcon();
		
		
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return appointmentBeanList.size();
	}
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return appointmentBeanList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){
			convertView= LayoutInflater.from(context).inflate(R.layout.layout_appointment_item,parent,false);
			holder=new Holder(convertView);
			holder.tv_appointment_broker_name=(TextView) convertView.findViewById(R.id.tv_appointment_broker_name);
			holder.tv_appointment_date=(TextView) convertView.findViewById(R.id.tv_appointment_date);
			holder.tv_appointment_time=(TextView) convertView.findViewById(R.id.tv_appointment_time);
			holder.tv_appointment_record_first=(TextView) convertView.findViewById(R.id.tv_appointment_record_first);
			
			holder.tv_appointment_record_acceptance=(TextView) convertView.findViewById(R.id.tv_appointment_record_acceptance);
			
			//holder.tv_appointment_record_thread=(TextView) convertView.findViewById(R.id.tv_appointment_record_thread);
			holder.img_appointment_record_corner=(ImageView) convertView.findViewById(R.id.img_appointment_record_corner);
			convertView.setTag(holder);
		}else{
			holder=(Holder) convertView.getTag();
		}	
		//holder.tv_appointment_record_first.setText(appointmentBeanList.get(position).getProductname1());
		//holder.tv_appointment_record_acceptance.setText(appointmentBeanList.get(position).getProductname2());
		
		if(appointmentBeanList.get(position).getAcceptance()==0){
			holder.tv_appointment_record_acceptance.setText("未处理");
			holder.tv_appointment_record_first.setVisibility(View.VISIBLE);
			holder.tv_appointment_record_acceptance.setVisibility(View.VISIBLE);		
		}
		//holder.tv_appointment_record_thread.setText(appointmentBeanList.get(position).getProductname3());
		holder.tv_appointment_broker_name.setText(appointmentBeanList.get(position).getBrokerName());
	    holder.tv_appointment_date.setText(appointmentBeanList.get(position).getDate());
	    holder.tv_appointment_time.setText(appointmentBeanList.get(position).getTime());
		
	    setCorner(appointmentBeanList.get(position).getStatus(),holder.img_appointment_record_corner);
		final ImageView appointment_broker_Icon=holder.getIcon();
		loadIcon.setRemoteImageListener(Constant.URL_GET_BROKERICON_ON_RECORD+appointmentBeanList.get(position).getIconname(),
					new LoadIcon.OnRemoteImageListener() {
						@Override
						public void onRemoteImage(Bitmap image) {
							// TODO Auto-generated method stub
							appointment_broker_Icon.setImageBitmap(image);
						}		
						@Override
						public void onError(String error) {
							// TODO Auto-generated method stub
							System.out.println("在预约记录上下载broker的头像原图失败"+error);
						}
					});
	
		
		return convertView;
	}
	public void setCorner(int status,ImageView imgCorner){
		switch (status) {
		case 1:
			//1：代表待见面
			imgCorner.setBackgroundResource(R.drawable.to_be_present);
			
			break;
		case -1:
			//1：代表取消见面
			imgCorner.setBackgroundResource(R.drawable.appointment_cancel);
			//imgCorner.setRotation(180);
			break;
		case 0:
			//预约取消
			imgCorner.setBackgroundResource(R.drawable.appointment_cancel);
			break;
		case 2:
			//预约过期
			imgCorner.setBackgroundResource(R.drawable.appointment_end);
			break;
		default:
			break;
		}
		//imgCorner.setRotation(-270);
	}
	private class Holder{
    	 View parentView;	 
    	 TextView tv_appointment_broker_name,tv_appointment_date,
    	          tv_appointment_time, tv_appointment_record_first,
    	          tv_appointment_record_acceptance;
    	 ImageView img_appointment_broker_icon,
    	           img_appointment_record_corner;
    	 public Holder(View view) {    
 	        this.parentView = view;    
 	    }    
 		public ImageView getIcon() {    
 	        if(img_appointment_broker_icon == null) {    
 	        	img_appointment_broker_icon = (ImageView) parentView.findViewById(R.id.img_appointment_broker_icon);    
 	        }    
 	        return img_appointment_broker_icon;    
 	    }    
    }

}
