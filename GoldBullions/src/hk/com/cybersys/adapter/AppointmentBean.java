package hk.com.cybersys.adapter;

import java.io.Serializable;

public class AppointmentBean implements Serializable {

	
	private static final long serialVersionUID = 1L;

	//Status:  (值， 代表的意思)
	//{(-1, 已删除), (0, 已取消), 
	//(1, 待出席), (2, 已过期),
	//(3, 已见面), (4, 跟进中), (5, 已交易), (6, 未见面) }
	private int status; 
	
	private int evaluationid;
	

	private int appointmentid;
	private String brokermobile;
	private int brokerid;
	private String iconname;
	private String brokerName;
	private String productname1;
	private String productname2;
	private String productname3;
	private String cutomername;
	private String time;
	private String date;
	private int acceptance;
	public int getEvaluationid() {
		return evaluationid;
	}
	public void setEvaluationid(int evaluationid) {
		this.evaluationid = evaluationid;
	}
	public int getAcceptance() {
		return acceptance;
	}
	public void setAcceptance(int acceptance) {
		this.acceptance = acceptance;
	}
	//构造函数
	public AppointmentBean( int status, int appointmentid, String brokermobile,
			int brokerid, String iconname, String brokerName,
			String productname1, String productname2, String productname3,
			String time, String date,int acceptance,int evaluationid) {
		
		super();
		this.status=status;
		this.appointmentid = appointmentid;
		this.brokermobile = brokermobile;
		this.brokerid = brokerid;
		this.iconname = iconname;
		this.brokerName = brokerName;
		this.productname1 = productname1;
		this.productname2 = productname2;
		this.productname3 = productname3;
		this.time = time;
		this.date = date;
		this.acceptance=acceptance;
		this.evaluationid=evaluationid;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getAppointmentid() {
		return appointmentid;
	}
	public void setAppointmentid(int appointmentid) {
		this.appointmentid = appointmentid;
	}
	public String getBrokermobile() {
		return brokermobile;
	}
	public void setBrokermobile(String brokermobile) {
		this.brokermobile = brokermobile;
	}
	public int getBrokerid() {
		return brokerid;
	}
	public void setBrokerid(int brokerid) {
		this.brokerid = brokerid;
	}
	public String getIconname() {
		return iconname;
	}
	public void setIconname(String iconname) {
		this.iconname = iconname;
	}
	public String getBrokerName() {
		return brokerName;
	}
	public void setBrokerName(String brokerName) {
		this.brokerName = brokerName;
	}
	public String getProductname1() {
		return productname1;
	}
	public void setProductname1(String productname1) {
		this.productname1 = productname1;
	}
	public String getProductname2() {
		return productname2;
	}
	public void setProductname2(String productname2) {
		this.productname2 = productname2;
	}
	public String getProductname3() {
		return productname3;
	}
	public void setProductname3(String productname3) {
		this.productname3 = productname3;
	}
	public String getCutomername() {
		return cutomername;
	}
	public void setCutomername(String cutomername) {
		this.cutomername = cutomername;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	
}
