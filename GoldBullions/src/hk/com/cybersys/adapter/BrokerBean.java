package hk.com.cybersys.adapter;

//封装broker
public class BrokerBean {
	
	private String brokerID;
	private String brokerName;//姓名
	private String address;//地址
	private String paperNum;//证件号码
	private String score;//评分
	private String iconName;//头像
	private int markCount;//评分人数
	private int averageMark;//平均分
	private int transactionCount;//交易数量
	
	public String getIconName() {
		return iconName;
	}
	public void setIconName(String iconName) {
		this.iconName = iconName;
	}
	public int getMarkCount() {
		return markCount;
	}
	public void setMarkCount(int markCount) {
		this.markCount = markCount;
	}
	public int getAverageMark() {
		return averageMark;
	}
	public void setAverageMark(int averageMark) {
		this.averageMark = averageMark;
	}
	public int getTransactionCount() {
		return transactionCount;
	}
	public void setTransactionCount(int transactionCount) {
		this.transactionCount = transactionCount;
	}

	
	
	
	public String getBrokerID() {
		return brokerID;
	}
	public void setBrokerID(String brokerID) {
		this.brokerID = brokerID;
	}
	public String getBrokerName() {
		return brokerName;
	}
	public void setBrokerName(String brokerName) {
		this.brokerName = brokerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPaperNum() {
		return paperNum;
	}
	public void setPaperNum(String paperNum) {
		this.paperNum = paperNum;
	}
	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}
	public String getBrokerIcon() {
		return iconName;
	}
	public void setBrokerIcon(String brokerIcon) {
		this.iconName = brokerIcon;
	}
	
	//构造函数
	public BrokerBean(String brokerID, String brokerName, 
			          String address,String iconName,
			          int averageMark,int markCount,int transactionCount) {
		super();
		this.brokerID = brokerID;
		this.brokerName = brokerName;
		this.address = address;
		this.iconName = iconName;
		this.averageMark=averageMark;
		this.markCount=markCount;
		this.transactionCount=transactionCount;
	}
	
	
	

}
