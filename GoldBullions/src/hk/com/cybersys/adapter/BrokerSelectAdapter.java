package hk.com.cybersys.adapter;



import hk.com.cybersys.basic.R;
import hk.com.cybersys.myLibrary.Constant;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import cao.jian.chen.utils.LoadIcon;

/*
 * 理财经理的Adapter，
 * @author By AllenZ
 */

public class BrokerSelectAdapter extends BaseAdapter {

	private List<BrokerBean> list=new ArrayList<BrokerBean>();
	private Context context;
	Holder holder;
	private LoadIcon loanIcon;
	
	public BrokerSelectAdapter( Context context,List<BrokerBean> list){
		this.context=context;
		this.list=list;
		//System.out.println("adapter上的数据");
		loanIcon=new LoadIcon();
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		//System.out.println("YYYYYYYYYYYYYYY"+list.size());
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		//holder=new Holder();
		
		if(convertView==null){				
			convertView=LayoutInflater.from(context).inflate(R.layout.layout_broker_items,parent ,false);
			holder=new Holder(convertView);
			holder.tv_broker_name=(TextView) convertView.findViewById(R.id.tv_broker_name);
			holder.tv_broker_address=(TextView) convertView.findViewById(R.id.tv_broker_address);
			holder.tv_selectappoint_mark_count=(TextView) convertView.findViewById(R.id.tv_selectappoint_mark_count);
			holder.tv_selectappoint_average_mark=(TextView) convertView.findViewById(R.id.tv_selectappoint_average_mark);
			holder.tv_selectappoint_transaction_count=(TextView) convertView.findViewById(R.id.tv_selectappoint_transaction_count);
			
			//	holder.brokerImg=(ImageView) convertView.findViewById(R.id.img_broker);
			convertView.setTag(holder);
		}else {
			holder=(Holder) convertView.getTag();
		}
		final ImageView icon=holder.getIcon();
	    //System.out.println("这是后来的"+list.get(position).getBrokerIcon());
		
		loanIcon.setRemoteImageListener(Constant.URL_DOWNLOAD_BROKER_IMG+list.get(position).getBrokerIcon(),
				new LoadIcon.OnRemoteImageListener() {  
            
            @Override  
            public void onError(String error) {  
                //Toast.makeText(context, error, Toast.LENGTH_LONG).show();  
            	System.out.println("下载图片失败");
            }  
            @Override  
            public void onRemoteImage(Bitmap image) {  
                      icon.setImageBitmap(image);  
            }  });
		holder.tv_broker_name.setText(list.get(position).getBrokerName());
		holder.tv_broker_address.setText(list.get(position).getAddress());
		holder.tv_selectappoint_mark_count.setText(list.get(position).getMarkCount()+"人");
		holder.tv_selectappoint_average_mark.setText(list.get(position).getAverageMark()+"分");
		holder.tv_selectappoint_transaction_count.setText(list.get(position).getTransactionCount()+"万");
		return convertView;
	}
	
	private class Holder{
		private View parentView;  
		ImageView brokerImg;
		TextView tv_broker_name,tv_broker_address,tv_selectappoint_mark_count,
		         tv_selectappoint_average_mark,tv_selectappoint_transaction_count;
		public Holder(View view) {    
	        this.parentView = view;    
	    }    
		public ImageView getIcon() {    
	        if(brokerImg == null) {    
	        	brokerImg = (ImageView) parentView.findViewById(R.id.img_broker);    
	        }    
	        return brokerImg;    
	    }    
	}
	
	//该类暂时没有使用
	class DownLoad extends AsyncTask<String, Void, Bitmap>{

	 private Bitmap mBitmap;
	 private ImageView imgView;
//	 private  final WeakReference  imageViewReference;  //使用WeakReference解决内存问题 
	 private String path;
	 public DownLoad(ImageView imageView){
		// imageViewReference = new WeakReference<ImageView>(imageView); 
		 imgView=imageView;
		// this.path=path;
		// System.out.println("这是DownLoad的构造方法he路径为："+path);
	 }
		@Override
		protected Bitmap doInBackground(String... params) {
			// TODO Auto-generated method stub
		
			//System.out.println("这个是doIn()上的参数："+params[0]);
	    	path=params[0];
			return getBitmap( Constant.URL_DOWNLOAD_BROKER_IMG+params[0]);
		}
		
		@Override
	    protected void onPostExecute( Bitmap bitmap) {
				// TODO Auto-generated method stub
			    mBitmap=bitmap;
				super.onPostExecute(bitmap);
				if (imgView != null) { 
				//	final ImageView imageView = (ImageView) imageViewReference.get();
					if(imgView.getTag()!=path){
						imgView.setImageBitmap(mBitmap);
					}else{
						
						System.out.println(path+"要加载的item相同");
					}
					
					//imgView.setImageBitmap(bitmap);  //下载完设置imageview为刚才下载的bitmap对象  
		            
		        }   
	    }
		@Override
			protected void onProgressUpdate(Void... values) {
				// TODO Auto-generated method stub
				super.onProgressUpdate(values);
				int i=0;
				//System.out.println("开始堵塞"+i++);
			}
		
		public Bitmap getBitmap(String imgurl){
			//Bitmap bitmap=null;
			//System.out.println("这里是getBitmap方法获得url："+imgurl);
			try {  
	            byte[] byteData = getImageByte(imgurl);  
	            if (byteData == null) {  
	              System.out.println("没有得到图片的byte，问题可能是path：" + imgurl);
	                return null;  
	            }  
	            int len = byteData.length;  
	            BitmapFactory.Options options = new BitmapFactory.Options();  
	            options.inPreferredConfig = Bitmap.Config.RGB_565;  
	            options.inPurgeable = true;  
	            options.inInputShareable = true;  
	            options.inJustDecodeBounds = false;  
	            if (len > 200000) {// 大于200K的进行压缩处理  
	                options.inSampleSize = 2;  
	            }  
	            return BitmapFactory.decodeByteArray(byteData, 0, len);  
	        } catch (Exception e) {  
	            e.printStackTrace();  
	            System.out.println("图片下载失败，异常信息是："+e.toString());
	            //Log.e(TAG, "图片下载失败，异常信息是：" + e.toString());  
	            return null;  
	        }  
			//return bitmap;
		}
	
		public byte[] getImageByte(String urlPath) {  
	        InputStream in = null;  
	        byte[] result = null;  
	        try {  
	            URL url = new URL(urlPath);  
	            HttpURLConnection httpURLconnection = (HttpURLConnection) url  
	                    .openConnection();  
	            httpURLconnection.setDoInput(true);  
	            httpURLconnection.connect();  
	            if (httpURLconnection.getResponseCode() == 200) {  
	                in = httpURLconnection.getInputStream();  
	                result = readInputStream(in);  
	                in.close();  
	            } else {  
	                //Log  .e(TAG, "下载图片失败，状态码是："  
	               //                 + httpURLconnection.getResponseCode());  
	            }  
	        } catch (Exception e) {  
	            //Log.e(TAG, "下载图片失败，原因是：" + e.toString());  
	            e.printStackTrace();  
	        } finally {  
	          //  Log.e(TAG, "下载图片失败!");  
	            if (in != null) {  
	                try {  
	                    in.close();  
	                } catch (IOException e) {  
	                    e.printStackTrace();  
	                }  
	            }  
	        }  
	        return result;  
		}
	
		private byte[]  readInputStream(InputStream in) throws Exception {  
			  
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();  
	        byte[] buffer = new byte[1024];  
	        int len = -1;  
	        while ((len = in.read(buffer)) != -1) {  
	            baos.write(buffer, 0, len);  
	        }  
	        baos.close();  
	        in.close();  
	        return baos.toByteArray();  
	    }  
	}
	
	
	
	
	
	

}
