package hk.com.cybersys.adapter;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1; // 数据库版本号

	private static final String DATABASE_NAME = "Collection"; // 数据库名称


	public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		String sqldept = "CREATE TABLE CollectionData(productID TEXT NOT NULL,PRIMARY KEY (productID))";
		db.execSQL(sqldept);
		
	}
	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
	}
}
