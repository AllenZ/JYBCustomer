package hk.com.cybersys.adapter;


import java.util.ArrayList;
import java.util.List;

import hk.com.cybersys.basic.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MessageAdapter extends BaseAdapter {
	private Holder holder;
	private Context context;
	private List<MessageBean> msgList=new ArrayList<MessageBean>();
	public MessageAdapter(Context context,List<MessageBean> myMsgList){
		super();
		this.context = context;
		this.msgList= myMsgList;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return msgList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return msgList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){
			holder=new Holder();
			convertView= LayoutInflater.from(context).inflate(R.layout.msgitem,parent,false);
			holder.tv_msgj=(TextView) convertView.findViewById(R.id.tv_msg);
			convertView.setTag(holder);
		}else{
			holder=(Holder) convertView.getTag();
		}	
		holder.tv_msgj.setText(msgList.get(position).getmContent());
		return convertView;
	}
	
	private class Holder{
   	  
   	 TextView tv_msgj;
	}
   

}
