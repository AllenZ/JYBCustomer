package hk.com.cybersys.adapter;

public class MessageBean {

	private String mContent;
	private String mUpdateTime;
	public String getmContent() {
		return mContent;
	}
	public void setmContent(String mContent) {
		this.mContent = mContent;
	}
	public String getmUpdateTime() {
		return mUpdateTime;
	}
	
	public void setmUpdateTime(String mUpdateTime) {
		this.mUpdateTime = mUpdateTime;
	}
	public MessageBean(String mContent, String mUpdateTime) {
		super();
		this.mContent = mContent;
		this.mUpdateTime = mUpdateTime;
	}
	
}
