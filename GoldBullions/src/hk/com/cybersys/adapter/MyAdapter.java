package hk.com.cybersys.adapter;

import hk.com.cybersys.basic.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MyAdapter extends BaseAdapter {
	private LinearLayout myProdutsClassBar;
	private Context context;
	private Activity activity;
	private List<ProductBean> list=new ArrayList<ProductBean>();
	private int rankingMethod;
	private String series1;
	private String series2;
	public MyAdapter(Context context,Activity activity,List<ProductBean>list,int rankingMethod) {
		this.context=context;
		this.list=list;
		this.activity = activity;
		this.rankingMethod=rankingMethod;
	}
	
	@Override
	public int getCount() {
		
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		
		return position;
	}
	Holder holder;
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		holder=new Holder();
		if (convertView==null) {
			convertView=LayoutInflater.from(context).inflate(R.layout.product_item, null);
			holder.tv_series=(TextView) convertView.findViewById(R.id.tv_series);
			holder.tv_productTitle=(TextView) convertView.findViewById(R.id.tv_city);
			holder.tv_saleprocessNum=(TextView) convertView.findViewById(R.id.tv_saleprocessNum);
			
			holder.tv_sillFundNum=(TextView) convertView.findViewById(R.id.tv_sillFundNum);
			holder.tv_productDeadlineNum=(TextView) convertView.findViewById(R.id.tv_productDeadlineNum);
			//设置标题字数，多余的用。。。表示
			holder.tv_productTitle.setSingleLine();
			holder.tv_productTitle.setEllipsize(TextUtils.TruncateAt.valueOf("END"));
			//holder.sale_time = (TextView)convertView.findViewById(R.id.sale_time);
			holder.productIMG=(ImageView) convertView.findViewById(R.id.ProductIMG);
			
			holder.tv_saleprocess=(TextView) convertView.findViewById(R.id.tv_saleprocess);
			holder.tv_sillFund=(TextView) convertView.findViewById(R.id.tv_sillFund);
			holder.tv_productDeadline=(TextView) convertView.findViewById(R.id.tv_productDeadline);
			//holder.product_collection_add = (ImageView)convertView.findViewById(R.id.product_collection_add);		  
			convertView.setTag(holder);
		}else {
			holder=(Holder) convertView.getTag();
		}
		//排序显示
		if(rankingMethod==1){
			//门槛资金
		       series1=list.get(position).getThreshold();
		       series2=position-1>=0?list.get(position-1).getThreshold():"";
		}else if(rankingMethod==2){
			//年收益率
			   series1=list.get(position).getMaxProfit()+"";
		       series2=position-1>=0?list.get(position-1).getMaxProfit()+"":"";
		}else if(rankingMethod==3){
			//产品期限
			   series1=list.get(position).getMinPeriod()+"";
		       series2=position-1>=0?list.get(position-1).getMinPeriod()+"":"";
		}
		else {
			series1=series2=" ";
		}
		

		
		//获取当前系统时间
		DateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date curDate=new Date(System.currentTimeMillis());
		String str=formatter.format(curDate);
		Date curDate1;
		long days = 0;
		try {
			curDate1 = formatter.parse(str);
			Date saleTime=formatter.parse(list.get(position).getSaleTime());
			long diff=curDate1.getTime()-saleTime.getTime();
			days=diff/(1000*60*60*24);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//排序的标题，首页的热门推荐不用，收藏列表也不用
		if (!series1.equals(series2)) {
			
			holder.tv_series.setVisibility(View.VISIBLE);
			holder.tv_series.setBackgroundColor(Color.WHITE);
			holder.tv_series.setText(series1);
			//holder.tv_productTitle.setText(list.get(position).getProductTitle());
		}else {
			//setTitle(rankingMethod);
			//setTitle(rankingMethod,list.get(position));
			holder.tv_series.setVisibility(View.GONE);
			//holder.tv_productTitle.setText(list.get(position).getProductTitle());	
		}
		holder.tv_productTitle.setText(list.get(position).getProductTitle());
		if(days>0){
			holder.productIMG.setImageResource(R.drawable.sale);
		}else if(days<0){
			holder.productIMG.setImageResource(R.drawable.newproduct);
		}
        //推荐角标
		if(list.get(position).getPromotion()!=0){
			holder.productIMG.setImageResource(R.drawable.recommend);
		}
		setTitle(rankingMethod,list.get(position));
		return convertView;
	}
	
	private class Holder{
		TextView tv_series,tv_productTitle,
		         sale_time,tv_saleprocessNum,
		         tv_sillFundNum,tv_productDeadlineNum,
		         tv_saleprocess,
                 tv_sillFund,tv_productDeadline;
		
		ImageView product_collection_add,productIMG;
	}
	//根据上面的选项来确定下面的标题
	private void setTitle(int rankingMethod,ProductBean product){
			
		switch (rankingMethod) {
		case 1:
			holder.tv_saleprocess.setText("销售进度");
			holder.tv_sillFund.setText("年收益率");
			holder.tv_productDeadline.setText("产品期限");	
			//System.out.println(product.getProgress()+"销售进度");
			holder.tv_saleprocessNum.setText(product.getProgress()+" %");
			holder.tv_sillFundNum.setText(product.getMaxProfit()+" %");
			holder.tv_productDeadlineNum.setText(product.getPeriod()+"个月");
			break;
		case 2:
			holder.tv_saleprocess.setText("销售进度");
			holder.tv_sillFund.setText("门槛资金");
			holder.tv_productDeadline.setText("产品期限");
			//System.out.println(product.getProgress()+"销售进度");
			holder.tv_saleprocessNum.setText(product.getProgress()+" %");
			holder.tv_sillFundNum.setText(product.getThreshold()+" 万");
			holder.tv_productDeadlineNum.setText(product.getPeriod()+" 个月");
			break;
		case 3:
			holder.tv_saleprocess.setText("销售进度");
			holder.tv_sillFund.setText("年收益率");
			holder.tv_productDeadline.setText("门槛资金");
			//System.out.println(product.getThreshold()+"门槛资金");
			holder.tv_saleprocessNum.setText(product.getProgress()+" %");
			holder.tv_sillFundNum.setText(product.getMaxProfit()+" %");
			holder.tv_productDeadlineNum.setText(product.getThreshold()+" 万");
			break;
			//4时将不显示排序的TextView
		case 4:
			
			holder.tv_saleprocess.setText("销售进度");
			holder.tv_sillFund.setText("年收益率");
			holder.tv_productDeadline.setText("产品期限");	
			//System.out.println(product.getProgress()+"销售进度");
			holder.tv_saleprocessNum.setText(product.getProgress()+" %");
			holder.tv_sillFundNum.setText(product.getMaxProfit()+" %");
			holder.tv_productDeadlineNum.setText(product.getPeriod()+"个月");
		default:
			break;
		}
	}
	
	
	 

}
