package hk.com.cybersys.adapter;

//金融产品的封装
public class ProductBean {

	private String threshold;
	private String productTitle;//可以替代省份名称
	private String saleTime;
	private String productID;
	private int progress;
	private String category;
    private int promotion;//是否推广
    private int maxProfit;
    private int minPeriod;
    private String period;
 
	
	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public int getMaxProfit() {
		return maxProfit;
	}

	public void setMaxProfit(int maxProfit) {
		this.maxProfit = maxProfit;
	}

	public int getMinPeriod() {
		return minPeriod;
	}

	public void setMinPeriod(int minPeriod) {
		this.minPeriod = minPeriod;
	}

	public ProductBean(String threshold,String productTitle,
			           String saleTime,String productID,int promotion,
			           int maxProfit,int minPeriod ,String period,int progress) {
		setThreshold(threshold);
		setProductTitle(productTitle);
		setSaleTime(saleTime);
		setProductID(productID);
		setPromotion(promotion);
		setMaxProfit(maxProfit);
		setMinPeriod(minPeriod);
		setPeriod(period);
		setProgress(progress);
	}
	
	public int getPromotion(){
		return promotion;
	}
	public void setPromotion(int promotion){
		this.promotion=promotion;
	}
	public int getProgress(){
		return progress;
	}
	public void setProgress(int progress){
		this.progress=progress;
	}
	
	public void setCategory(){
		this.category=category;
	}
	public String getCategory(String category){
		return category;
	}

	public String getThreshold() {
		return threshold;
	}

	public void setThreshold(String threshold) {
		this.threshold = threshold;
	}

	public String getProductTitle() {
		return productTitle;
	}
	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}
	public String getSaleTime() {
		return saleTime;
	}
	public void setSaleTime(String saleTime){
		this.saleTime = saleTime;
	}
	public String getProductID(){
		return productID;
	}
	public void setProductID(String productID){
		this.productID = productID;
	}
	
	
}
