package hk.com.cybersys.adapter;



import hk.com.cybersys.basic.R;
import hk.com.cybersys.myLibrary.Constant;

import java.util.ArrayList;
import java.util.List;

import cao.jian.chen.utils.LoadIcon;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

/*
 * @author AllenZHANG
 */
public class TradeRecordAdapter extends BaseAdapter {

	private List<TradeRecordBean> tradeRecordList=new ArrayList<TradeRecordBean>();
	private Context context;
	private LoadIcon loadIcon;
	Holder holder;
	public TradeRecordAdapter(Context context,List<TradeRecordBean> tradeRecordList){
		this.context=context;
		this.tradeRecordList=tradeRecordList;
		loadIcon=new LoadIcon();
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return tradeRecordList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return tradeRecordList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		// TODO Auto-generated method stub
		if(convertView==null){
			convertView=LayoutInflater.from(context).inflate(R.layout.trade_record_item, null);
			holder=new Holder(convertView);
			//holder.Img_userImg= (ImageView) convertView.findViewById(R.id.Img_userImg);
			holder.tv_userName=(TextView) convertView.findViewById(R.id.tv_userName);
			holder.tv_tradeProductName=(TextView) convertView.findViewById(R.id.tv_tradeProductName);
			holder.tv_tradeTime=(TextView) convertView.findViewById(R.id.tv_tradeTime);
			holder.tv_tradeState=(TextView) convertView.findViewById(R.id.tv_tradeState);	
			convertView.setTag(holder);
		}else{
			holder=(Holder) convertView.getTag();
		}
		holder.tv_userName.setText(tradeRecordList.get(position).getmBrokerName());
		holder.tv_tradeProductName.setText(tradeRecordList.get(position).getmProductName());
		holder.tv_tradeTime.setText(tradeRecordList.get(position).getmTransactionTime());
		if(tradeRecordList.get(position).getmState()==0){
			holder.tv_tradeState.setText("未审核");
		}else{
			holder.tv_tradeState.setText("已审核");
		}
		final ImageView icon=holder.getIcon();
		loadIcon.setRemoteImageListener(Constant.URL_GET_BROKERICON_ON_RECORD+tradeRecordList.get(position).getmUserPic(),
				new LoadIcon.OnRemoteImageListener() {
					@Override
					public void onRemoteImage(Bitmap image) {
						// TODO Auto-generated method stub
						icon.setImageBitmap(image);
					}		
					@Override
					public void onError(String error) {
						// TODO Auto-generated method stub
						System.out.println("在交易记录上下载broker的头像原图失败"+error);
					}
				});
		//holder.tv_userName.setText(tradeRecordList.get(position));
		return convertView;
	}
	
	private class Holder{
		View parentView;
		ImageView Img_userImg;
		TextView tv_userName,tv_tradeProductName,tv_tradeTime,tv_tradeState;
		public Holder(View view) {    
 	        this.parentView = view;    
 	    }    
		public ImageView getIcon() {    
 	        if(Img_userImg == null) {    
 	        	Img_userImg = (ImageView) parentView.findViewById(R.id.Img_userImg);    
 	        }    
 	        return Img_userImg;    
 	    }    
	}

}
