package hk.com.cybersys.adapter;

import java.io.Serializable;

/*
 * @author By AllenZhang
 * 该类主要是用于封装交易记录，由于filed较多，为了方便以后的扩展采用JAVA中的Builder模式
 * 使得参数可选
 */
public class TradeRecordBean implements Serializable {
	private  String mProductName;
	private  int    mProductId;
	private  int    mCustomerId;
	private  String mTransactionTime;
	private  String mUserPic;
	private  String mCustomerName;
	private  String mBrokerName;
	private String mBrokerId;
	private  String   mTransactionId;//根据TransactionId来获取交易记录，比较重要的一个字段
	private  String mCategory;
	private  int    mTrandSum;
	private  int mState;

	public TradeRecordBean(String mTransactionId,String mProductName,int mProductId,
			String mBrokerName,int mTrandSum,int mState,String mTransactionTime,String mUserPic
			,String mCategory){
		this.mTransactionId=mTransactionId;
		this.mProductName=mProductName;
		this.mProductId=mProductId;
		this.mBrokerName=mBrokerName;
		this.mTrandSum=mTrandSum;
		this.mState=mState;
		this.mTransactionTime=mTransactionTime;
		this.mUserPic=mUserPic;
		this.mCategory=mCategory;
	}
	public String getmProductName() {
		return mProductName;
	}
	public int getmProductId() {
		return mProductId;
	}
	public int getmCustomerId() {
		return mCustomerId;
	}
	public String getmTransactionTime() {
		return mTransactionTime;
	}
	public String getmUserPic() {
		return mUserPic;
	}
	public String getmCustomerName() {
		return mCustomerName;
	}
	public String getmBrokerName() {
		return mBrokerName;
	}
	public String getmBrokerId() {
		return mBrokerId;
	}
	public String getmTransactionId() {
		return mTransactionId;
	}
	public void setmProductName(String mProductName) {
		this.mProductName = mProductName;
	}
	public void setmProductId(int mProductId) {
		this.mProductId = mProductId;
	}
	public void setmCustomerId(int mCustomerId) {
		this.mCustomerId = mCustomerId;
	}
	public void setmTransactionTime(String mTransactionTime) {
		this.mTransactionTime = mTransactionTime;
	}
	public void setmUserPic(String mUserPic) {
		this.mUserPic = mUserPic;
	}
	public void setmCustomerName(String mCustomerName) {
		this.mCustomerName = mCustomerName;
	}
	public void setmBrokerName(String mBrokerName) {
		this.mBrokerName = mBrokerName;
	}
	public void setmBrokerId(String mBrokerId) {
		this.mBrokerId = mBrokerId;
	}
	public void setmTransactionId(String mTransactionId) {
		this.mTransactionId = mTransactionId;
	}
	public void setmCategory(String mCategory) {
		this.mCategory = mCategory;
	}
	public void setmTrandSum(int mTrandSum) {
		this.mTrandSum = mTrandSum;
	}
	public void setmState(int mState) {
		this.mState = mState;
	}
	public String getmCategory() {
		return mCategory;
	}
	public int getmTrandSum() {
		return mTrandSum;
	}
	public int getmState() {
		return mState;
	}
	
	
		
}
