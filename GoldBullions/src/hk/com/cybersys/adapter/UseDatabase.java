package hk.com.cybersys.adapter;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class UseDatabase {

	Context context;
	DatabaseHelper dbhelper;
	public SQLiteDatabase sqlitedatabase;

	public UseDatabase(Context context) {
		super();
		this.context = context;
	}

	// 打开数据库连接
	public void opendb(Context context) {
		dbhelper = new DatabaseHelper(context);
		sqlitedatabase = dbhelper.getWritableDatabase();
	}

	// 关闭数据库连接
	public void closedb(Context context) {
		if (sqlitedatabase.isOpen()) {
			sqlitedatabase.close();
		}
	}

	// 插入表数据
	public void insert(String table_name, ContentValues values) {
		opendb(context);
		sqlitedatabase.insert(table_name, null, values);
		closedb(context);
	}

	// 更新数据
	public int updatatable(String table_name, ContentValues values, int ID) {
		opendb(context);
		return sqlitedatabase.update(table_name, values, " Type_ID = ? ",
				new String[] { String.valueOf(ID) });
	}

	// 删除表数据
	public void delete(String table_name) {
		opendb(context);
		try {
			sqlitedatabase.delete(table_name, null, null);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closedb(context);
		}
	}

	// 查找数据
	public JSONArray DeptArray() {
		JSONArray Items = new JSONArray();
		try {
			opendb(context);//打开数据库
			String sql = "SELECT * FROM CollectionData";
			Cursor c = sqlitedatabase.rawQuery(sql, null);
			if (c != null) {
				while (c.moveToNext()) {
					JSONObject item = new JSONObject();
					item.put("productID",
							c.getString(c.getColumnIndex("productID")));
					Items.put(item);
				}
				c.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closedb(context);
		}
		return Items;
	}
}