package hk.com.cybersys.basic;

import hk.com.cybersys.activity.AppointmentRecord;
import hk.com.cybersys.adapter.AppointmentBean;
import hk.com.cybersys.dialog.TitleMenu.ActionItem;
import hk.com.cybersys.dialog.TitleMenu.TitlePopup;
import hk.com.cybersys.dialog.TitleMenu.TitlePopup.OnItemOnClickListener;
import hk.com.cybersys.http.HCPostRequest;
import hk.com.cybersys.myLibrary.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import cao.jian.chen.utils.LoadIcon;
import cao.jian.chen.utils.Utils;


/*
 * @author By AllenZ
 * @Time:2015-8-5 17:00
 * @功能：一个Activity 用于展示预约记录的详细情况，可以在这个Activity上删除
 *       取消预约。
 */
public class AppointmentDetailActivity extends Activity implements OnClickListener {

	private final  static String AppointTag="AppointmentDetailActivity";
	private TextView txt_title;
	private ImageView backImg;
	private ImageView img_right;
	private TitlePopup titlePopup;
	private ImageView brokerIcon;
	private TextView  tv_appointment_detail_brokerName,tv_appointment_detail_brokerpingjunpen,
	                  tv_appointment_detail_product1,tv_appointment_detail_product2,tv_appointment_detail_product3,
	                  tv_appointment_detail_appoint_date,tv_appointment_detail_appoint_time,
	                  tv_appointment_detail_phone;
	private LoadIcon loadIcon;
	private Handler myHandler;
	private AppointmentBean appointmentBean;
	private Message msg;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_appointment_detail);
		findView();
		Intent intent=this.getIntent();
		appointmentBean=(AppointmentBean) intent.getSerializableExtra("appointRecBean");
		initTitle();
		msg=new Message();
		setOnListener();
		initPopWindow();
		loadIcon=new LoadIcon();	
		Log.d(AppointTag, intent.getSerializableExtra("appointRecBean")+"");
		
		setView();
		//处理删除，取消预约
		myHandler=new Handler(){
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				super.handleMessage(msg);
				switch (msg.what) {
				case 0:
					//删除预约
					Toast.makeText(getApplicationContext(), "删除预约成功！", 0).show();
					Utils.start_Activity(AppointmentDetailActivity.this,
						                 AppointmentRecord.class);
					break;
				case 1:
					//取消预约
					Toast.makeText(getApplicationContext(), "取消预约成功！", 0).show();
					Utils.start_Activity(AppointmentDetailActivity.this,
						      AppointmentRecord.class);
					break;
				case 2:
					//评价预约
					
					break;

				default:
					break;
				}
			}
		};
	}

	//设置View上的数据
	private void setView(){
		tv_appointment_detail_brokerName.setText(appointmentBean.getBrokerName());
		
		if(!appointmentBean.getProductname1().equals("null")){
			tv_appointment_detail_product1.setText(appointmentBean.getProductname1());
		}else{
			tv_appointment_detail_product1.setText("未选择");
			
		}		
		if(!appointmentBean.getProductname2().equals("null")){
			tv_appointment_detail_product2.setText(appointmentBean.getProductname2());		
		}else{
			tv_appointment_detail_product2.setText("未选择");
		}
		if(!appointmentBean.getProductname3().equals("null")){
			tv_appointment_detail_product3.setText(appointmentBean.getProductname2());			
		}else{
			tv_appointment_detail_product3.setText("未选择");
		}
		
		tv_appointment_detail_appoint_date.setText(appointmentBean.getDate());
		tv_appointment_detail_appoint_time.setText(appointmentBean.getTime());
		tv_appointment_detail_phone.setText(appointmentBean.getBrokermobile());
		loadIcon.setRemoteImageListener(Constant.URL_GET_BROKERICON_ON_RECORD+appointmentBean.getIconname(),
				new LoadIcon.OnRemoteImageListener() {
					@Override
					public void onRemoteImage(Bitmap image) {
						// TODO Auto-generated method stub
						brokerIcon.setImageBitmap(image);
					}		
					@Override
					public void onError(String error) {
						// TODO Auto-generated method stub
						System.out.println("在预约记录上下载broker的头像原图失败"+error);
					}
				});
	}
	//初始化View
	private void findView(){	
		brokerIcon=(ImageView) findViewById(R.id.img_appointment_detail_brokerIcon);
		tv_appointment_detail_brokerName=(TextView) findViewById(R.id.tv_appointment_detail_brokerName);
		tv_appointment_detail_brokerpingjunpen=(TextView) findViewById(R.id.tv_appointment_detail_brokerpingjunpen);
		tv_appointment_detail_product1=(TextView) findViewById(R.id.tv_appointment_detail_product1);
		tv_appointment_detail_product2=(TextView) findViewById(R.id.tv_appointment_detail_product2);
		tv_appointment_detail_product3=(TextView) findViewById(R.id.tv_appointment_detail_product3);
		tv_appointment_detail_appoint_date=(TextView) findViewById(R.id.tv_appointment_detail_appoint_date);
		
		tv_appointment_detail_appoint_time=(TextView) findViewById(R.id.tv_appointment_detail_appoint_time);
		tv_appointment_detail_phone=(TextView) findViewById(R.id.tv_appointment_detail_phone);
		
	}
	//初始化预约详情界面
	private void initTitle() {
  		backImg=(ImageView) findViewById(R.id.img_back);
  		img_right = (ImageView) findViewById(R.id.img_right);
  		txt_title = (TextView) findViewById(R.id.txt_title);
  		backImg.setVisibility(View.VISIBLE);
  		backImg.setOnClickListener(new View.OnClickListener() {
  			@Override
  			public void onClick(View v) {
  				// TODO Auto-generated method stub
  				//System.out.println("哈哈哈哈！");
  				Utils.finish(AppointmentDetailActivity.this);
  			}
  		});
  		findViewById(R.id.txt_right).setVisibility(View.GONE);
  		txt_title.setText("预约详情");
  		img_right.setVisibility(View.VISIBLE);
		img_right.setImageResource(R.drawable.icon_add);
  	}

	/*
	 * 初始化右上角的弹窗，
	 */
	private void initPopWindow() {
        Toast.makeText(this, "Status:"+appointmentBean.getStatus()+"", 1).show();
		// 实例化标题栏弹窗
	    titlePopup = new TitlePopup(this, LayoutParams.WRAP_CONTENT,
						LayoutParams.WRAP_CONTENT);
	    titlePopup.setItemOnClickListener(onitemClick);
	    titlePopup.addAction(new ActionItem(this, "删除预约",
				R.drawable.appitem_del_btn));
	    // 给标题栏弹窗添加子类
		if(appointmentBean.getStatus()==3||appointmentBean.getStatus()==4||appointmentBean.getStatus()==5){				
			titlePopup.addAction(new ActionItem(this, "评价预约",
					R.drawable.appointment_assess));	
		}else{
			titlePopup.addAction(new ActionItem(this, "取消预约",
					R.drawable.appoinment_cancel));
		}	
	}
	private void setOnListener() {
		img_right.setOnClickListener(this);
	}
	
	private OnItemOnClickListener onitemClick = new OnItemOnClickListener() {

		@Override
		public void onItemClick(ActionItem item, int position) {
			// mLoadingDialog.show();
			switch (position) {
			case 0:
				  System.out.println("appointId:"+Integer.toString(appointmentBean.getAppointmentid()));			
				 //删除预约
				 DelAppointment del=new DelAppointment(Integer.toString(appointmentBean.getAppointmentid()));
				 new Thread(del).start();
				// Utils.finish(AppointmentDetailActivity.this);				
				break;
			case 1:
				// 取消预约or评价预约 
				if(appointmentBean.getStatus()==3||appointmentBean.getStatus()==4||appointmentBean.getStatus()==5){
					//评价预约
					Toast.makeText(getApplicationContext(), "此功能上待开发！", 1).show();
				}else{
					//取消预约
				    CancelAppointment can=new CancelAppointment(Integer.toString(appointmentBean.getAppointmentid()),0);
				    new Thread(can).start();
				}
			   
			   				
//				Utils.start_Activity(MainActivity.this, PublicActivity.class,
//						new BasicNameValuePair(Constants.NAME, "添加朋友"));
				break;
//			case 2:// 扫一扫
//				Utils.start_Activity(MainActivity.this, CaptureActivity.class);
//				break;
//			case 3:// 收钱
//				Utils.start_Activity(MainActivity.this, GetMoneyActivity.class);
//				break;
			default:
				break;
			}
		}
	};

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.img_right:					
			titlePopup.show(findViewById(R.id.layout_bar_appointment_detail));
			break;
		default:
			break;
		}
	}
    /*
     * 删除预约记录的线程
     * @Author by AllenZ
     * @Time 2015-8-6 9:27
     * 
     */
	class DelAppointment implements Runnable{
		private String param;
		public DelAppointment(String param){
			this.param=param;
			System.out.println("param:"+param);
		}
		@Override
		public void run() {
			// TODO Auto-generated method stub
			Looper.prepare();
			try {
				JSONObject ruturnData=new JSONObject(HCPostRequest.sendPost(Constant.URL_DEL_APPOINTMENT_BY_APPOINTMENTID,
						                    "appointmentid="+param));
			    System.out.println(ruturnData);
				if(ruturnData.getString("success").equals("1")){
			    	System.out.println("删除预约成功");
			    	msg.what=0;
			    	myHandler.sendMessage(msg);    	
			    }else{
			    	//Toast.makeText(getApplicationContext(), "删除预约失败！", 0).show();
			    	System.out.println("删除预约失败");
			    }
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}		
	}
	
	/*
	 * 取消预约功能
	 * @Author by AllenZ
	 * @Time 2015-8-6 
	 */
	class CancelAppointment implements Runnable{
		private String param1;
		private int param2;
		public CancelAppointment(String param1,int param2){
			this.param1=param1;
			this.param2=param2;
		}
		@Override
		public void run() {
			Looper.prepare();
			// TODO Auto-generated method stub		
			try {
				JSONObject ruturnData=new JSONObject(HCPostRequest.sendPost(Constant.URL_CANCEL_APPOINTMENT_BY_APPOINTMENTID,
				        "appointmentid="+param1+"&"+"status="+param2));
				System.out.println("取消预约："+ruturnData);
				if(ruturnData.getString("success").equals("1")){
					//Toast.makeText(getApplicationContext(), "取消预约成功！", 0).show();
			    	System.out.println("取消预约成功");
			    	msg.what=1;
			    	myHandler.sendMessage(msg);		    	
			    }else{
			    	Toast.makeText(getApplicationContext(), "取消预约失败！", 0).show();
			    	System.out.println("删除预约失败");
			    }
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
	}
    /*
     * 评价预约，只有在预约Status为3,4,5时，才可以评价
     * @Aurhor by AllenZ
     * @Time 2015-8-6 10:51
     */
	
	
	
}
