package hk.com.cybersys.myLibrary;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class Common {

	private SharedPreferences mySharedPreferences;
	private SharedPreferences.Editor myEditor;

	/**
	 * 使用SharedPreferences保存数据 author chenjiancao
	 * 
	 * @param context
	 *            上下文对象
	 * @param name
	 *            存储的名称
	 * @param mode
	 *            文件的打开方式
	 * @param key
	 *            数据的键
	 * @param value
	 *            数据的值
	 */
	public void saveSharedPreferences(Context context, String name, int mode,
			String key, String value) {
		this.mySharedPreferences = context.getSharedPreferences(name, mode);
		this.myEditor = this.mySharedPreferences.edit();
		this.myEditor.putString(key, value);
		this.myEditor.commit();
	}

	public static String[] getSharedPreference(String key,Context mContext) {
		String regularEx = "#";
		String[] str = null;
		SharedPreferences sp = mContext.getSharedPreferences("data",
				Context.MODE_PRIVATE);
		String values;
		values = sp.getString(key, "");
		str = values.split(regularEx);

		return str;
	}

	public static void setSharedPreference(String key, String[] values,Context mContext) {
		String regularEx = "#";
		String str = "";
		SharedPreferences sp = mContext.getSharedPreferences("data",
				Context.MODE_PRIVATE);
		if (values != null && values.length > 0) {
			for (String value : values) {
				str += value;
				str += regularEx;
			}
			Editor et = sp.edit();
			et.putString(key, str);
			et.commit();
		}
	}

}
