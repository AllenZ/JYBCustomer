package hk.com.cybersys.myLibrary;

import hk.com.cybersys.basic.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

public class CommonWidget {

	public static ProgressDialog progressDialog;
	public static android.app.AlertDialog.Builder myBuilder;
	public static Dialog loadingDialog;

	public static void myProgressDialogShow(Context context, String msg,
			String title, boolean needTitle) {
		progressDialog = new ProgressDialog(context);
		progressDialog.setMessage(msg);
		if (needTitle) {
			progressDialog.setTitle(title);
		}
		progressDialog.setCancelable(false);
		progressDialog.show();
	}

	public static void myAlertDialog(Context context,View v, String title,OnClickListener listener) {
		myBuilder = new AlertDialog.Builder(context);
		myBuilder.setTitle(title);
		myBuilder.setIcon(android.R.drawable.ic_dialog_info);
		myBuilder.setView(v);
		myBuilder.setPositiveButton("ȷ��", listener);
		myBuilder.setNegativeButton("ȡ��", listener);
		myBuilder.show();
	}
	
	public static void myCustomDialog(Context context,View v,boolean cancelable) {
		
		loadingDialog = new Dialog(context, R.style.loading_dialog);
		loadingDialog.setContentView(v,new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
			LinearLayout.LayoutParams.MATCH_PARENT));
		loadingDialog.setCancelable(cancelable);
		loadingDialog.show();

	}

}
