package hk.com.cybersys.myLibrary;

public class Constant {
	
	public static String host="http://112.74.28.177/index.php/Home";
	//根据产品Id
	public static String URL_GET_PRODUCT_BY_ID=host+"/Mobile/product/id/";
	// 所有产品
	public static String URL_PRODUCT_FULL_LIST = host+"/Product/getFullList";
	//获取产品首页的所有banner图片列表
	public static String URL_GET_BANNER=host+"/Banner/getFullList";
	//根据image来获取当前的banner
	public static String URL_GET_BANNER_IMAGE="http://112.74.28.177/Public/Banner/";
	//登陆
	public static String URL_CUSTOMER_LOGIN = host+"/Customer/login";
	
	//注册
	public static String URL_CUSTOMER_ADD = host+"/Customer/add";
	
	//预约
	public static String URL_APPOINTMENT_ADD = host+"/Appointment/add";
	
	//客户信息
	public static String URL_CUSTOMER_GET_BY_ID = host+"/Customer/getByID";
	
	//修改客户信息
	public static String URL_CUSTOMER_EDIT = host+"/Customer/edit";
	//上传用户头像
	public static String URL_UPLOAD_CUSTOMER_ICON=host+"/uploadUserIcon";
	//通过ID取产品
	public static String URL_PRODUCT_GET_BY_ID = host+"/Product/getByID";
	//增加token
	public static String URL_CUSTOMER_ADD_TOKEN = host+"/Customer/addToken";
	//获取收藏列表
	public static String URL_FAVOR_GET=host+"/Favor/getByCustomerid";
	//增加收藏
	public static String URL_FAVOR_ADD=host+"/Favor/add";
	//查看收藏
	public static String URL_CHECK_FAVOR=host+"/Favor/check";
	//查看预约记录
	public static String URL_APPOINTMENT_GET_BY_CUSTOMERID=host+"/Appointment/getByCustomerid";
	//删除预约记录
	public static String URL_DEL_APPOINTMENT_BY_APPOINTMENTID=host+"/Appointment/customerDel";
	//取消预约
	public static String URL_CANCEL_APPOINTMENT_BY_APPOINTMENTID=host+"/Appointment/edit";
	//删除收藏
	public static String URL_DEL_Favor_BY_FAVORID=host+"/Favor/del";

	//获取交易记录
	public static String URL_GET_Transaction_BY_CUSTOMERID=host+"/Transaction/getByCustomerid";
	
	//获取所有的Broker
	public static String URL_GET_BROKER=host+"/Broker/getFullList";
	//从网上下载图片
	public static String URL_DOWNLOAD_BROKER_IMG="http://112.74.28.177/Public/UserIcon/";
	
	//用户从预约记录上获取broker的头像
	public static String URL_GET_BROKERICON_ON_RECORD="http://112.74.28.177/Public/UserIcon/";
    //用户注册时，检测验证码
	public static String URL_REGISTER_CHECK_CODE=host+"/User/verifyMobileAndroid";
	//修改Customer密码
	public static String URL_EDIT_CUSTOMER_PSW=host+"/Customer/editPassword";
	//获取后台推送的的消息
	public static String URL_GET_MSG=host+"/News/getPushMsg";
	//获取后台的新闻列表
	public static String URL_GET_NEWS=host+"/News/getNews";
	//获取所有的理财师
	public static String URL_GET_FULLBROKER=host+"/Broker/getFullList";
}
