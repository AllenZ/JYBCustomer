package hk.com.cybersys.myinterface;

public interface FragementCallBack {

	public void callBackFromLogin();
	public void callBackFromCollection();
	public void callBackFromAppointment();
}
