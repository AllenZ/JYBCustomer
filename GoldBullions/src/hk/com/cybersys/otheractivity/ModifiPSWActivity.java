package hk.com.cybersys.otheractivity;




import hk.com.cybersys.activity.Login;
import hk.com.cybersys.basic.R;

import hk.com.cybersys.http.HCPostRequest;
import hk.com.cybersys.myLibrary.Constant;

import org.json.JSONException;
import org.json.JSONObject;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import cao.jian.chen.utils.Constants;
import cao.jian.chen.utils.Utils;
import cn.smssdk.EventHandler;
import cn.smssdk.SMSSDK;

/*
 * @Author By AllenZ
 * @Time 2015-8-19 16:07
 * @Function 用户密码的修改
 */
public class ModifiPSWActivity extends Activity {

	private ImageView backImg;
	private TextView txt_title;
	private EditText et_modifiPhoneNum,et_oldPSW,et_newPSW,et_ensurePSW;
	private Button btn_modifiPSW_getCode,btn_modifiPSW_SUBMIT;
	private String params;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_modifi_psw);
		//初始化短信接口
		SMSSDK.initSDK(this,"943373575ae0","c2707c222debedc1f797d640794b1438");
				EventHandler eh=new EventHandler(){
					@Override
					public void afterEvent(int event, int result, Object data) {
						// TODO Auto-generated method stub
						super.afterEvent(event, result, data);
					}
				};
		SMSSDK.registerEventHandler(eh);
		initTitle();
		initView();
		
//		mHandler=new Handler(){
//			@Override
//			public void handleMessage(Message msg) {
//				// TODO Auto-generated method stub
//				super.handleMessage(msg);
//				switch (msg.what) {
//				case 1:
//					Utils.RemoveValue(ModifiPSWActivity.this, Constants.Account);
//					Utils.RemoveValue(ModifiPSWActivity.this,Constants.LoginState );
//					Utils.RemoveValue(ModifiPSWActivity.this, Constants.CustomerID);
//					Utils.RemoveValue(ModifiPSWActivity.this, Constants.UserName);
//					startLogin();
//					Utils.finish(ModifiPSWActivity.this);
//					break;
//				default:
//					break;
//				}
//			}
//		};
	}

	private void initTitle(){
		backImg=(ImageView) findViewById(R.id.img_back);
		backImg.setVisibility(View.VISIBLE);
		backImg.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//System.out.println("哈哈哈哈！");
				Utils.finish(ModifiPSWActivity.this);
			}
		});
		findViewById(R.id.txt_right).setVisibility(View.GONE);
		txt_title = (TextView) findViewById(R.id.txt_title);
		txt_title.setText("修改密码");
	}
	private void initView(){
		et_modifiPhoneNum=(EditText) findViewById(R.id.et_modifiPhoneNum);
		et_oldPSW=(EditText) findViewById(R.id.et_oldPSW);
		et_newPSW=(EditText) findViewById(R.id.et_newPSW);
		et_ensurePSW=(EditText) findViewById(R.id.et_ensurePSW);
		//btn_modifiPSW_getCode=(Button) findViewById(R.id.btn_modifiPSW_getCode);
		//确定修改密码
		btn_modifiPSW_SUBMIT=(Button) findViewById(R.id.btn_modifiPSW_SUBMIT);
		setListener();
	}
	
	private void setListener(){
		btn_modifiPSW_SUBMIT.setOnClickListener(new View.OnClickListener() {		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.out.println("旧密码："+Utils.getValue(getApplicationContext(), Constants.PWD));
				System.out.println("新密码："+et_newPSW.getText().toString().trim());
				System.out.println("确认密码："+et_ensurePSW.getText().toString().trim());
			   //开启线程，更改密码
				params="customerid="+Utils.getValue(getApplicationContext(), Constants.CustomerID)+
					   "&password="+et_oldPSW.getText().toString().trim()+"&newpassword="+et_newPSW.getText().toString().trim()+
					   "&repassword="+et_ensurePSW.getText().toString().trim();
				System.out.println("更改密码的params"+params);
				Runnable editPSW=new EditCustomerPsw();
				new Thread(editPSW).start();
			}	
		});
		et_oldPSW.addTextChangedListener(new OldPswChanged());
		et_ensurePSW.addTextChangedListener(new ConparisonPsw());
	}
	/*
	 * 修改密码的线程
	 */
	class EditCustomerPsw implements Runnable{

		@Override
		public void run() {
			// TODO Auto-generated method stub
			//修改成功后跳转到登录界面
			try {
			   JSONObject JsonOb=new JSONObject(HCPostRequest.sendPost(Constant.URL_EDIT_CUSTOMER_PSW, params));
			   System.out.println("JsonOb:"+JsonOb);
			   System.out.println("JsonOb的success："+JsonOb.getString("success"));
				if(JsonOb.getString("success").equals("1")){
					System.out.println("修改密码成功，正在跳转。。。。");
			    	//Toast.makeText(getApplicationContext(), "修改密码成功，正在跳转。。。。", 1).show();
			    	//msg.what=1;
					Utils.RemoveValue(ModifiPSWActivity.this, Constants.Account);
					Utils.RemoveValue(ModifiPSWActivity.this,Constants.LoginState );
					Utils.RemoveValue(ModifiPSWActivity.this, Constants.CustomerID);
					Utils.RemoveValue(ModifiPSWActivity.this, Constants.UserName);
					Intent myIntent=new Intent();
					myIntent.setClass(ModifiPSWActivity.this, Login.class);
					startActivity(myIntent);
					Utils.finish(ModifiPSWActivity.this);
			    }else{
			    	Toast.makeText(getApplicationContext(), "修改密码失败，原因问数据库", 1).show();
			    }
				//mHandler.sendMessage(msg);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
	}
	//跳转到登录界面
	private void startLogin(){
		Intent myIntent=new Intent();
		myIntent.setClass(ModifiPSWActivity.this, Login.class);
		startActivity(myIntent);
	}
	/*
	 * 判断两次输入的新密码是否相同
	 * 
	 */
	class ConparisonPsw implements TextWatcher{

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			 if(et_ensurePSW.getText().toString().trim().length()==et_newPSW.getText().toString().trim().length()){
				 //笔记两次输入的密码是否一致
				 if(et_ensurePSW.getText().toString().trim().equals(et_newPSW.getText().toString().trim())){
					 //一致
					 btn_modifiPSW_SUBMIT.setEnabled(true);
				 }else{
					 //不一致
					 Toast.makeText(getApplicationContext(), "您两次输入的密码不一致，请重新输入！", 1).show();
				 }
			 }else{
				 
			 }
		}

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	/*
	 * 监听旧密码是否输入正确
	 */
	class OldPswChanged implements TextWatcher{

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			//与缓存的旧密匹配
			if(et_oldPSW.getText().toString().trim().length()>0&&et_oldPSW.getText().toString().trim().length()<18){
				if(et_oldPSW.getText().toString().trim().equals(Utils.getValue(getApplicationContext(), Constants.PWD))){
					//相同			
					et_newPSW.setEnabled(true);
					et_ensurePSW.setEnabled(true);
				}else{
					//不相同
					//Toast.makeText(getApplicationContext(), "请输入正确的旧密码", 0).show();
				}
			}else{
				//Toast.makeText(getApplicationContext(), "请输入正确的旧密码", 0).show();
			}	
		}
		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub	
		}
				
	}

	
    @Override
    protected void onDestroy() {
    	// TODO Auto-generated method stub
    	super.onDestroy();
    	SMSSDK.unregisterAllEventHandler();
    }
}
