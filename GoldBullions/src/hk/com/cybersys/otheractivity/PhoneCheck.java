package hk.com.cybersys.otheractivity;

import hk.com.cybersys.activity.RegisterCustomer;
import hk.com.cybersys.basic.R;
import hk.com.cybersys.http.HCPostRequest;
import hk.com.cybersys.myLibrary.CommonWidget;
import hk.com.cybersys.myLibrary.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import cao.jian.chen.utils.Utils;
import cn.smssdk.EventHandler;
import cn.smssdk.SMSSDK;

public class PhoneCheck extends Activity implements OnClickListener{
	private TextView txt_title;
	private Button btn_phoneCheck_next, btn_phoneCheck_getCode;
	private EditText et_usertel, et_code;
	private MyCount mc;
	private String mPhoneNum,mCode;
	
	private Handler mHandler;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_phone_check);
		//初始化短信接口
		SMSSDK.initSDK(this,"943373575ae0","c2707c222debedc1f797d640794b1438");
		EventHandler eh=new EventHandler(){
			@Override
			public void afterEvent(int event, int result, Object data) {
				// TODO Auto-generated method stub
				super.afterEvent(event, result, data);
			}
		};
		SMSSDK.registerEventHandler(eh);
		init();
		mHandler=new Handler(){
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				super.handleMessage(msg);
				switch (msg.what) {
				case 1:
					//跳转到个人注册的详细信息界面
					startUserRegisterDetail(et_usertel.getText().toString().trim());
					break;
				case 2:
					Toast.makeText(getApplicationContext(), "验证失败！", 0).show();
					break;
				default:
					break;
				}
			}
		};
		
	}

	
	//初始化
	public void init(){			
		findViewById(R.id.txt_right).setVisibility(View.GONE);
		btn_phoneCheck_next=  (Button) findViewById(R.id.btn_phoneCheck_next);
		btn_phoneCheck_getCode=(Button) findViewById(R.id.btn_phoneCheck_getCode);
		txt_title = (TextView) findViewById(R.id.txt_title);
		et_usertel=(EditText) findViewById(R.id.et_usertel);
		et_code=(EditText) findViewById(R.id.et_code);
		txt_title.setText("注册"); 
		setListener();		
	}
	
	//设置监听
	protected void setListener(){	
		btn_phoneCheck_next.setOnClickListener(this);
		btn_phoneCheck_getCode.setOnClickListener(this);
		et_usertel.addTextChangedListener(new TelTextChange());
		et_code.addTextChangedListener(new TextChange());
	}
	/*
	 * 手机号码监听器
	 */
	class TelTextChange implements TextWatcher{
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			String phone = et_usertel.getText().toString();
			if (phone.length() == 11) {
				if (Utils.isMobileNO(phone)) {
					btn_phoneCheck_getCode.setBackgroundDrawable(getResources().getDrawable(
							R.drawable.btn_bg_green));
					btn_phoneCheck_getCode.setTextColor(0xFFFFFFFF);
					btn_phoneCheck_getCode.setEnabled(true);
				} else {
					et_usertel.requestFocus();
					Utils.showLongToast(getApplicationContext(), "请输入正确的手机号码！");
				}
			} else {
				btn_phoneCheck_getCode.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.btn_enable_green));
				btn_phoneCheck_getCode.setTextColor(0xFFD0EFC6);
				btn_phoneCheck_getCode.setEnabled(false);
			}
		}

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	/*
	 * 验证码监听器
	 */
	class TextChange implements TextWatcher {

		@Override
		public void afterTextChanged(Editable arg0) {

		}

		@Override
		public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
				int arg3) {

		}

		@Override
		public void onTextChanged(CharSequence cs, int start, int before,
				int count) {
			boolean Sign1 = et_code.getText().length() > 0;
			boolean Sign2 = et_usertel.getText().length() > 0;			
			if (Sign1 & Sign2) {
				btn_phoneCheck_next.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.btn_bg_green));
				btn_phoneCheck_next.setTextColor(0xFFFFFFFF);
				btn_phoneCheck_next.setEnabled(true);
			} else {
				btn_phoneCheck_next.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.btn_enable_green));
				btn_phoneCheck_next.setTextColor(0xFFD0EFC6);
				btn_phoneCheck_next.setEnabled(false);
			}
		}
	}
	/* 定义一个倒计时的内部类 */
	private class MyCount extends CountDownTimer {
		public MyCount(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onFinish() {
			btn_phoneCheck_getCode.setEnabled(true);
			btn_phoneCheck_getCode.setText("重新获取验证码");
		}

		@Override
		public void onTick(long millisUntilFinished) {
			btn_phoneCheck_getCode.setEnabled(false);
			btn_phoneCheck_getCode.setText("(" + millisUntilFinished / 1000 + ")秒");
			
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_phoneCheck_getCode://获取验证码
			getCode();
			if (mc == null) {
				mc = new MyCount(60000, 1000); // 第一参数是总的时间，第二个是间隔时间
			}
			mc.start();			
			break;
		case R.id.btn_phoneCheck_next://下一步
			//将手机号码和验证码发送到服务器，返回的如果success=1，则跳转到
			//注册的详细界面的填写
			mPhoneNum=et_usertel.getText().toString().trim();
			mCode=et_code.getText().toString().trim();
			//开启线程，访问网络,暂时没法获取验证码hold 
			System.out.println("开始填写详细信息：");
			String params="mobile="+mPhoneNum+"&code="+mCode+"&userType=customer";
			System.out.println("详细信息："+params);
			Runnable checkCode=new CheckCode(params);
			new Thread(checkCode).start();
			CommonWidget.myProgressDialogShow(PhoneCheck.this, "正在跳转...请稍候。。。", "", false);
			//Utils.showShortToast(this, "下一步注册");
			break;
		default:
			break;
		}	
	}
	//获取验证码
	private void getCode(){
		Toast.makeText(this, "手机号："+et_usertel.getText().toString().trim(), 1).show();
		SMSSDK.getVerificationCode("86", et_usertel.getText().toString().trim());
	}
	//跳转到个人确认密码界面
	private void startUserRegisterDetail(String param){
		Intent intent = new Intent();
		
		intent.putExtra("phoneNum",param);
		System.out.println("phoneCheck:"+param);
		intent.setClass(PhoneCheck.this, RegisterCustomer.class);
		startActivity(intent);
		overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
		finish();
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		SMSSDK.unregisterAllEventHandler();
	}

    //访问数据库，获取验证是否成功
	class CheckCode implements Runnable{
        String para;
        String ruturnData;
        Message msg;
        public CheckCode(String paramer){
        	this.para=paramer;
        	msg=new Message();
        	System.out.println("验证码构造函数上的参数para："+para);
        }
		@Override
		public void run() {
			Looper.prepare();
			// TODO Auto-generated method stub
			try {
				ruturnData=HCPostRequest.sendPost(Constant.URL_REGISTER_CHECK_CODE, this.para);
				System.out.println("验证码页码上的ruturnData："+ruturnData);
				JSONObject  JsonOb=new JSONObject(ruturnData);
			    if(JsonOb.getString("success").equals("1")){
			    	Toast.makeText(getApplicationContext(), "验证成功！", 0).show();
			    	//跳转到注册的详细信息填写界面
			    	msg.what=1;    	
			    }else{
			    	msg.what=2;    	
			    }
			    mHandler.sendMessage(msg);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Looper.loop();
		}
	}
}
