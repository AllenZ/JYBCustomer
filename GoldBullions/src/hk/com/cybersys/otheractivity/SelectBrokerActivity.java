package hk.com.cybersys.otheractivity;

import hk.com.cybersys.activity.Appointment;
import hk.com.cybersys.activity.Products;
import hk.com.cybersys.activity.ProductsShow;
import hk.com.cybersys.adapter.BrokerBean;
import hk.com.cybersys.adapter.BrokerSelectAdapter;
import hk.com.cybersys.basic.R;
import hk.com.cybersys.http.HCPostRequest;
import hk.com.cybersys.myLibrary.CommonWidget;
import hk.com.cybersys.myLibrary.Constant;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import cao.jian.chen.utils.Utils;

public class SelectBrokerActivity extends Activity {

	private TextView txt_title;
	private ImageView backImg;
	private ListView brokerListView;
	private String brokerID;
	private Handler mHandler;
	private BrokerSelectAdapter brokerAdapter;
	private String returnData;
	private JSONArray brokerArray;
	private List<BrokerBean>  list=new ArrayList<BrokerBean>();;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_broker);
		initTitle();
	    //将数据库中的理财师显示到页面上
		showBroker();
		//给每个理财师添加点击事件
		//获得理财师的ID
		//将ID返回给上一个页面
		
	}
	
	//显示所有的理财师
	public void showBroker(){
		brokerListView=(ListView) findViewById(R.id.lv_select_broker);
		mHandler=new Handler(){
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub	
				switch (msg.what) {
				case 1:
					//System.out.println("这是SelectBrokerActivity上brokerArray的长度："+brokerArray.length());	
					for(int i=0;i<brokerArray.length();i++){
						 try {
							System.out.println("第"+i+"个brokerid："+brokerArray.getJSONObject(i).getString("brokerid"));
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
					for(int i=0;i<brokerArray.length();i++){
						BrokerBean brokerBean;
						try {		
							brokerBean=new BrokerBean(
									  brokerArray.getJSONObject(i).getString("brokerid"),
								      brokerArray.getJSONObject(i).getString("name"), 
								      brokerArray.getJSONObject(i).getString("details"),
								      brokerArray.getJSONObject(i).getString("iconname"),
								      brokerArray.getJSONObject(i).getInt("average_mark"),
								      brokerArray.getJSONObject(i).getInt("mark_count"),
								      brokerArray.getJSONObject(i).getInt("transaction_count"));		
							list.add(brokerBean);
							//System.out.println(list.);
							//如果Icon为null则设置默认头像
							if(list.get(i).getBrokerIcon()==null){
								list.get(i).setBrokerIcon("55805cd7ea147.jpeg");
							}				
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					break;
				default:
					break;
				}
				System.out.println("加入到list后的数据list大小："+list.size());
				System.out.println("选择broker开始适配：");
				brokerAdapter=new BrokerSelectAdapter(SelectBrokerActivity.this,list);
				brokerListView.setAdapter(brokerAdapter);
				brokerListView.setDivider(new ColorDrawable(Color.WHITE));  
				brokerListView.setDividerHeight(2);
				//添加点击事件
				brokerListView.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						// TODO Auto-generated method stub
						Intent intent = new Intent();
						Bundle bundle = new Bundle();
						//将ProductID做为参数传递过去
						bundle.putString("brokerName", list.get(position).getBrokerName());
						bundle.putString("isSelectedBrokerID", list.get(position).getBrokerID());
						intent.putExtras(bundle);
						intent.setClass(SelectBrokerActivity.this,Appointment.class);
						startActivity(intent);
					}
				
				});
				super.handleMessage(msg);
			}
			
		};
		
		Runnable  getFullBroker=new GetFullBroker(mHandler);
		new Thread(getFullBroker).start();
		CommonWidget.myProgressDialogShow(this, "正在加载理财师...", "", false);
		
	}
	//开启一个线程获取所有的Broker
	private class GetFullBroker implements Runnable {
		private JSONObject brokerOb;
		private Handler handler;
		
		public GetFullBroker(Handler handler){
			System.out.println("这是获取所有Broker的构造函数！");
			this.handler=handler;
		}
		//开始获取所有的Broker
		public void run() {
			Looper.prepare();
			returnData=HCPostRequest.sendPost(Constant.URL_GET_BROKER,null);
			try {
				brokerOb=new JSONObject(returnData);
				//System.out.println("——————这是获取到的所有的broker字段："+brokerOb);
				brokerArray=brokerOb.getJSONArray("list");
				System.out.println("+++++这是brokerArray"+brokerArray);		
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Message msg=new Message();
			msg.what=1;
			handler.sendMessage(msg);
		}
	}
	
	//初始化Title
	public void initTitle(){
		backImg=(ImageView) findViewById(R.id.img_back);
		backImg.setVisibility(View.VISIBLE);
		backImg.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//System.out.println("哈哈哈哈！");
				Utils.finish(SelectBrokerActivity.this);
			}
		});
		findViewById(R.id.txt_right).setVisibility(View.GONE);
		txt_title = (TextView) findViewById(R.id.txt_title);
		txt_title.setText("理财经理"); 
	}
}
