package hk.com.cybersys.otheractivity;


import hk.com.cybersys.adapter.TradeRecordBean;
import hk.com.cybersys.basic.R;
import hk.com.cybersys.basic.R.color;
import hk.com.cybersys.myLibrary.Constant;
import cao.jian.chen.utils.LoadIcon;
import cao.jian.chen.utils.Utils;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class TransationDetailsiActivity extends Activity {

	private LoadIcon loadIcon;
	private TextView txt_title;
	private ImageView backImg,img_icon;
	private TextView tv_userName,tv_backup,tv_transationDetailsi_trandeSum,tv_productCatory,tv_productName,tv_trandsactionStatus;
	private TradeRecordBean mTradeRecordBean;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_transation_detailsi);
		initTitle();
		initView();
		//填充数据
		initData();
		
	}
    private void initData(){
    	loadIcon=new LoadIcon();
    	Intent intent=this.getIntent();
    	mTradeRecordBean=(TradeRecordBean) intent.getSerializableExtra("TradeRecordBean");
    	tv_userName.setText(mTradeRecordBean.getmBrokerName());
    	tv_backup.setText("无");
    	tv_backup.setTextColor(color.blue);
    	tv_transationDetailsi_trandeSum.setText(mTradeRecordBean.getmTrandSum()+"万");
    	tv_productCatory.setText(mTradeRecordBean.getmCategory());
    	tv_productName.setText(mTradeRecordBean.getmProductName());
    	if(mTradeRecordBean.getmState()==0){
    		tv_trandsactionStatus.setText("未审核");
    	}else{
    		tv_trandsactionStatus.setText("已审核");
    	}  
    	loadIcon.setRemoteImageListener(Constant.URL_GET_BROKERICON_ON_RECORD+mTradeRecordBean.getmUserPic(),
				new LoadIcon.OnRemoteImageListener() {
			@Override
			public void onRemoteImage(Bitmap image) {
				// TODO Auto-generated method stub
				img_icon.setImageBitmap(image);
			}		
			@Override
			public void onError(String error) {
				// TODO Auto-generated method stub
				System.out.println("在交易记录上下载broker的头像原图失败"+error);
			}
		});
    }
	private void initView(){
		img_icon=(ImageView) findViewById(R.id.img_icon);
		tv_userName=(TextView) findViewById(R.id.tv_userName);
		tv_backup=(TextView) findViewById(R.id.tv_backup);
		tv_transationDetailsi_trandeSum=(TextView) findViewById(R.id.tv_transationDetailsi_trandeSum);
		tv_productCatory=(TextView) findViewById(R.id.tv_productCatory);
		tv_productName=(TextView) findViewById(R.id.tv_productName);
		tv_trandsactionStatus=(TextView) findViewById(R.id.tv_trandsactionStatus);
	}
	private void initTitle(){
		backImg=(ImageView) findViewById(R.id.img_back);
		txt_title = (TextView) findViewById(R.id.txt_title);
  		backImg.setVisibility(View.VISIBLE);
  		backImg.setOnClickListener(new View.OnClickListener() {
  			@Override
  			public void onClick(View v) {
  				// TODO Auto-generated method stub
  				//System.out.println("哈哈哈哈！");
  				Utils.finish(TransationDetailsiActivity.this);
  			}
  		});
  		findViewById(R.id.txt_right).setVisibility(View.GONE);
  		txt_title.setText("交易详情");
	}
	
	
}
