package hk.com.cybersys.service;

import hk.com.cybersys.activity.MainActivity;
import hk.com.cybersys.basic.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
/*
 * 推送服务，用于预约成功后，定时向APP推送预约见面的消息
 * @author By AllenZ
 * @Time 2015-8-20 9:15
 * 
 */
public class PushService extends Service {

	static Timer timer=null;
	private int delay;
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(final Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub		
		System.out.println("onStartCommand执行了");
		//long period = 24*60*60*1000; //24小时一个周期
		SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//获取系统的当前时间
		
		String currentTime=df.format(new Date());
		System.out.println("系统当前时间currentTime："+currentTime);
		//获取预约时间
		String appointmentTime=intent.getStringExtra("appointmentTime");
		System.out.println("PushService上的appointmentTime："+appointmentTime);
		Date d1;
		Date d2;	
		try {
			d1 = df.parse(appointmentTime);
		    d2=df.parse(currentTime);
		    System.out.println("PushService上的d1："+d1);
		    System.out.println("PushService上的d2："+d2);
			//计算时间差
			long time=d1.getTime()-d2.getTime();
			System.out.println("PushService上的time："+time);
			//延迟时间
			delay=(int)(time-3600000);
			System.out.println("PushService上的delay："+delay);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (null == timer ) {
		    timer = new Timer();
		}
		timer.schedule(new TimerTask() {
			             @Override
			   public void run() {
		           // TODO Auto-generated method stub
			      NotificationManager mn= (NotificationManager) PushService.this.getSystemService(NOTIFICATION_SERVICE);    
		          Notification.Builder builder = new Notification.Builder(PushService.this);
		          Intent notificationIntent = new Intent(PushService.this,MainActivity.class);//点击跳转位置                
		          PendingIntent contentIntent = PendingIntent.getActivity(PushService.this,0,notificationIntent,0);                
		          builder.setContentIntent(contentIntent);
		          builder.setSmallIcon(R.drawable.ic_launcher);
		          builder.setTicker(intent.getStringExtra("tickerText")); //测试通知栏标题
			      builder.setContentText(intent.getStringExtra("contentText")); //下拉通知啦内容
			      builder.setContentTitle(intent.getStringExtra("contentTitle"));//下拉通知栏标题
			      builder.setAutoCancel(true);
		          builder.setDefaults(Notification.DEFAULT_ALL);
			      Notification notification = builder.build();
			      mn.notify((int)System.currentTimeMillis(),notification);
		      }
		},delay);
		
		return super.onStartCommand(intent, flags, startId);
		
	}	 	
}
