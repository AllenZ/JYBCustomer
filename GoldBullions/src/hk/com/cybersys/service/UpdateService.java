package hk.com.cybersys.service;

import cao.jian.chen.utils.Constants;
import cao.jian.chen.utils.Utils;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
/*
 * 广播服务，监听用户的事件,注册广播，通知MainActivity刷新数据
 * @author By AllenZ
 * @Time 2015-8-6 15:35
 * 
 */
public class UpdateService extends Service {

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub		
		if(Utils.getBooleanValue(getApplicationContext(),Constants.LoginState )){
			//登录
			System.out.println("启动服务：啦啦啦啦lalala");
			sendBrodcast("Collection");
		}else{
			//未登录
			
		}
		
		return super.onStartCommand(intent, flags, startId);
		//启动服务
		
		
	}
	
	
	//发送广播
	private void sendBrodcast(String Action){
		Intent intent = new Intent();
		intent.setAction("hk.com.cybersys.basic");
		intent.putExtra("Action",Action);
		sendBroadcast(intent);
	}
}
