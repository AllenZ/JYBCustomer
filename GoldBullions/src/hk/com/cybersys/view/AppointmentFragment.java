package hk.com.cybersys.view;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cao.jian.chen.utils.Constants;
import cao.jian.chen.utils.LoadIcon;
import cao.jian.chen.utils.Utils;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.map.BaiduMap.OnMapTouchListener;
import com.baidu.mapapi.map.BaiduMap.OnMarkerClickListener;
import com.baidu.mapapi.model.LatLng;
import com.baidu.navisdk.util.SysOSAPI;

import hk.com.cybersys.activity.Appointment;
import hk.com.cybersys.activity.Login;
import hk.com.cybersys.activity.MapReservation;
import hk.com.cybersys.activity.MapReservation.MyLocationListener;
import hk.com.cybersys.basic.R;
import hk.com.cybersys.http.HCPostRequest;
import hk.com.cybersys.myLibrary.Constant;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class AppointmentFragment extends Fragment {
	private Activity ctx;
	private View layout;
	private WebView wv;
	private MapView mMapView ;
	private BaiduMap bMap ;
	// 定位相关
	LocationClient mLocClient;
	public MyLocationListener myListener = new MyLocationListener();
	boolean isFirstLoc = true;// 是否首次定位
	
	private long exitTime = 0;
	private String brokerStr;
	public TextView mapBrokerName;
	public TextView mapBrokerAddress;
	public TextView tv_average_mark,tv_mark_count,tv_transaction_count;
	public ImageView img_appointment_broker_icon;
	
	public RelativeLayout brokerRelativeLayout;
	private LoadIcon loanIcon;
	
	private String brokerName;
	private String isSelectedBrokerID;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		SDKInitializer.initialize(getActivity().getApplicationContext());
		 if(layout==null){
   		    ctx = this.getActivity();
	    	layout = ctx.getLayoutInflater().inflate(R.layout.activity_map_appointment,
						null);
	    	initView();
   	  }else{
   		  ViewGroup parent = (ViewGroup) layout.getParent();
				if (parent != null) {
					parent.removeView(layout);
				 }
	     //refresh();
   	  }
		
		return layout;
	}
	//初始化地图布局
	public void initView(){
		mMapView = (MapView)layout. findViewById(R.id.bmapView);	
		bMap = mMapView.getMap();
		loanIcon=new LoadIcon();
		// 开启定位图层
		bMap.setMyLocationEnabled(true);
		this.mLocClient = new LocationClient(getActivity().getApplicationContext());
		this.mLocClient.registerLocationListener(this.myListener);
		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(true);// 打开gps
		option.setCoorType("bd09ll"); // 设置坐标类型
		option.setScanSpan(1000);
		mLocClient.setLocOption(option);
		mLocClient.start();
		brokerRelativeLayout = (RelativeLayout)layout.findViewById(R.id.showBroker);
		this.mapBrokerName = (TextView)this.brokerRelativeLayout.findViewById(R.id.mapBrokerName);
		this.mapBrokerAddress = (TextView)this.brokerRelativeLayout.findViewById(R.id.mapBrokerAddress);
		tv_average_mark=(TextView) this.brokerRelativeLayout.findViewById(R.id.tv_average_mark);
		tv_mark_count=(TextView) this.brokerRelativeLayout.findViewById(R.id.tv_mark_count);
		tv_transaction_count=(TextView) this.brokerRelativeLayout.findViewById(R.id.tv_transaction_count);
		img_appointment_broker_icon=(ImageView) this.brokerRelativeLayout.findViewById(R.id.img_appointment_broker_icon);
		brokerRelativeLayout.setVisibility(View.GONE);
		
		Button appointBut = (Button)layout.findViewById(R.id.appointment_button);
		//点击预约，进入选择预约经纪人的界面
		appointBut.setOnClickListener(new OnClickListener() {		
			@Override
			public void onClick(View arg0) {
				SharedPreferences settings = getActivity().getSharedPreferences("LoginStatus", 0);
				String isLogin = settings.getString("isLogin", "");
				if(Utils.getBooleanValue(getActivity(),Constants.LoginState )){
					Bundle bundle=new Bundle();
				    bundle.putString("brokerName", brokerName);
				    bundle.putString("isSelectedBrokerID", isSelectedBrokerID);
					Intent intent = new Intent();
					intent.putExtras(bundle);
					intent.setClass(getActivity(), Appointment.class);
					startActivity(intent);
					mMapView.onPause();		
				}else{
					Toast.makeText(getActivity(), "请登陆后，再预约！",Toast.LENGTH_SHORT).show();
					Intent intent = new Intent();
					intent.setClass(getActivity(), Login.class);
					startActivity(intent);
					mMapView.onPause();
				}
			}
		});
        StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		brokerStr = HCPostRequest.sendPost(Constant.URL_GET_FULLBROKER, "");
			
		try {
			JSONObject jsonObject = new JSONObject(brokerStr);
			JSONArray jsonArray = jsonObject.getJSONArray("list");
			for(int i=0;i < jsonArray.length();i++){
				LatLng point = new LatLng(Double.parseDouble(jsonArray.getJSONObject(i).getString("latitude")),Double.parseDouble(jsonArray.getJSONObject(i).getString("longtitude")));
				
				BitmapDescriptor bitmap = new BitmapDescriptorFactory().fromResource(R.drawable.broker_position);
				OverlayOptions option22 = new MarkerOptions().position(point).icon(bitmap);
				bMap.addOverlay(option22);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
     bMap.setOnMarkerClickListener(new OnMarkerClickListener() {		
			@Override
			public boolean onMarkerClick(Marker marker) {
				// TODO Auto-generated method stub
				if(brokerRelativeLayout.getVisibility() != View.GONE){
					//mBtnReservation.setVisibility(View.VISIBLE);
				}else{
					brokerRelativeLayout.setVisibility(View.VISIBLE);
					//mBtnReservation.setVisibility(View.GONE);
					JSONObject jsonObject;
					JSONArray jsonArray;
					try {
						jsonObject = new JSONObject(brokerStr);
						jsonArray = jsonObject.getJSONArray("list");
						for(int i=0;i < jsonArray.length();i++){
							if(marker.getPosition().latitude == Double.parseDouble(jsonArray.getJSONObject(i).getString("latitude"))){
								mapBrokerName.setText(jsonArray.getJSONObject(i).getString("name"));
								mapBrokerAddress.setText(jsonArray.getJSONObject(i).getString("details"));
								tv_average_mark.setText(jsonArray.getJSONObject(i).getString("average_mark")+"分");
								tv_mark_count.setText(jsonArray.getJSONObject(i).getString("mark_count")+"人");
								tv_transaction_count.setText(jsonArray.getJSONObject(i).getString("transaction_count")+"单");								
								
								loanIcon.setRemoteImageListener(Constant.URL_DOWNLOAD_BROKER_IMG+jsonArray.getJSONObject(i).getString("iconname"),
										new LoadIcon.OnRemoteImageListener() { 
						                      public void onError(String error) {  
						                         //Toast.makeText(context, error, Toast.LENGTH_LONG).show();  
						            	         System.out.println("地图上的下载图片失败:"+error);
						                      }  
						                      public void onRemoteImage(Bitmap image) {  
						            	         img_appointment_broker_icon.setImageBitmap(image);  
						                      } 
						            });
								//以下要传给预约界面的数据
								brokerName = jsonArray.getJSONObject(i).getString("name");
								isSelectedBrokerID = jsonArray.getJSONObject(i).getString("brokerid");
							}			
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}				
				}
				return false;
			}
		});
		bMap.setOnMapTouchListener(new OnMapTouchListener() {		
			@Override
			public void onTouch(MotionEvent arg0) {
				// TODO Auto-generated method stub
				brokerRelativeLayout.setVisibility(View.GONE);
			}
		});		
	}
	
	public class MyLocationListener implements BDLocationListener {
		@Override
		public void onReceiveLocation(BDLocation location) {
			// map view 销毁后不在处理新接收的位置
			if (location == null || mMapView == null){
				return;
			}
//			System.out.println("维度"+location.getLatitude());
//			System.out.println("经度"+location.getLongitude());
			MyLocationData locData = new MyLocationData.Builder()
					.accuracy(location.getRadius())
					// 此处设置开发者获取到的方向信息，顺时针0-360
					.direction(100).latitude(location.getLatitude())
					.longitude(location.getLongitude()).build();
			bMap.setMyLocationData(locData);
			if (isFirstLoc) {
				isFirstLoc = false;
				LatLng ll = new LatLng(location.getLatitude(),
						location.getLongitude());
				MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(ll);
				bMap.animateMapStatus(u);
			}
		}

	}

	@Override
	public void onDestroy() {
		// 退出时销毁定位
		if(mLocClient!=null){
			mLocClient.stop();
		}	
		// 关闭定位图层
		bMap.setMyLocationEnabled(false);
		mMapView.onDestroy();
		mMapView = null;
		System.out.println("AppointmentFragment上调用onDestroy()");
		super.onDestroy();
		// 在activity执行onDestroy时执行mMapView.onDestroy()，实现地图生命周期管理
	   //  mMapView.onDestroy();
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		//System.out.println("调用onStart()");
		//Fragment 生命周期的管理
	     // refresh();
	}
	@Override
	public void onResume() {
		  refresh();
		// 在activity执行onResume时执行mMapView. onResume ()，实现地图生命周期管理
		//System.out.println("调用onResume()");
		mMapView.onResume();		
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		// 在activity执行onPause时执行mMapView. onPause ()，实现地图生命周期管理
		mMapView.onPause();
	}
	public void refresh(){
		initView();
	}
}
