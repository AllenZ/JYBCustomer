package hk.com.cybersys.view;

import hk.com.cybersys.activity.Login;
import hk.com.cybersys.activity.ProductsShow;
import hk.com.cybersys.adapter.MyAdapter;
import hk.com.cybersys.adapter.ProductBean;
import hk.com.cybersys.basic.R;
import hk.com.cybersys.http.HCPostRequest;
import hk.com.cybersys.myLibrary.Constant;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cao.jian.chen.utils.Constants;
import cao.jian.chen.utils.Utils;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

public class CollectionFragment extends Fragment implements OnClickListener {

	public  ListView listView;
	private List<ProductBean> productlist ;// 封装Bean
	private ProductBean product;
	private LinearLayout myCollectionBar = null;	
	private Button collectionToLogin = null;
	private MyAdapter myAdapter;
	private Handler collectionHandler;
	private Handler myHandler;

	private long exitTime = 0;
	private int activityNum = 2;
	
	private String customerID;
	private JSONArray productArray;
	private int delIndex;
	private int favorid; 
	private View layout;
	private Activity ctx;
	private boolean isCollect=false;

	
	@Override
	public View onCreateView(LayoutInflater inflater,
			 ViewGroup container,  Bundle savedInstanceState) {
			if (layout == null) {
				System.out.println("执行了Fragment上的onCreateView方法");
				ctx = this.getActivity();
				layout = ctx.getLayoutInflater().inflate(R.layout.activity_collection,
						null);
				initViews();
				init();				
			} else {
				ViewGroup parent = (ViewGroup) layout.getParent();
				if (parent != null) {
					parent.removeView(layout);
				}
			}		 
		    return layout;
	}	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}
	public void initViews(){
		collectionToLogin=(Button) layout.findViewById(R.id.collection_tologin);
		listView=(ListView) layout.findViewById(R.id.collection_list_view);
		collectionToLogin.setOnClickListener(this);
	}
	public void init(){		
		if(Utils.getBooleanValue(getActivity(),Constants.LoginState )){
		    //是否已经收藏了
			initData();					
		}else{
			//System.out.println("CollectionFragment未登录"+Utils.getBooleanValue(getActivity(),Constants.LoginState ));
			collectionToLogin.setVisibility(android.view.View.VISIBLE);
			listView.setVisibility(android.view.View.GONE);
		}
	}
	//从数据库中获取数据
	private void initData(){
		
		productlist= new ArrayList<ProductBean>();
		collectionHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {				
				switch (msg.what) {
				case 0:
					listView.setVisibility(android.view.View.GONE);
					collectionToLogin.setText("亲，还没有收藏哦！赶快去收藏吧 ");
					break;
				case 1:			
					collectionToLogin.setVisibility(android.view.View.GONE);					
					System.out.println("productArray的长度："+productArray.length());
					if(productlist.size()>0){
						productlist.clear();
					}
					//封装bean				
					  for(int i = 0; i < productArray.length(); i++){
						try {
							product=new ProductBean(productArray.getJSONObject(i).getString("threshold") ,
							                        productArray.getJSONObject(i).getString("name"),
							                        productArray.getJSONObject(i).getString("starttime"),
							                        productArray.getJSONObject(i).getString("productid"),
							                        productArray.getJSONObject(i).getInt("promotion"),
							                        productArray.getJSONObject(i).getInt("maxprofit"),
							                        productArray.getJSONObject(i).getInt("minperiod"),
							                        productArray.getJSONObject(i).getString("period"),
										            productArray.getJSONObject(i).getInt("progress"));
							productlist.add(product);
						} catch (JSONException e) {	
							e.printStackTrace();
						}	  
					}
					break;
				case 2:
					System.out.println("开始删除");
					productlist.remove(delIndex);
					//listView.removeViewAt(delIndex);
					//((List<ProductBean>) listView).remove(delIndex);
					myAdapter.notifyDataSetChanged();
					break;

				default:
					break;
				}
				System.out.println("collectionHandler上的productlistBean:");
				for(int i=0;i<productlist.size();i++){
					System.out.println("第"+i+"个product项目名："+productlist.get(i).getProductTitle());
				}
				// 根据CustomerID封装产品bean
				myAdapter = new MyAdapter(getActivity(), getActivity(),
						productlist, 4);
				listView.setAdapter(myAdapter);
				listView.setDivider(new ColorDrawable(Color.WHITE));
				listView.setDividerHeight(10);
				// 每个item的监听事件
				listView.setOnItemClickListener(new OnItemClickListener(){
					@Override
					public void onItemClick(AdapterView<?> parent,
							View view, int position, long id) {
						System.out.println("短按事件的发生");
						Intent intent = new Intent();
						Bundle bundle = new Bundle();
						bundle.putString("ProductID",
								productlist.get(position).getProductID());
						intent.putExtras(bundle);
						intent.setClass(getActivity(), ProductsShow.class);
						startActivity(intent);
						
					}
				});
				//长按删除收藏
				listView.setOnItemLongClickListener(new OnItemLongClickListener() {
					public boolean onItemLongClick(AdapterView<?> parent,
							View view, int position, long id) {
						System.out.println("长按事件的发生");
						// TODO Auto-generated method stub
						try {
							delIndex=position;
							//System.out.println(delIndex+"/////////////");
							favorid=Integer.valueOf(productArray.getJSONObject(position).get("favorid").toString()) ;
							//Runnable  del=new DelCollection(i);
						    //new Thread(del).start();
							showMyDialog(favorid);
							
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						return true;
					}
				});			
				super.handleMessage(msg);
			}
		};		
		if(Utils.getValue(getActivity(), Constants.CustomerID)!=null){
			customerID =  Utils.getValue(getActivity(), Constants.CustomerID);			
		}else{
			customerID=1+"";
			System.out.println("这是退出后的customerID"+customerID);
		}
	     //System.out.println("这是收藏节目上的customerID:"+customerID);
		//System.out.println("收藏节目开始获取收藏的产品");
		Runnable favorProduct = new GetFavorProduct(collectionHandler);
		new Thread(favorProduct).start();		
	}
	

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		switch (v.getId()) {
		case R.id.collection_tologin:
			intent.putExtra("activityNum", activityNum);
			intent.setClass(getActivity(), Login.class);
			startActivity(intent);
			break;

		default:
			break;
		}
		
	}
	
	// 获得用户收藏的的产品,根据用户id获取收藏的id
	class GetFavorProduct implements Runnable {
		private JSONObject productJsonOb;
		private String product_json_str;
		private Handler handler;
		private  Message message;

		public GetFavorProduct(Handler handler) {
				this.handler = handler;
			    message = new Message();
			    
				System.out.println("收藏上的构造函数");
		}		
		@Override
		public void run() {
			System.out.println("customerID:"+customerID);
			String data = HCPostRequest.sendPost(Constant.URL_FAVOR_GET,
						"customerid=" + customerID);
			System.out.println("收藏上的data："+data);
			try {
				   JSONObject jsonOb = new JSONObject(data);
				   if(jsonOb.get("success").equals("0")){
					   message.what = 0;
				   }else{
				       productArray = jsonOb.getJSONArray("list");
				       isCollect=true;
				       message.what = 1;
				   }
			} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			}
			// 发送消息更新UI
		    this.handler.sendMessage(message);
			}
	}
	
	//删除收藏
	class DelCollection implements Runnable{
		private int favorid;
		public DelCollection(int favorid){
				this.favorid=favorid;
		}
		@Override
		public void run() {
			// TODO Auto-generated method stub
			System.out.println(favorid+"favorid+++++++++++++++++");
			String data = HCPostRequest.sendPost(Constant.URL_DEL_Favor_BY_FAVORID,
						"favorid=" + favorid);
			System.out.println("data:"+data);
			try {
				JSONObject jsonOb = new JSONObject(data);
				System.out.println("jsonOb:"+jsonOb);
				if(jsonOb.getString("success").equals("1")){
					System.out.println("删除收藏成功！");
					Message msg=new Message();
					msg.what=2;
					System.out.println("msg.what:"+msg.what);
					collectionHandler.sendMessage(msg);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
	}
	
	//提示的Dialog
	public void showMyDialog(final int i){
	    System.out.println("进入到dialog上");
		AlertDialog.Builder dialogBuild=new Builder(getActivity());
		dialogBuild.setMessage("删除该收藏!");
		dialogBuild.setTitle("提示");
		dialogBuild.setPositiveButton("确认", new DialogInterface.OnClickListener() {	
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
			    System.out.println("确定删除！");
				Runnable  del=new DelCollection(i);
				new Thread(del).start();
			    dialog.dismiss();
			   // Collection.this.finish();
			}	
		});
		dialogBuild.setNeutralButton("取消", new DialogInterface.OnClickListener() {	
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				System.out.println("取消删除！");
				 dialog.dismiss();
				// Collection.this.finish();
			}
		});
		dialogBuild.create().show();
}

    //刷新
	public void refresh(){
		init();
	}
    
}
