package hk.com.cybersys.view;

import hk.com.cybersys.activity.AppointmentRecord;
import hk.com.cybersys.adapter.AppointmentAdapter;
import hk.com.cybersys.adapter.AppointmentBean;
import hk.com.cybersys.basic.R;
import hk.com.cybersys.http.HCPostRequest;
import hk.com.cybersys.myLibrary.CommonWidget;
import hk.com.cybersys.myLibrary.Constant;
import hk.com.cybersys.otheractivity.AppointmentDetailActivity;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import cao.jian.chen.utils.Constants;
import cao.jian.chen.utils.Utils;

/*
 * 预约的历史记录，
 * @author By AllenZ
 */
public class HistoryFragment extends Fragment{

	private Activity ctx;
	private View layout;
	private ListView listview;
	private TextView appoointment_norecord;
	private String customerID;
	private AppointmentRecord parentActivity;
	private JSONObject returnData;
	private JSONArray jsonArrayData;
	private Handler handler;
	private List<AppointmentBean> appointmentBeanHisList=new ArrayList<AppointmentBean>();
	
	public View onCreateView(LayoutInflater inflater,
			 ViewGroup container,  Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if(layout==null){
			ctx=getActivity();
			parentActivity = (AppointmentRecord) getActivity();
			layout = ctx.getLayoutInflater().inflate(R.layout.layout_appointment_attent,
					null);		
			init();
		}else {
			ViewGroup parent = (ViewGroup) layout.getParent();
			if (parent != null) {
				parent.removeView(layout);
			}
		}					
		return layout;
	}
	
	public void init(){
		listview = (ListView)layout.findViewById(R.id.appointment_record_listview);
		appoointment_norecord=(TextView) layout.findViewById(R.id.appoointment_norecord);
		customerID =  Utils.getValue(getActivity(), Constants.CustomerID);
		handler=new Handler(){
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				super.handleMessage(msg);
				switch (msg.what) {
				case 1:
					try {
						jsonArrayData = returnData.getJSONArray("list");
						System.out.println("wo这里length："+jsonArrayData.length());
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					   try {
					      for(int i=0;i < jsonArrayData.length();i++){
						     AppointmentBean appointmentbean;
							 if(jsonArrayData.getJSONObject(i).getInt("status")!=1){
								appointmentbean=new AppointmentBean(
										jsonArrayData.getJSONObject(i).getInt("status"),
										jsonArrayData.getJSONObject(i).getInt("appointmentid"),
										jsonArrayData.getJSONObject(i).getString("brokermobile"), 
										jsonArrayData.getJSONObject(i).getInt("brokerid"), 
										jsonArrayData.getJSONObject(i).getString("brokericonname"), 
										jsonArrayData.getJSONObject(i).getString("brokername"),
										jsonArrayData.getJSONObject(i).getString("productname1"), 
										jsonArrayData.getJSONObject(i).getString("productname2"), 
										jsonArrayData.getJSONObject(i).getString("productname3"),
										jsonArrayData.getJSONObject(i).getString("time"),
										jsonArrayData.getJSONObject(i).getString("date"),
										jsonArrayData.getJSONObject(i).getInt("acceptance"),
										jsonArrayData.getJSONObject(i).getInt("evaluationid")
										);
								appointmentBeanHisList.add(appointmentbean);
							}	
					     }
					  } catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					  }
				    
					 AppointmentAdapter appointadapter=new AppointmentAdapter(getActivity(), appointmentBeanHisList);
					 listview.setAdapter(appointadapter);
					  //System.out.println("预约记录上listview的conunt"+listview.getCount());
					 listview.setDivider(new ColorDrawable(Color.WHITE));  
					 listview.setDividerHeight(7);
					//点击listView进入到预约的详细页面
					  listview.setOnItemClickListener(new OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent, View view,
								int position, long id) {
							// TODO Auto-generated method stub
							Bundle myBundle=new Bundle();
							myBundle.putSerializable("appointRecBean", appointmentBeanHisList.get(position));						
							Toast.makeText(getActivity(), "点击的历史记录listview的position："+position, 1).show();
							Intent myIntent=new Intent();
							myIntent.putExtras(myBundle);
							myIntent.setClass(getActivity(), AppointmentDetailActivity.class);
							startActivity(myIntent);					
						}
					});
					break;
				case 2:
					listview.setVisibility(View.GONE);
					appoointment_norecord.setVisibility(View.VISIBLE);
					appoointment_norecord.setText("亲,还没有预约记录哦%%");
					break;
				default:
					break;
				}
				
			}
		};
		System.out.println("历史记录上的开始执行");
		Runnable r = new GetAppointmentRecord(customerID);
		new Thread(r).start();	
		CommonWidget.myProgressDialogShow(getActivity(), "正在加载...，请稍候！", "", false);
	}
	
	
	class GetAppointmentRecord implements Runnable{
		private String parameter;
		Message msg;
		public GetAppointmentRecord(String parameter){
			this.parameter =  parameter;
			 msg= new Message();
		}
		@Override
		public void run() {
			try {
				//returnData就是一个JSON对象
				System.out.println("HistoryFragment上的customerid"+parameter);
				returnData = new JSONObject(HCPostRequest.sendPost(Constant.URL_APPOINTMENT_GET_BY_CUSTOMERID, 
						                   "customerid="+parameter));
				System.out.println(returnData+"++++++++++++++++++++获得的returnData");
				if(returnData.getString("success").equals("0")){
					//预约记录为0
					msg.what=2;
				}else{
					//预约记录不为0
					msg.what=1;
				}				
				/*if(returnData.getString("success").equals("1")){
					msg.what=1;
				}else if(returnData.getString("error").equals("无记录")){
					msg.what = 3;
				}*/
				//System.out.println("这是GetAppointmentRecord上发送的msg.what:"+msg.what);
				
				handler.sendMessage(msg);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
}
