package hk.com.cybersys.view;

import hk.com.cybersys.activity.AppointmentRecord;
import hk.com.cybersys.activity.IndividualCenterInformation;
import hk.com.cybersys.activity.MyNews;
import hk.com.cybersys.activity.MyTrading;
import hk.com.cybersys.activity.NewMsg;
import hk.com.cybersys.activity.Setting;
import hk.com.cybersys.basic.R;

import java.util.Hashtable;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import cao.jian.chen.utils.Constants;
import cao.jian.chen.utils.Utils;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.onekeyshare.OnekeyShare;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

/*
 * 个人中心
 * @author by AllenZ
 */
public class PersonalCenterFragment extends Fragment {
	private View layout;
	private Activity ctx;
	private onLogoutListener mLogOutCallBack;
	private TextView my_account = null;
	private TextView my_loginNmae = null;
	// private Button my_login_but = null;
	private LinearLayout individual_center_account_information;
	private TextView myTradingBut, myAppointmentsBut, setting, newMsg, myNew;
	private ImageView qr_code_iv;
	private SharedPreferences settings;
	private LayoutInflater inflater;
	private View myCode;
	private Button btn_shareQRcode;
	private ImageView img_code;
	private Context mContext;
	

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		// 这是为了保证Activity容器实现了用以回调的接口。如果没有，它会抛出一个异常。
		try {
			mLogOutCallBack = (onLogoutListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement LogOut的回调函数");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (layout == null) {
			// System.out.println("第一次进来个人中心");
			ctx = this.getActivity();
			layout = ctx.getLayoutInflater().inflate(
					R.layout.activity_individual_center, null);
			initView();
		} else {
			// System.out.println("第二次进来个人中心=======");
			ViewGroup parent = (ViewGroup) layout.getParent();
			if (parent != null) {
				parent.removeView(layout);
			}
			initView();
		}
		return layout;
	}
	

	// 初始化数据
	private void initView() {

		my_loginNmae = (TextView) layout.findViewById(R.id.myName);
		my_account = (TextView) layout.findViewById(R.id.my_account);
		// my_login_but = (Button)layout.findViewById(R.id.my_login_but);
		individual_center_account_information = (LinearLayout) layout
				.findViewById(R.id.individual_center_account_information);
		qr_code_iv = (ImageView) layout.findViewById(R.id.qr_code_iv);
		newMsg = (TextView) layout.findViewById(R.id.newMsg);
		myNew = (TextView) layout.findViewById(R.id.myNew);
		myTradingBut = (TextView) layout.findViewById(R.id.myTradingBut);
		myAppointmentsBut = (TextView) layout
				.findViewById(R.id.myAppointmentsBut);
		setting = (TextView) layout.findViewById(R.id.setting);

		if (Utils.getBooleanValue(getActivity(), Constants.LoginState)) {
			// 登录成功
			System.out.println("登录成功！");
			this.my_account.setText("账号："
					+ Utils.getValue(getActivity(), Constants.Account));
			this.my_loginNmae.setText(Utils.getValue(getActivity(),
					Constants.UserName));
			// this.my_login_but.setText("退出登陆");
		} else {
			System.out.println("登录失败，原因不知");

		}
		individual_center_account_information
				.setOnClickListener(new ButtonListener());
		// my_login_but.setOnClickListener(new View.OnClickListener() {
		// @Override
		// public void onClick(View v) {
		// // TODO Auto-generated method stub
		// Button b = (Button)v;
		// if(b.getText().equals("退出登陆")){
		// my_loginNmae.setText("你好，欢迎使用金元宝！");
		// my_account.setText("账号：" + "尚未登录");
		// my_login_but.setText("登陆");
		// //清除缓存
		// Utils.RemoveValue(getActivity(), Constants.Account);
		// Utils.RemoveValue(getActivity(),Constants.LoginState );
		// Utils.RemoveValue(getActivity(), Constants.CustomerID);
		// Utils.RemoveValue(getActivity(), Constants.UserName);
		// //通知其他页面缓存已经清除
		// System.out.println("开始调用Logout上的回调");
		// mLogOutCallBack.refCollectFragment();
		// //RegistrationReceiver(new MyBroadcastReceiver(),
		// // new IntentFilter("hk.com.cybersys"));
		// //Intent myIntent=new Intent();
		// //settings.edit().clear().commit();
		// }else{
		// //1代表从个人中心进入
		// startActivityForResult(new Intent(getActivity(),Login.class), 1);
		// //startActivity(new Intent(getActivity(),Login.class));
		// getActivity().finish();
		// }
		// }
		// });
		qr_code_iv.setOnClickListener(new ButtonListener());
		newMsg.setOnClickListener(new ButtonListener());
		myNew.setOnClickListener(new ButtonListener());
		myTradingBut.setOnClickListener(new ButtonListener());
		myAppointmentsBut.setOnClickListener(new ButtonListener());
		setting.setOnClickListener(new ButtonListener());
	}

	private class ButtonListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			// 个人详细信息
			case R.id.individual_center_account_information:
				System.out.println("开始跳转到个人详细");
				startActivity(new Intent(getActivity(),
						IndividualCenterInformation.class));

				break;
			case R.id.qr_code_iv:
				// 未登录，不能查看二维码
				if (!Utils.getBooleanValue(getActivity(), Constants.LoginState)) {
					Toast.makeText(getActivity(), "请先登录，再查看", 1).show();
					return;
				} else {
					inflater = LayoutInflater.from(getActivity());
					myCode = inflater.inflate(R.layout.layout_mycode, null);
					btn_shareQRcode = (Button) myCode
							.findViewById(R.id.btn_shareQRcode);
					img_code = (ImageView) myCode.findViewById(R.id.img_code);
					// ImageView img = new ImageView(getActivity());
					// img.setImageResource(R.drawable.qr_show);
					// 生成二维码
					EncondingHandler EncondingHandler = new EncondingHandler();
					try {
						Bitmap qrCodeBitmap = EncondingHandler.createQRCode(
								"1236555", 350);
						img_code.setImageBitmap(qrCodeBitmap);
					} catch (WriterException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					new AlertDialog.Builder(getActivity()).setView(myCode)
							.show();
					btn_shareQRcode.setOnClickListener(new OnClickListener() {

						public void onClick(View v) {
							// TODO Auto-generated method stub
							System.out.println("分享QRCode按钮被点击了");
							// 获取需要的邀请码,1.0分享连接
							showShare();
						}
					});
				}

				break;
			case R.id.setting:
				// 设置
				startActivity(new Intent(getActivity(), Setting.class));

				break;
			case R.id.myTradingBut:
				// 交易
				startActivity(new Intent(getActivity(), MyTrading.class));
				// Utils.finish(getActivity());
				break;
			case R.id.newMsg:
				// 新消息
				startActivity(new Intent(getActivity(), NewMsg.class));
				break;
			case R.id.myNew:
				// 新闻
				startActivity(new Intent(getActivity(), MyNews.class));
				break;
			case R.id.myAppointmentsBut:
				startActivity(new Intent(getActivity(), AppointmentRecord.class));
				break;
			default:
				Toast.makeText(getActivity(), "此功能待待开发。。。。。", Toast.LENGTH_LONG)
						.show();
				break;
			}
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (resultCode) {
		case 1:
			System.out.println("这里是个人中心");
			break;

		default:
			break;
		}
	}

	// 刷新页面
	public void refresh() {
		initView();
	}

	// 通知Activity的Collection做出刷新
	public interface onLogoutListener {
		public void refCollectFragment();
	}

	/*
	 * 生成二维码
	 */
	public final static class EncondingHandler {
		private static final int BLACK = 0xff000000;

		public static Bitmap createQRCode(String str, int widthAndHeight)
				throws WriterException {
			Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>();
			hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
			BitMatrix matrix = new MultiFormatWriter().encode(str,
					BarcodeFormat.QR_CODE, widthAndHeight, widthAndHeight);
			int width = matrix.getWidth();
			int height = matrix.getHeight();
			int[] pixels = new int[width * height];

			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					if (matrix.get(x, y)) {
						pixels[y * width + x] = BLACK;
					}
				}
			}
			Bitmap bitmap = Bitmap.createBitmap(width, height,
					Bitmap.Config.ARGB_8888);
			bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
			return bitmap;
		}
	}

	private void showShare() {
		ShareSDK.initSDK(getActivity().getApplicationContext());
		OnekeyShare oks = new OnekeyShare();
		// 分享时Notification的图标和文字
		oks.disableSSOWhenAuthorize();

		oks.setTitle(getString(R.string.share));
		// titleUrl是标题的网络链接，仅在人人网和QQ空间使用
		oks.setTitleUrl("http://sharesdk.cn");
		// text是分享文本，所有平台都需要这个字段
		oks.setText("我是分享文本");
		// imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
		// oks.setImagePath("/sdcard/test.jpg");//确保SDcard下面存在此张图片
		// rl("g&fromurl=ippr_z2C%24qAzdH3FAzdH3Fooo_z%26e3B2jxtg2_z%26e3Bv54AzdH3Ffiwtp7AzdH3F8n0a8a9_z%26e3Bip4s&gsm=0");
		oks.setImageUrl("http://f1.sharesdk.cn/imgs/2014/02/26/owWpLZo_638x960.jpg");
		// url仅在微信（包括好友和朋友圈）中使用
		oks.setUrl("http://www.baidu.com");
		// comment是我对这条分享的评论，仅在人人网和QQ空间使用
		oks.setComment("我是测试评论文本");
		// site是分享此内容的网站名称，仅在QQ空间使用
		oks.setSite(getString(R.string.app_name));
		// siteUrl是分享此内容的网站地址，仅在QQ空间使用
		oks.setSiteUrl("http://www.baidu.com");
		// 启动分享GUI
		oks.show(getActivity().getApplicationContext());
	}

    
}
