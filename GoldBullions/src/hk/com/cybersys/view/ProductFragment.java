package hk.com.cybersys.view;


import hk.com.cybersys.activity.MainActivity;
import hk.com.cybersys.activity.Products;
import hk.com.cybersys.activity.ProductsShow;
import hk.com.cybersys.adapter.MyAdapter;
import hk.com.cybersys.adapter.ProductBean;
import hk.com.cybersys.basic.R;
import hk.com.cybersys.http.HCPostRequest;
import hk.com.cybersys.myLibrary.Constant;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import cao.jian.chen.utils.Utils;


public class ProductFragment extends Fragment implements OnClickListener,
OnItemClickListener {
	private ViewPager mviewPager;
	private Activity ctx;
	private View layout, layout_head;
	private MainActivity parentActivity;
		
	private ListView hotListView;
	private MyAdapter myAdapter;
	private JSONArray productArray;
	private LinearLayout barLinearLayout;
		
	private List<ProductBean> hotList=new ArrayList<ProductBean>();
	private ProductBean hotProduct;
	private Handler hotHandler;
	private LinearLayout  bt_private_equity,bt_trust_products,bt_assest_managemen,
	                      P2P_btn,baoxian_btn;
	

	@Override
	public View onCreateView(LayoutInflater inflater,
			 ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub	
		if(layout==null){
			ctx=getActivity();
			parentActivity = (MainActivity) getActivity();
			layout = ctx.getLayoutInflater().inflate(R.layout.activity_products_class,
					null);
			layout.setOnTouchListener(new OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					//System.out.println("触摸了屏幕啦啦啦啦");
					return false;
				}
			});
			//找到
			findViewById();
			initView();
		}else {
			ViewGroup parent = (ViewGroup) layout.getParent();
			if (parent != null) {
				parent.removeView(layout);
			}
		}
					
		return layout;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
	}
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();	
		
	}
	@Override
	public void onResume() {
		// TODO Auto-generated method stub		
		super.onResume();	
	}
	@Override
    public void onPause() {
		super.onPause();
	};
	private void initView(){
		//GetBanner banner=new GetBanner();
		//new Thread(banner).run();
		
		hotHandler=new Handler(){
			@Override
			public void handleMessage(Message msg){
				// TODO Auto-generated method stub
				if(msg.what==1){
					for(int i=0;i<productArray.length();i++){
						//如果销售进度小于50%就放到热门推荐上,并且将门槛资金的TextView删除！
						try {
							if(productArray.getJSONObject(i).getInt("promotion")!=0){
								hotProduct=new ProductBean(productArray.getJSONObject(i).getString("threshold") ,
							                               productArray.getJSONObject(i).getString("name"),
							                               productArray.getJSONObject(i).getString("starttime"),
							                               productArray.getJSONObject(i).getString("productid"),
							                               productArray.getJSONObject(i).getInt("promotion"),
							                               productArray.getJSONObject(i).getInt("maxprofit"),
								                           productArray.getJSONObject(i).getInt("minperiod"),
								                           productArray.getJSONObject(i).getString("period"),
												           productArray.getJSONObject(i).getInt("progress"));
							
								hotList.add(hotProduct);
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						myAdapter=new MyAdapter(getActivity(),getActivity() ,hotList,4);
						hotListView.setAdapter(myAdapter);
						hotListView.setDivider(new ColorDrawable(Color.WHITE));  
						hotListView.setDividerHeight(15);
						
						//每个item添加监听事件
						hotListView.setOnItemClickListener(new OnItemClickListener() {
							@Override
							public void onItemClick(AdapterView<?> parent, View view, int position,
		                            long id) {
								Intent intent = new Intent();
								Bundle bundle = new Bundle();
								//将ProductID做为参数传递过去
								bundle.putString("ProductID", hotList.get(position).getProductID());
								intent.putExtras(bundle);
								intent.setClass(getActivity(),ProductsShow.class);
								startActivity(intent);
								Toast.makeText(getActivity(), hotList.get(position).getProductID(), Toast.LENGTH_SHORT).show();						
							}
						});
					}
					hotListView.setOnTouchListener(new OnTouchListener() {
						
						@Override
						public boolean onTouch(View v, MotionEvent event) {
							// TODO Auto-generated method stub
							switch (event.getAction()) {
							case MotionEvent.ACTION_MOVE:
								return true;
				
							default:
								break;
							}
							return  false;
						}
					});
					Utils.setProductScroll(hotListView);
					//hotListView.onInterceptTouchEvent(MotionEvent);
					
				}
		           // System.out.println();
				super.handleMessage(msg);
			}
		};
		
		Runnable run = new GetHotProduct(hotHandler);
		new Thread(run).start();
	}
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

	public void refresh() {
		initView();
		
	}
	//获得所有控件的ID
	private void findViewById(){
		hotListView=(ListView) layout.findViewById(R.id.HotProducts_list_view);
		bt_private_equity=(LinearLayout) layout.findViewById(R.id.private_equity_button);
		bt_trust_products=(LinearLayout) layout.findViewById(R.id.trust_products_button);
		bt_assest_managemen=(LinearLayout) layout.findViewById(R.id.asset_management_button);
		baoxian_btn=(LinearLayout) layout.findViewById(R.id.baoxian_btn);
		P2P_btn=(LinearLayout) layout.findViewById(R.id.P2P_btn);
		bt_private_equity.setOnClickListener(new ButtonListener());
		bt_trust_products.setOnClickListener(new ButtonListener());
		bt_assest_managemen.setOnClickListener(new ButtonListener());
		P2P_btn.setOnClickListener(new ButtonListener());
		baoxian_btn.setOnClickListener(new ButtonListener());
	}	
	//事件监听
	private class ButtonListener implements OnClickListener{
		Bundle bundle = new Bundle();
		Intent intent = new Intent();
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.private_equity_button:
				bundle.putString("productType", "001");
				intent.putExtras(bundle);
				intent.setClass(getActivity(), Products.class);
				startActivity(intent);
				break;
			case R.id.trust_products_button:
				bundle.putString("productType", "002");
				intent.putExtras(bundle);
				intent.setClass(getActivity(), Products.class);
				startActivity(intent);
				break;
			case R.id.asset_management_button:
				bundle.putString("productType", "003");
				intent.putExtras(bundle);
				intent.setClass(getActivity(), Products.class);
				startActivity(intent);
				break;
			case R.id.P2P_btn:
				Toast.makeText(getActivity(), "产品研发ing！敬请期待。。。", 1).show();
				break;
			case R.id.baoxian_btn:
				Toast.makeText(getActivity(), "产品研发ing！敬请期待。。。", 1).show();
				break;
			default:
				break;
			}		
		}	
	};

	// 获得热门推荐,并显示在产品页
	class GetHotProduct implements Runnable {

			private JSONObject productJsonOb;
			private String product_json_str;
			private Handler handler;

			public GetHotProduct(Handler handler) {
				this.handler = handler;
			}

			@Override
			public void run() {
				// TODO Auto-generated method stub
				this.product_json_str = HCPostRequest.sendPost(Constant.URL_PRODUCT_FULL_LIST,
						                                       "field=&category="+"all" 
		                 );
	            try {
	                 productJsonOb = new JSONObject(product_json_str);
	                 //获得数据库上的所有产品list
	                productArray = productJsonOb.getJSONArray("list");
	                //System.out.println(productArray+"6666666666666666666666666666666666666");
	                this.handler.sendEmptyMessage(1);
	             } catch (JSONException e1) {
	               // TODO Auto-generated catch block
	                e1.printStackTrace();
	             }
				
			 }
		  }

	 
}
