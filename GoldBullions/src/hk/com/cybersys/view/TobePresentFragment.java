package hk.com.cybersys.view;

import hk.com.cybersys.activity.AppointmentRecord;
import hk.com.cybersys.activity.Login;
import hk.com.cybersys.activity.MainActivity;
import hk.com.cybersys.activity.MyDialog;

import hk.com.cybersys.activity.MyDialog.MyDialogOption;
import hk.com.cybersys.adapter.AppointmentAdapter;
import hk.com.cybersys.adapter.AppointmentBean;
import hk.com.cybersys.basic.R;
import hk.com.cybersys.http.HCPostRequest;
import hk.com.cybersys.myLibrary.CommonWidget;
import hk.com.cybersys.myLibrary.Constant;
import hk.com.cybersys.otheractivity.AppointmentDetailActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.baidu.navisdk.util.SysOSAPI;

import cao.jian.chen.utils.Constants;
import cao.jian.chen.utils.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.renderscript.Sampler.Value;

import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

//待出席的预约记录
public class TobePresentFragment extends Fragment {

	private Activity ctx;
	private View layout;
	private AppointmentRecord parentActivity;
	
	private ListView listview;
	private TextView noRecordText;
	private int delIndex;
	private ArrayList<HashMap<String, Object>> listItem;
	//用来封装获得的预约记录
	private List<AppointmentBean> appointmentBeanList=new ArrayList<AppointmentBean>();
	
	 
	private HashMap<String, Object> hashMap;
	
	private SimpleAdapter msimpleAdapter;
	
	private JSONObject returnData;
	private JSONArray jsonArrayData;
	
	//客户的ID
	private String customerID;
	private SharedPreferences settings;
	//是否已经登录
	private String isLogin;
	
	private Handler handler;
	private Handler delHandler;
	private MyDialog myDialog;
	private String appointmentId;
	private  AppointmentAdapter appointadapter;
	private  DisplayMetrics dm;
	private  int m_dispWidth;
	private  int m_dispHeight;
	@Override
	
	public View onCreateView(LayoutInflater inflater,
			 ViewGroup container,  Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if(layout==null){
			ctx=getActivity();
			parentActivity = (AppointmentRecord) getActivity();
			layout = ctx.getLayoutInflater().inflate(R.layout.layout_appointment_attent,
					null);		
			init();
		}else {
			ViewGroup parent = (ViewGroup) layout.getParent();
			if (parent != null) {
				parent.removeView(layout);
			}
		}					
		return layout;	
	}
	
	public void init(){
		System.out.println("这是预约记录");
		listview = (ListView)layout.findViewById(R.id.appointment_record_listview);
		noRecordText=(TextView) layout.findViewById(R.id.appoointment_norecord);		
		customerID =  Utils.getValue(getActivity(), Constants.CustomerID);
		dm = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
		m_dispWidth = dm.widthPixels;
		m_dispHeight = dm.heightPixels;
		
		handler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				switch(msg.what){
				case 1:
					initlistview();
					break;
				case 2:
					CommonWidget.progressDialog.dismiss();
					break;
				case 3:
					CommonWidget.progressDialog.dismiss();
					//System.out.println("=========================================++++++++++++++++++++++++++++");
					listview.setVisibility(View.GONE);
					noRecordText.setVisibility(View.VISIBLE);
					Toast.makeText(getActivity(), "还没有预约记录！", 1).show();
					break;
				case 4:				
					appointmentBeanList.remove(delIndex);
					appointadapter.notifyDataSetChanged();
					break;
				}			
				super.handleMessage(msg);
			}			
		};
		
			
			Runnable r = new GetAppointmentRecord(customerID);
			new Thread(r).start();	
			
		
	}
	
	
	
	//获取数据
	public void initlistview(){
		//listItem = new ArrayList<HashMap<String,Object>>();
		try {
			jsonArrayData = returnData.getJSONArray("list");
			//System.out.println("这里："+jsonArrayData);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(int i=0;i < jsonArrayData.length();i++){
			AppointmentBean appointmentbean;
			try {
				if(jsonArrayData.getJSONObject(i).getInt("status")==1){
					//Toast.makeText(getActivity(),jsonArrayData.getJSONObject(i).getInt("acceptance") ,1).show();
					appointmentbean=new AppointmentBean(
							jsonArrayData.getJSONObject(i).getInt("status"),
							jsonArrayData.getJSONObject(i).getInt("appointmentid"),
							jsonArrayData.getJSONObject(i).getString("brokermobile"), 
							jsonArrayData.getJSONObject(i).getInt("brokerid"), 
							jsonArrayData.getJSONObject(i).getString("brokericonname"), 
							jsonArrayData.getJSONObject(i).getString("brokername"),
							jsonArrayData.getJSONObject(i).getString("productname1"), 
							jsonArrayData.getJSONObject(i).getString("productname2"), 
							jsonArrayData.getJSONObject(i).getString("productname3"),
							jsonArrayData.getJSONObject(i).getString("time"),
							jsonArrayData.getJSONObject(i).getString("date"),
							jsonArrayData.getJSONObject(i).getInt("acceptance"),
							jsonArrayData.getJSONObject(i).getInt("evaluationid"));
					appointmentBeanList.add(appointmentbean);
				}
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
		//for(int i=0;i < appointmentBeanList.size();i++){
		//	System.out.println("这是第"+i+"个bean的值："+appointmentBeanList.get(i).getStatus());
		//}
		
	        
		  //System.out.println("开始适配");
		  appointadapter=new AppointmentAdapter(getActivity(), appointmentBeanList);
		  listview.setAdapter(appointadapter);
		   //System.out.println("预约记录上listview的conunt"+listview.getCount());
		  listview.setDivider(new ColorDrawable(Color.CYAN));  
		  listview.setDividerHeight(8);
		  //点击listView进入到预约的详细页面
		  listview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Bundle myBundle=new Bundle();
				myBundle.putSerializable("appointRecBean", appointmentBeanList.get(position));
				
				Toast.makeText(getActivity(), "点击的listview的position："+position, 1).show();
				Intent myIntent=new Intent();
				myIntent.putExtras(myBundle);
				myIntent.setClass(getActivity(), AppointmentDetailActivity.class);
				startActivity(myIntent);
				
			}
		});
		   //短按删除
		 /* listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				ListView lv = (ListView)parent;  
				delIndex=position;
			    //HashMap<String,Object> appointmentRecord = (HashMap<String,Object>)lv.getItemAtPosition(position);
		       // System.out.println(appointmentRecord.get("appointmentId")+"=====================");
		        try {
					int i=Integer.valueOf(jsonArrayData.getJSONObject(position).getInt("appointmentid"));
					myDialog=new MyDialog(getActivity(), i);
	                myDialog.setMyDialogOption(new MyDialogOption() {				
						@Override
						public void del() {
							// TODO Auto-generated method stub
							System.out.println("开始删除预约！");
							Runnable deleteAppointment=new DeleteAppointmentRecord(appointmentId);
					        new Thread(deleteAppointment).start();
					        myDialog.dismiss();		
						}		
						@Override
						public void cancleAppointment() {
							// TODO Auto-generated method stub				
						}	
						@Override
						public void cancle() {
							// TODO Auto-generated method stub
							myDialog.dismiss();		
						}
					});
		        } catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		       // appointmentId=String.valueOf(i);      
			}
		   });*/
		
		   //长按删除
		   listview.setOnItemLongClickListener(new OnItemLongClickListener() {
              
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				System.out.println("这是待出席的长按删除");
				// TODO Auto-generated method stub
				ListView lv = (ListView)parent;  
				delIndex=position;
		       
		        int i;
				try {
					i = Integer.valueOf(jsonArrayData.getJSONObject(position).getInt("appointmentid"));
					appointmentId=String.valueOf(i);
			        myDialog=new MyDialog(getActivity(), i);
			        myDialog.setMyDialogOption(new MyDialogOption() {
						@Override
						public void del() {
							// TODO Auto-generated method stub
							//System.out.println("开始删除预约！");
							//Runnable deleteAppointment=new DeleteAppointmentRecord(appointmentId);
					       // new Thread(deleteAppointment).start();
					        //myDialog.dismiss();		
						}
						@Override
						public void cancleAppointment() {
							// TODO Auto-generated method stub			
						}				
						@Override
						public void cancle() {
							// TODO Auto-generated method stub
							myDialog.dismiss();	
						}
					});
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        System.out.println("开始删除");
		        //showMyDialog((String)appointmentRecord.get("appointmentId"));
		        Runnable deleteAppointment=new DeleteAppointmentRecord(appointmentId);
		        new Thread(deleteAppointment).start();
		        Toast.makeText(getActivity(), "删除预约成功", 1).show();
				return false;
			}
		});

		    
		Message msg = new Message();
		msg.what = 2;
		handler.sendMessage(msg);
	}
	
	class GetAppointmentRecord implements Runnable{
		private String parameter;
		public GetAppointmentRecord(String parameter){
			this.parameter =  parameter;
		}
		@Override
		public void run() {
			try {
				//returnData就是一个JSON对象
				returnData = new JSONObject(HCPostRequest.sendPost(Constant.URL_APPOINTMENT_GET_BY_CUSTOMERID, 
						                   "customerid="+parameter));	
				Message msg = new Message();
				if(returnData.getString("success").equals("1")){
					msg.what=1;
				}else if(returnData.getString("error").equals("无记录")){
					msg.what = 3;
				}
				//System.out.println("这是GetAppointmentRecord上发送的msg.what:"+msg.what);
				
				handler.sendMessage(msg);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
	}
	//删除预约记录
	class DeleteAppointmentRecord implements Runnable{
		private String recordID;
        private String returnData;
		public DeleteAppointmentRecord(String recordID){
			this.recordID=recordID;
		}
		@Override
		public void run() {
			// TODO Auto-generated method stub
			returnData=HCPostRequest.sendPost(Constant.URL_DEL_APPOINTMENT_BY_APPOINTMENTID, 
					                          "appointmentid="+recordID);
			System.out.println("recordID:"+recordID);
			System.out.println(returnData+"++++++++++++++");
			try {
				JSONObject jsonOb = new JSONObject(returnData);
				if((jsonOb.getString("success").equals("1"))){
					
					System.out.println("删除成功！");
					//删除成功后更新UI
					Message msg = new Message();
					msg.what = 4;
					System.out.println("msg.what :"+msg.what );
					handler.sendMessage(msg);
					//handler.post(this);
				}else{
					System.out.println("删除失败！");
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
    public void showMyDialog(final String i){
	    System.out.println("进入到dialog上");
		AlertDialog.Builder dialogBuild=new Builder(getActivity());
		dialogBuild.setMessage("选择所需的操作!");
		dialogBuild.setTitle("提示");
		dialogBuild.setPositiveButton("删除预约", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
			    System.out.println("删除预约！");
			    Runnable deleteAppointment=new DeleteAppointmentRecord(i);
		        new Thread(deleteAppointment).start();
			    dialog.dismiss();
			   // Collection.this.finish();
			}

			
		});
		dialogBuild.setNeutralButton("取消预约", new DialogInterface.OnClickListener() {			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				System.out.println("取消预约！");
				 dialog.dismiss();
				
			}
		});
		dialogBuild.create().show();
}
}
