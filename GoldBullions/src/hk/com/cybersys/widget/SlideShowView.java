package hk.com.cybersys.widget;

import hk.com.cybersys.basic.R;
import hk.com.cybersys.http.HCPostRequest;
import hk.com.cybersys.myLibrary.Constant;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.os.StrictMode;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import cao.jian.chen.utils.LoadIcon;


/**
 * ViewPager实现的轮播图广告自定义视图，如京东首页的广告轮播图效果；
 * 既支持自动轮播页面也支持手势滑动切换页面
 * @author chenjiancao
 *
 *banner轮播，连接数据库，
 */

public class SlideShowView extends FrameLayout {

	private float mX, mY;
    //轮播图图片数量
    private final static int IMAGE_COUNT = 5;
    //自动轮播的时间间隔
    private final static int TIME_INTERVAL = 8;
    //自动轮播启用开关
    private  boolean isAutoPlay = true; 
    private boolean isTouching =false;
    
    //自定义轮播图的资源ID
    private int[] imagesResIds;
    private ImageView[] bannerImg;
    //放轮播图片的ImageView 的list
    private List<ImageView> imageViewsList;
    //放圆点的View的list
    private List<View> dotViewsList;
    
    private ViewPager viewPager;
    //当前轮播页
    private int currentItem  = 0;
    //定时任务
    private ScheduledExecutorService scheduledExecutorService;
    private Context context;
    private LoadIcon mbanner;
    private JSONArray bannerArray;
    
    
    
    //Handler
    private Handler bannerHander=new Handler(){
    	public void handleMessage(Message msg) {	
    		if(msg.what==1){
    			bannerImg=new ImageView[bannerArray.length()];
    			for(int i=0;i<bannerArray.length();i++){
    				final ImageView view =  new ImageView(context); 
    				try {
						mbanner.setRemoteImageListener(Constant.URL_GET_BANNER_IMAGE+bannerArray.getJSONObject(i).get("image"),
								                        new LoadIcon.OnRemoteImageListener() {   
						    @Override  
						    public void onError(String error) {  
						        //Toast.makeText(context, error, Toast.LENGTH_LONG).show();  
						    	System.out.println("下载图片失败");
						    }  
						    @Override  
						    public void onRemoteImage(Bitmap image) {  
						    	view.setImageBitmap(image);  
						    }  }               );
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    				// view.setImageResource(imageID);
    		         view.setScaleType(ScaleType.FIT_XY);
    		         imageViewsList.add(view);
    			}
    		/*	for(int i=0;i<bannerArray.length();i++){
    				final ImageView view =  new ImageView(context);
    				try {
						ImageLoader.getInstance().loadImage(Constant.URL_GET_BANNER_IMAGE+bannerArray.getJSONObject(i).get("image"),
								new ImageLoadingListener() {
									
									@Override
									public void onLoadingStarted(String arg0, View arg1) {
										// TODO Auto-generated method stub
										
									}									
									@Override
									public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
										// TODO Auto-generated method stub
										
									}
									
									@Override
									public void onLoadingComplete(String arg0, View arg1, Bitmap loadedImage) {
										// TODO Auto-generated method stub
										view.setImageBitmap(loadedImage);
									}
									
									@Override
									public void onLoadingCancelled(String arg0, View arg1) {
										// TODO Auto-generated method stub
										
									}
								});
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    				view.setScaleType(ScaleType.FIT_XY);
   		            imageViewsList.add(view);		
    			}*/
    			
    			initUI();
    		}
    	};
    };
    private Handler handler = new Handler(){

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
       	
				viewPager.setCurrentItem(currentItem);
	          //  handler.sendEmptyMessageAtTime(1, 2000);			        
        }      
    };
    
    //构造函数
    public SlideShowView(Context context) {
        //this(context,null);
    	super(context);
    	System.out.println("这是含一个参数的SlideShowView");
    	//this.context=context;
    	//initData();
    	//startPlay();
    }
    public SlideShowView(Context context, AttributeSet attrs) {
        super(context,attrs);
        // TODO Auto-generated constructor stub
    	//this(context,attrs,0);
        this.context=context;
        System.out.println("这是含两个参数的SlideShowView");
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        mbanner=new LoadIcon();
        initData();
        startPlay();
    }
    public SlideShowView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        // TODO Auto-generated constructor stub
        this.context=context;
        System.out.println("这是含三个参数的SlideShowView");
        //initData();
        
        /*if(isAutoPlay){
            startPlay();
        }*/
       // startPlay();
    }
    /**
     * 开始轮播图切换
     */
    private void startPlay(){
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleAtFixedRate(new SlideShowTask(), 1, 
        		                                  8, TimeUnit.SECONDS);
    }
    /**
     * 停止轮播图切换
     */
    private void stopPlay(){
        scheduledExecutorService.shutdown();
    }
    /**
     * 初始化相关Data
     */
    private void initData(){
    	
    	//数据库中抓取数据，获取数据库中图片的数量
    	//getBannerNum();
    	/*Handler imgHandler=new Handler(){
    		@Override
    		public void handleMessage(Message msg) {
    			// TODO Auto-generated method stub
    			super.handleMessage(msg);
    		}*/
    	//};
    	//initUI();
     /*  imagesResIds = new int[]{
                R.drawable.pic1,
                R.drawable.pic2,
                R.drawable.pic3,
                R.drawable.pic4,
                R.drawable.pic5,
        };*/
    	//System.out.println("第一次");
        imageViewsList = new ArrayList<ImageView>();//图片的初始化
    	//System.out.println("第二次");
        dotViewsList = new ArrayList<View>();//点的初始化
        System.out.println("开启线程");
        GetBanner banner=new GetBanner(bannerHander);
    	new Thread(banner).run();
    	    
    }
   
    /**
     * 初始化Views等UI
     */
    private void initUI(){
        LayoutInflater.from(context).inflate(R.layout.layout_slideshow, this, true);
       // System.out.println("SilideShowView:HAHAHAHAHHAH");
      
       /* for(int imageID : imagesResIds){
            ImageView view =  new ImageView(context);
           
            view.setImageResource(imageID);
            view.setScaleType(ScaleType.FIT_XY);
            imageViewsList.add(view);
        }*/
        dotViewsList.add(findViewById(R.id.v_dot1));
        dotViewsList.add(findViewById(R.id.v_dot2));
        dotViewsList.add(findViewById(R.id.v_dot3));
        dotViewsList.add(findViewById(R.id.v_dot4));
        dotViewsList.add(findViewById(R.id.v_dot5));
        
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setFocusable(true);//设置焦点
        
        viewPager.setAdapter(new MyPagerAdapter());//适配器，装载图片
        viewPager.setOnPageChangeListener(new MyPageChangeListener());//监听器
        //触摸监听事件
            
    }
    
    /**
     * 填充ViewPager的页面适配器
     * @author caizhiming
     */
    private class MyPagerAdapter  extends PagerAdapter{

        @Override
        public void destroyItem(View container, int position, Object object) {
            // TODO Auto-generated method stub
            //((ViewPag.er)container).removeView((View)object);
            ((ViewPager)container).removeView(imageViewsList.get(position%imageViewsList.size()));
        }

        @Override
        public Object instantiateItem(View container, int position) {
            // TODO Auto-generated method stub
            ((ViewPager)container).addView(imageViewsList.get(position));
            return imageViewsList.get(position%imageViewsList.size());//
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return imageViewsList.size();
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            // TODO Auto-generated method stub
            return arg0 == arg1;
        }
        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {
            // TODO Auto-generated method stub

        }

        @Override
        public Parcelable saveState() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public void startUpdate(View arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void finishUpdate(View arg0) {
            // TODO Auto-generated method stub
            
        }
        
    }
    /**
     * ViewPager的监听器
     * 当ViewPager中页面的状态发生改变时调用
     * @author caizhiming
     */
    private class MyPageChangeListener implements OnPageChangeListener{
    	 boolean isAutoPlay = false;
        //boolean isAutoPlay = false;
        @Override
        public void onPageScrollStateChanged(int arg0) {
            // TODO Auto-generated method stub
            switch (arg0) {
            case 1:// 手势滑动，空闲中
               isAutoPlay = false;
              //  stopPlay();
                //设置一个休眠时间后自动滑动   
               // stopPlay();
                break;
            case 2:// 界面切换中
               isAutoPlay = true;         
                break;
            case 0:// 滑动结束，即切换完毕或者加载完毕  	
                // 当前为最后一张，此时从右向左滑，则切换到第一张
                if (viewPager.getCurrentItem() == viewPager.getAdapter().getCount() - 1 && !isAutoPlay) {
                    viewPager.setCurrentItem(0);
                }
                // 当前为第一张，此时从左向右滑，则切换到最后一张
                else if (viewPager.getCurrentItem() == 0 && !isAutoPlay) {
                    viewPager.setCurrentItem(viewPager.getAdapter().getCount() - 1);
                }                   
                break;
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
            // TODO Auto-generated method stub
        	
            
        }

        @Override
        public void onPageSelected(int pos) {
            // TODO Auto-generated method stub           
            currentItem = pos;
            for(int i=0;i < dotViewsList.size();i++){
                if(i == pos){
                    ((View)dotViewsList.get(pos)).setBackgroundResource(R.drawable.dot_black);
                }else {
                    ((View)dotViewsList.get(i)).setBackgroundResource(R.drawable.dot_white);
                }
            }
         //   isAutoPlay=true;
           // startPlay();
           
        }     
    }
    
    /*
     * 分发触摸事件
     */
     //@Override
     /* public boolean dispatchTouchEvent(MotionEvent ev) {
    	// TODO Auto-generated method stub
    	switch (ev.getAction()) {
		case MotionEvent.ACTION_DOWN:
			mX = ev.getRawX();
			mY = ev.getRawY();
			isTouching = true;
			isAutoPlay=false;
			getParent().requestDisallowInterceptTouchEvent(false);
			if (mHandler != null) {
				mHandler.removeCallbacksAndMessages(null);
			}
			break;
		case MotionEvent.ACTION_MOVE:
			if (isTouching) {
				float dx = Math.abs(ev.getRawX() - mX);
				float dy = Math.abs(ev.getRawY() - mY);
				if (dx > dy) {
					getParent().requestDisallowInterceptTouchEvent(true);
					isTouching = false;
				}
				if (mHandler != null) {
					mHandler.removeCallbacksAndMessages(null);
//					handler.sendEmptyMessageDelayed(2, 1000);
				}
			}
			break;
		case MotionEvent.ACTION_UP:
			isTouching=false;
			isTouching = false;
			getParent().requestDisallowInterceptTouchEvent(false);	
			if (mHandler != null) {
				mHandler.removeCallbacksAndMessages(null);
				mHandler.sendEmptyMessageDelayed(2, 1000);
			}
		default:
			break;
		}
    	return super.dispatchTouchEvent(ev);
    }*/
    
    /**
     *执行轮播图切换任务
     *@author caizhiming
     */
    private class SlideShowTask implements Runnable{

        @Override
        public void run() {
        	
            // TODO Auto-generated method stub
            synchronized (viewPager) {
                currentItem = (currentItem+1)%imageViewsList.size();
             //   handler.sendEmptyMessageDelayed(1,2000);
                //handler.sendEmptyMessage(1);
                handler.obtainMessage().sendToTarget();
            }
        }
    }
    /**
     * 销毁ImageView资源，回收内存
     * @author caizhiming
     */
    private void destoryBitmaps() {
        for (int i = 0; i < IMAGE_COUNT; i++) {
            ImageView imageView = imageViewsList.get(i);
            Drawable drawable = imageView.getDrawable();
            if (drawable != null) {
                //解除drawable对view的引用
                drawable.setCallback(null);
            }
        }
    }
    

   
    //获取广告图片
	 class GetBanner implements Runnable{
		    private Handler mHandler;
	    	private String bannerStr;
	    	public GetBanner(Handler handler){
	    		//System.out.println("这是滚动广告条上启动图片线程的构造函数");
	    		this.mHandler=handler;
	    	}
			@Override
			public void run() {
				bannerStr=HCPostRequest.sendPost(Constant.URL_GET_BANNER,"");
				try {	
					bannerArray=new JSONArray(bannerStr);
					
					System.out.println("bannerArray的长度："+bannerArray.length());
					for(int i=0;i<bannerArray.length();i++){
						System.out.println("第"+i+"个图片的image为："+bannerArray.getJSONObject(i).get("image"));
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Message msg=new Message();
				msg.what=1;
				mHandler.sendMessage(msg);
			}    	
	    };
	
}